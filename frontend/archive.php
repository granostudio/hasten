<?php
/**
 * The template for displaying archive pages
 *
 *
 */

get_header(); ?>

<?php if (is_archive()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- Page Content -->
    <div class="container">

        <div class="row article-list">

            <div class="row header-archive">
                <div class="container">
                    <h1 class="page-header">
                        <?php
                            the_archive_title( '<small style="color: #0D2D25;">', '</small>' );
                            the_archive_description( '<small>', '</small>' );
                         ?>
                        <!-- <small>Secondary Text</small> -->
                    </h1> 
                        
                    <div class="categorias"> 
                        <p>Sugestões de categorias: </p>
                        <?php wp_list_categories( array(
                            'orderby'    => 'name',
                            'title_li' => false
                        ) ); ?> 
                    </div>
                </div>
            </div>
            

            <div class="row articles">

                <?php
                if( have_posts() ) {
                  while ( have_posts() ) {
                    the_post(); ?>
                    
                    <div class="col-sm-4 posts">
                        <div class="img-thumb" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                        <ul class="lista-categoria">
                        <?php
                          foreach((get_the_category()) as $category) {
                            echo '<li class="hashtags">' . $category->cat_name . '</li>';
                          }
                         ?>
                        </ul> 
                        <p><?php echo get_the_title(); ?></p>
                        <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
                    </div>


                  <?php }
                } else {
                  /* No posts found */
                } ?>
                
            </div>
            <!-- Pager -->
            <div class="row">
                <ul class="pager">

                    <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                    <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                </ul>
            </div>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

	
<?php get_footer(); ?>
