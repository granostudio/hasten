<?php get_header(); ?>

<div class="indicador">
	<p>Você está aqui: <span>Blog</span></p>
</div>

<div class="div-1-blog">

	<div id="myCarousel" class="carousel slide carousel-home" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      <li data-target="#myCarousel" data-slide-to="1"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner">

		<?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'Destaque');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>	    

	      <div class="item active" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	      	<div class="mask-banner"></div>
	      	<div class="conteudo">
	      		<ul class="lista-categoria">
	            <?php
	              foreach((get_the_category()) as $category) {
	                echo '<li class="hashtags">' . $category->cat_name . '</li>';
	              }
	             ?>
	            </ul>
	      		<h2><?php echo get_the_title(); ?></h2>
	      		<a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
	      	</div>
	      </div>

	     <?php endwhile; // end of the loop. ?>
         <?php endif; ?>


        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'Destaque', 'offset' => 1);
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>	
			
		  <a href="<?php echo get_the_permalink(); ?>">	
		      <div class="item" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
		      	<div class="mask-banner"></div>
		      	<div class="conteudo">
		      		<ul class="lista-categoria">
		            <?php
		              foreach((get_the_category()) as $category) {
		                echo '<li class="hashtags">' . $category->cat_name . '</li>';
		              }
		             ?>
		            </ul>
		      		<h2><?php echo get_the_title(); ?></h2>
		      		<a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
		      	</div>
		      </div>
	      </a>

	      <?php endwhile; // end of the loop. ?>
          <?php endif; ?>

	    </div>

	    <!-- Left and right controls -->
	    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	      <span class="glyphicon glyphicon-chevron-left">
	      	<img src="<?php echo get_stylesheet_directory_uri();?>/img/left-arrow-angle.png" class="">
	      </span>
	      <span class="sr-only">Previous</span>
	    </a>

	    <a class="right carousel-control" href="#myCarousel" data-slide="next">
	      <span class="glyphicon glyphicon-chevron-right">
	      	<img src="<?php echo get_stylesheet_directory_uri();?>/img/left-arrow-angle.png" class="">
	      </span>
	      <span class="sr-only">Next</span>
	    </a>

	 </div>
		
</div>

<div class="container div-2-blog">
	<div class="row">
		<form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
            <div class="form-group">
                <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
            </div>
        </form>
	</div>

	<div class="row">
		
		<div class="col-sm-6">
			<?php
	         $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'Destaque');
	         $loop = new WP_Query( $args );

	         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

				<div class="post-destaque" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
					<div class="mask"></div>
					<div class="elements">
						<ul class="lista-categoria">
			            <?php
			              foreach((get_the_category()) as $category) {
			                echo '<li class="hashtags">' . $category->cat_name . '</li>';
			              }
			             ?>
			            </ul> 
						<h2><?php echo get_the_title(); ?></h2>
						<p><?php echo the_excerpt_max_charlength(300); ?></p>
						<p><a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a></p>
					</div>
				</div>

			<?php endwhile; // end of the loop. ?>
        	<?php endif; ?>

		</div>

		<div class="col-sm-6">
			<?php
	         $args = array( 'post_type' => 'post', 'posts_per_page' => 2, 'category_name' => 'Destaque', 'offset' => 1);
	         $loop = new WP_Query( $args );

	         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>
			
			<a href="<?php echo get_the_permalink(); ?>">
				<div class="posts-destaques">
					<ul class="lista-categoria">
		            <?php
		              foreach((get_the_category()) as $category) {
		                echo '<li class="hashtags">' . $category->cat_name . '</li>';
		              }
		             ?>
		            </ul> 
					<h3><?php echo get_the_title(); ?></h3>
					<p><?php echo the_excerpt_max_charlength(300); ?></p>
					<a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
				</div>	
			</a>

			<?php endwhile; // end of the loop. ?>
        	<?php endif; ?>
		
		</div>

	</div>

	<div class="row">

		<?php
		 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		 $args = array( 'post_type' => 'post', 'posts_per_page' => 6, 'page' => $paged);
		 $loop = new WP_Query( $args );

		 if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

		<a href="<?php echo get_the_permalink(); ?>">
			<div class="col-sm-4 posts">
				<div class="img-thumb" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
				<div class="content">
					<ul class="lista-categoria">
		            <?php
		              foreach((get_the_category()) as $category) {
		                echo '<li class="hashtags">' . $category->cat_name . '</li>';
		              }
		             ?>
		            </ul> 
					<p><?php echo get_the_title(); ?></p>
				</div>
				<a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
			</div>
		</a>

		<?php endwhile; // end of the loop. ?>
        <?php endif; ?>

	</div>

	<!-- Pager -->
    <div class="">
      <ul class="pager">
        <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Posts Anteriores', $loop->max_num_pages); ?></li>
        <li class="next"><?php previous_posts_link( ' Próximos Posts <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
      </ul>
    </div>

</div>

<?php get_footer(); ?>