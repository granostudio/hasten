<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php if ( is_active_sidebar( 'sidebar_blog' )  ) : ?>
	<aside class="col-md-4">
		<?php dynamic_sidebar( 'sidebar_blog' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>
