<?php get_header(); ?>

<div class="indicador">
	<p>Você está aqui: <span>Saúde Humana</span></p>
</div>

<div class="div-1-saudehumana">

	<div class="container">
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<h3 style="margin-bottom: 25px;">
					<img src="<?php echo get_stylesheet_directory_uri();?>/img/coracao-icon-saude-humana.png" class="" style="margin-right: 25px;">
				Saúde Humana</h3>
				<p>Com o aumento da expectativa de vida e uma população de idosos em ascensão, o Brasil vive um momento bastante favorável aos investimentos da indústria de saúde.</p>
				<p>As pessoas começam a tomar mais consciência sobre sua saúde e demandam mais acesso aos serviços.</p>
				<p>Tudo isso resulta num maior consumo de materiais, equipamentos, dispositivos e medicamentos.</p>
			</div> 
			<div class="col-sm-5 col-md-4 coluna-icon">
				
				<div class="row">
					<img src="<?php echo get_stylesheet_directory_uri();?>/img/brasil-branco.png" class="">
					<p>Dos mais de 15 mil<br> hospitais da Am. Latina</p>
					<p class="count-p"><span class="count">7</span> mil</p>
					<p>estão no Brasil</p>
				</div>

				<div class="row">
					<img src="<?php echo get_stylesheet_directory_uri();?>/img/medico-icon-saude-humana.png" class="">
					<p>De 1 milhão de médicos, quase</p>
					<p class="count-p"><span class="count">400</span> mil</p>
					<p>são brasileiros</p>
				</div>

				<div class="row">
					<img src="<?php echo get_stylesheet_directory_uri();?>/img/pranchetabranca.png" class="">
					<p class="count-p"><span class="count">51</span> mi</p>
					<p>de beneficiários de planos de saúde e 21 milhões de planos odontológicos</p>
				</div>

			</div>
		</div>

		<div class="row">
			<img src="<?php echo get_stylesheet_directory_uri();?>/img/angle-arrow-down.png" class="img-seta">
		</div>
	</div>
	
</div>

<div class="container div-2-saudehumana">

	<div class="row titulo-div2">
		<p>Em 2015, o mercado de saúde brasileiro<br> ultrapassou os</p>
		<p>US$ <span>52 bi</span></p>
		<p>representando 43,1% das receitas da América Latina.</p>
	</div>

	<p class="pre-titulo">A indústria de saúde no Brasil</p>

	  <table class="table">
	    <tbody>

	      <tr>
	        <td data-aos="fade-right">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/bandaid-icon-saude-humana.png">
	        	<p><span>R$</span> 270 Bi</p>
	        	<p>é o gasto anual om saúde</p>
	        </td>
	        <td data-aos="fade">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/microscopio-icon-saude-humana.png">
	        	<p>5<span>%</span> das despesas</p>
	        	<p>totais de saúde são dedicadas a equipamentos, produtos e suprimentos médicohospitalares</p>
	        </td>
	        <td data-aos="fade-left">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/home-icon-saude-humana.png">
	        	<p>12 mil empresas</p>
	        	<p>em média, estão estabelecidas no segmento</p>
	        </td data-aos="fade-left">
	      </tr>

	      <tr>
	        <td data-aos="fade-right">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/maleta-icon-saude-humana.png">
	        	<p>8,4<span>%</span> do PIB</p>
	        	<p>provém do setor de saúde</p>
	        </td>
	        <td data-aos="fade">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/moeda-icon-saude-humana.png">
	        	<p><span>R$</span> 13 Bi</p>
	        	<p>foi o faturamento em 2011</p>
	        </td>
	        <td data-aos="fade-left">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/prancheta-icon-saude-humana.png">
	        	<p>10<span>%</span> é o investimento</p>
	        	<p>em pesquisa e desenvolvimento</p>
	        </td>
	      </tr>

	      <tr>
	        <td data-aos="fade-right">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/telefone-icon-saude-humana.png">
	        	<p>2013</p>
	        	<p>US$ 4,9 Bi em importações<br><span>US$</span> 740 Bi em exportações</p>
	        </td>
	        <td data-aos="fade">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/moeda-icon-saude-humana.png">
	        	<p><span>R$</span> 3 Bi</p>
	        	<p>é a contribuição de impostos do setor</p>
	        </td>
	        <td data-aos="fade-left">
	        	<img src="<?php echo get_stylesheet_directory_uri();?>/img/brasil-icon-saude-humana.png">
	        	<p>2<span>º</span> país</p>
	        	<p>emergente mais relevante para a indústria de dispositivos médicos, atrás da China</p>
	        </td>
	      </tr>
	    </tbody>
	  </table>

	  <p style="text-align: center;font-size: 10px;">Fontes: IBGE, Ministério da Saúde, Ministério do Desenvolvimento, ANS, Anvisa, Abimo, Abimed, Brazilian Health Devices e Frost&Sullivan</p>
	
</div>


<?php get_footer(); ?>		