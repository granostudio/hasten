<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
 

add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	}



// =========================== Custom post type	=========================== // 

 function custom_post_type() {

 // Set UI labels for Custom Post Type
 	$labels = array(
 		'name'                => _x( 'Colaboradores', 'Post Type General Name', 'twentythirteen' ),
 		'singular_name'       => _x( 'Colaborador', 'Post Type Singular Name', 'twentythirteen' ),
 		'menu_name'           => __( 'Colaboradores', 'twentythirteen' ),
 		'parent_item_colon'   => __( 'Parent Colaboradores', 'twentythirteen' ),
 		'all_items'           => __( 'Todos os Colaboradores', 'twentythirteen' ),
 		'view_item'           => __( 'View Colaboradores', 'twentythirteen' ),
 		'add_new_item'        => __( 'Adicionar novo Colaborador', 'twentythirteen' ),
 		'add_new'             => __( 'Adicionar novo', 'twentythirteen' ),
 		'edit_item'           => __( 'Editar Colaborador', 'twentythirteen' ),
 		'update_item'         => __( 'Atualizar Colaborador', 'twentythirteen' ),
 		'search_items'        => __( 'Procurar Colaboradore', 'twentythirteen' ),
 		'not_found'           => __( 'Não encontrado', 'twentythirteen' ),
 		'not_found_in_trash'  => __( 'Não encontrado no lixo', 'twentythirteen' ),
 	);

 // Set other options for Custom Post Type

 	$args = array(
 		'label'               => __( 'Colaboradores', 'twentythirteen' ),
 		'description'         => __( 'Colaboradores', 'twentythirteen' ),
 		'labels'              => $labels,
 		// Features this CPT supports in Post Editor
 		'supports'            => array( 'title', 'editor', 'thumbnail' ),
 		// You can associate this CPT with a taxonomy or custom taxonomy.
 	// 	'taxonomies'          => array( 'genres' ),
 		/* A hierarchical CPT is like Pages and can have
 		* Parent and child items. A non-hierarchical CPT
 		* is like Posts.
 		*/ 		
 		'menu_icon'   		  => 'dashicons-businessman',
 		'hierarchical'        => false,
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_nav_menus'   => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => 1,
 		'can_export'          => true,
 		'has_archive'         => true,
 		'exclude_from_search' => false,
 		'publicly_queryable'  => true,
 		'capability_type'     => 'post',
 	);

 	// Registering your Custom Post Type
 	register_post_type( 'colaboradores', $args );		

 }

 add_action( 'init', 'custom_post_type', 0 );


// =========================== Custom post type	=========================== // 


function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );  

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 wp_enqueue_style('front-awasome', get_stylesheet_directory_uri() . '/fonts/font-awesome.min.css');

 wp_enqueue_style('ionicons', get_stylesheet_directory_uri() . '/fonts/ionicons.min.css');

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);

 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
