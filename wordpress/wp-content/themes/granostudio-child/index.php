<?php get_header(); ?>

<div class="container container-home">
    <div class="">        
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/home-icone-mundo.png" class="">
        <h2>Aumente seu mercado de atuação</h2>
        <p class="pre-titulo">Temos soluções específicas para <strong>distribuidores</strong> e <strong>fabricantes da área da Saúde</strong> e <strong>Hospitais</strong></p>
    </div>

    <div class="row">
        <select class="select" id="select">
            <option value="0">Escolha seu perfil</option>
            <option value="1">Fabricantes</option>
            <option value="2">Hospitais</option> 
            <option value="3">Distribuidores de Saúde</option>
        </select>

        <p><a href="/portugues/" class="btn btn-primary" id="botao-avancar">Avançar</a></p>
        <p><a href="/portugues/sobre/" class="link-sobre">Sobre a Hasten LLC</a></p>
    </div>
</div>  

<?php get_footer(); ?> 
 