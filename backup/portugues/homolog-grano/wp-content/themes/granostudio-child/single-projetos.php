<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>


<div class="container bounce animated area-projeto-interna">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post();

	$files_imagens_socias = get_post_meta( get_the_ID(), 'wiki_test_imagens_projeto', 1 );?>      
    
    <div class="row borda-bottom">
        <div class="col-sm-1">
            <p style="font-family:Muli, sans-serif;">Projeto</p>
        </div>
        <div class="col">
        	<select style="background-color:rgba(0,0,0,0);font-family:Muli, sans-serif;font-size:14;color:rgb(104,168,82);">
        		<optgroup label="Escolha um projeto">
        		
        		<?php
	             $args = array( 'post_type' => 'projetos');
	             $loop = new WP_Query( $args );

	             if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        			<a href="<?php echo get_the_permalink(); ?>"><option value="12"><?php echo get_the_title(); ?></option></a> 

        		<?php endwhile; // end of the loop. ?>
            	<?php endif; ?>	

        		</optgroup>
        	</select>
        </div>
    </div>

    <div class="row projetos-categorias">
        
        <div class="col">
        	<ul class="lista-categoria">
		        <?php
		          foreach((get_the_category()) as $category) {
		            echo '<li>' . $category->cat_name . '</li>';
		          }
		         ?>
	        </ul>
        </div>

    </div>

    <div class="row">
        
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">  

        	<?php 
			// Loop para banner
			$img_size = 'large';
			foreach ( (array) $files_imagens_socias as $attachment_id => $attachment_url ) {					
				
				echo wp_get_attachment_image( $attachment_id, $img_size );		

			}
			 ?>

        </div>

        <div class="col">
            <div class="row share-row">
                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4" style="font-family:Muli, sans-serif;">
                	<?php the_content(); ?>

                    <p>Compartilhe:&nbsp;
                    	<a href="#" class="share-icons"><i class="fa fa-facebook-f"></i></a>
                    	<a href="#" class="share-icons"><i class="fa fa-twitter"></i></a>
                    	<a href="#" class="share-icons"><i class="fa fa-linkedin"></i></a>
                    </p>

                </div>
            </div> 
        </div>

    </div>     

</div>


<?php endwhile; // end of the loop. ?>    

<?php get_footer(); ?>