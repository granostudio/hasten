<?php get_header(); ?>

<div class="indicador">
	<p>Você está aqui: Home  >  <span>Perfil: Hospitais</span></p>
</div>
	
<div class="banner-inicial">
	<div class="banner-foto banner-hospitais">
		<a href="/portugues/homolog-grano/fabricantes/" class="btn btn-primary">Fabricantes</a>
		<a href="/portugues/homolog-grano/hospitais/" class="btn btn-primary">Hospitais</a>
		<a href="/portugues/homolog-grano/distribuidores-de-saude/" class="btn btn-primary">Distribuidores de Saúde</a>
	</div>
</div>   

<div class="container">
	<div class="" style="text-align: center;margin-top: 50px;">
		<h2>Perfil: Hospitais</h2>
		<p style="font-size: 16px;">Clique nos botões abaixo e entenda como a Hasten LLC pode ajudar sua empresa</p>
	</div>

	<div class="div-botoes">
		<div class="botoes-perfil item1">
			<div>
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/seta-icon.png" class="">
			</div>
			<p>Entenda nossa <br>abordagem</p>
		</div>
		<div class="botoes-perfil item2">
			<div>
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/Network-icon.png" class="">
			</div>
			<p>Rede de <br>atuação</p>
		</div>
		<div class="botoes-perfil item3">
			<div>
				<img src="<?php echo get_stylesheet_directory_uri();?>/img/mais-icon.png" class="">
			</div>
			<p>Perfil Hospitais</p>
		</div>
	</div>

	<div class="content">
		<div class="content-out content1"> 
			<p><strong>Qual a abordagem da Hasten LLC Corp. para hospitais?</strong></p>
			<p>A Hasten LLC Corp. atua como elo entre hospitais que queiram adquirir produtos inovadores e marcas de fabricantes de Saúde globais. Acelera acesso aos mais recentes produtos e soluções de Saúde para garantir satisfação e segurança do paciente, redução de custos, melhoria de atendimento e, como consequência, garantir diferencial competitivo aos hospitais.</p>
		</div>
		<div class="content-out content2">
			<p><strong>Qual a rede de atuação da Hasten LLC Corp para hospitais?</strong></p>
			<p>A Hasten LLC Corp. possui uma rede global, com destaque para países como Estados Unidos, México, Costa Rica, Argentina, Chile, Peru, Colômbia, Equador, Caribe, Brasil e China, que permite o contato direto e indireto com empresas da Saúde de pequeno, médio e grande porte, que fornecem produtos e serviços altamente inovadores.</p>
		</div>
		<div class="content-out content3">
			<p><strong>Qual o perfil de hospitais que a Hasten LLC Corp. atende?</strong></p>
			<p>O modelo de negócios da Hasten LLC Corp. é direcionado a hospitais de todos os portes e especialidades que queiram ter acesso aos mais recentes produtos e soluções de Saúde de marcas dos Estados Unidos e Chilan - acrônimo para China e países da América Latina - com o objetivo de aumentar receita, diminuir custos e prestar um melhor e mais seguro atendimento ao paciente, obtendo, como consequência, um diferencial competitivo.</p>
		</div>
	</div>

	<div class="row">
		<p style="text-align: center;"><a href="/portugues/homolog-grano/sobre/" class="link-sobre">Sobre a Hasten LLC</a></p> 
	</div>

</div>



<?php get_footer(); ?>		