<?php get_header(); ?>


<div class="indicador">
	<p>Você está aqui: <span>Sobre a Hasten LLC Corp</span></p>
</div>

<div class="container div-1-sobre">
	
	<div class="col-sm-8">
		<h4>Sobre a<br> Hasten LLC Corp</h4>
		<p>A Hasten LLC Corp. é uma trading company que tem como meta auxiliar indústrias inovadoras do setor de Saúde a comercializarem seus produtos com segurança na Chilan - acrônimo criado para definir o mercado internacional entre China e países da América Latina.</p>
		<p>Fundada em 2014 por Eduardo de Melo, profissional com mais de 30 anos de experiência no setor de Saúde, a Hasten LLC Corp. conta com equipe de profissionais gabaritados, com mais de 20 anos de atuação na área, aptos a colaborar na inserção de produtos em outros países de forma ética, segura, sem custo fixo e com crescimento consistente. Tem como objetivo uma gestão comercial dinâmica, com perfil estratégico e analítico. É especializada em planejamento estratégico e desenvolvimento de oportunidades, oferecendo serviços que abrangem desde a estratégia às operações comerciais e logísticas.</p>
	</div>
  
	<div class="col-sm-4">
		<div class="col-6">
			<div class="row div-color-green">
				<p>Representamos</p>
				<p class="count">12</p>
				<p>marcas</p>
			</div>
			<div class="row div-color-green">
				<p>Presença em</p>
				<p class="count">30</p>
				<p>páises</p>
			</div>
		</div>
		<div class="col-6">
			<div class="row div-color-blue">
				<p>Mais de</p>
				<p class="count">250</p>
				<p>distribuidores</p>
			</div>
			<div class="row div-color-blue">
				<p>Mais de</p>
				<p class="count">40</p>
				<p style="margin-bottom: 0px;">colaboradores</p>
				<p style="font-size: 10px;">direitos e indireitos</p>
			</div>
		</div>		
	</div>
	
</div>

<div class="container div-2-sobre">
	<div class="titulo row" data-aos="fade-up">
		<h3>Ecossistema Hasten LLC Corp</h3>
		<p>Safe landing para fabricantes de Saúde e fonte de acesso a produtos e<br> soluções médicas inovadoras a distribuidores e hospitais</p>
	</div>

	<div class="row">
		<div class="col-sm-7">
			<div data-aos="fade-right"></div>
		</div>
		<div class="col-sm-5">
			<div data-aos="fade-left">
				<h3>A Hasten LLC Corp. nas palavras de Eduardo de Melo</h3>
				<p>Imagine um oceano de águas turbulentas, um mar agitado sem nenhuma área de pouso seguro. É assim que muitas empresas veem esses países. A turbulência é uma metáfora para o risco que associam aos negócios nessas regiões, ocasionado pelo alto custo fixo e pela falta de conhecimento das características de mercado e das relações de consumo. A Hasten LLC Corp. é uma plataforma no meio desse oceano turbulento, que garante uma
				área de safe landing (aterrissagem segura) para as empresas que querem fazer negócios na LatAm - além de ser fonte de acesso a produtos e soluções médicas inovadoras para hospitais e distribuidores certificados. Com suas soluções, é capaz de ajudar a indústria do setor de Saúde desde o desenvolvimento de novos produtos até a efetiva comercialização.</p>
			</div>
		</div>
	</div>
</div>

<div class="container div-3-sobre" data-aos="fade-up">
	<div class="titulo row">
		<h2>A Hasten LLC no mundo</h2>
		<p>Escritórios em cinco países e presença em cerca de 30 nações, com mais de 40 colaboradores diretos e indiretos.</p>
	</div>

	<div class="row">
		<div class="col-sm-9">
			<div class="bullet bullet-usa"></div>
			<div class="bullet bullet-br"></div>
			<div class="bullet bullet-mex"></div>
			<div class="bullet bullet-cost"></div>
			<div class="bullet bullet-chin"></div>
		</div>
		<div class="col-sm-3">
			<div class="endereco endereco-usa">
				<p>Estados Unidos</p>
				<p>1603 Capitol Ave., Suite 314-900, Cheyenne, WY, 82001</p>
				<p>Tel: +1-404-444-1207</p>
			</div>
			<div class="endereco endereco-br">
				<p>Brasil</p>
				<p>Rua Pe Francisco Arantes, 321 - Suite 602, Bairro Vila Paris, Belo Horizonte, MG</p>
				<p>Tel: +5531-3038-0730</p>
			</div>
			<div class="endereco endereco-mex">
				<p>México</p>
				<p>Avenida División del Norte, 2723, despacho 201, Col. San Lucas Delegación Coyoacán, Ciudad de México</p>
				<p>Tel: +5255-5549-5961</p>
			</div>
			<div class="endereco endereco-cost">
				<p>Costa Rica</p>
				<p>Mercedes Norte Residencial Zumlo, Casa Esquinera, 119</p>
				<p>Tel: +506-2260-1045 / +506-87125500</p>
			</div>
			<div class="endereco endereco-chin">
				<p>China</p>
				<p>Rm.516, Tianlong Building,378 Zhujiang South Road, Suzhou, Jiangsu</p>
				<p style="color: #55a54a">Tel: +86-512-67951120</p>
				<p>Fax: +86-512-66232972</p>
			</div>
			<p><a href="/portugues/homolog-grano/contato/" class="btn btn-primary">Entrar em contato</a></p>
		</div>
	</div>
</div>

<?php get_footer(); ?>		