<?php get_header(); ?>

<div class="container div-3-sobre" data-aos="fade-up" style="margin-top: 100px;">
	<div class="titulo row">
		<h2>A Hasten LLC no mundo</h2>
		<p>Escritórios em cinco países e presença em cerca de 30 nações, com mais de 40 colaboradores diretos e indiretos.</p>
	</div>

	<div class="row">
		<div class="col-sm-9">
			<div class="bullet bullet-usa"></div>
			<div class="bullet bullet-br"></div>
			<div class="bullet bullet-mex"></div>
			<div class="bullet bullet-cost"></div>
			<div class="bullet bullet-chin"></div>
		</div>
		<div class="col-sm-3">
			<div class="endereco endereco-usa">
				<p>Estados Unidos</p>
				<p>1603 Capitol Ave., Suite 314-900, Cheyenne, WY, 82001</p>
				<p>Tel: +1-404-444-1207</p>
			</div>
			<div class="endereco endereco-br">
				<p>Brasil</p>
				<p>Rua Pe Francisco Arantes, 321 - Suite 602, Bairro Vila Paris, Belo Horizonte, MG</p>
				<p>Tel: +5531-3038-0730</p>
			</div>
			<div class="endereco endereco-mex">
				<p>México</p>
				<p>Avenida División del Norte, 2723, despacho 201, Col. San Lucas Delegación Coyoacán, Ciudad de México</p>
				<p>Tel: +5255-5549-5961</p>
			</div>
			<div class="endereco endereco-cost">
				<p>Costa Rica</p>
				<p>Mercedes Norte Residencial Zumlo, Casa Esquinera, 119</p>
				<p>Tel: +506-2260-1045 / +506-87125500</p>
			</div>
			<div class="endereco endereco-chin">
				<p>China</p>
				<p>Rm.516, Tianlong Building,378 Zhujiang South Road, Suzhou, Jiangsu</p>
				<p style="color: #55a54a">Tel: +86-512-67951120</p>
				<p>Fax: +86-512-66232972</p>
			</div>
			<p><p data-toggle="modal" data-target="#modal-contato" class="btn btn-primary">Entrar em contato</a></p>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal-contato" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content modal-contato">

      	<div class="container">
      		<div class="row">

      			<div class="col-sm-6 col-sm-offset-3">
		
				<h2>Entre em contato pelo formulário!</h2>

				<?php echo do_shortcode('[contact-form-7 id="47" title="Contato"]'); ?>	

				</div>
			
			</div>
			
		</div>

		<p class="sair" data-dismiss="modal">< Voltar</p>

      </div>
	</div>
</div>

<?php get_footer(); ?>		