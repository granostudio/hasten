<?php get_header(); ?>


<div class="container margem-t-40">
    <div class="row">
        <div class="col">
            <div class="jumbotron bounce animated" style="height:400px;font-family:Muli, sans-serif;padding:120px 60px;">
                <h1>Heading text</h1>
                <p>Nullam id dolor id nibh ultricies vehicula ut id elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam.</p>
                <p><a class="btn btn-primary" role="button" href="#">Saiba Mais</a></p>
            </div>
            <nav class="navbar navbar-light navbar-expand-md navigation-clean-search" data-aos="fade" style="background-color:rgba(250,250,250,0);">
                <div class="container"><a class="navbar-brand" href="#" style="font-family:Muli, sans-serif;font-size:16px;">Categorias</a><button class="navbar-toggler d-none d-sm-none" data-toggle="collapse" data-target="#navcol-1" style="width:auto;background-color:transparent;"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
                    <div
                        class="collapse navbar-collapse" id="navcol-1" style="font-family:Muli, sans-serif;">
                        <ul class="nav navbar-nav">
                            <li class="nav-item" role="presentation"><a class="nav-link active" href="#">Link 1</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="#">Link 2</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href="#">Link 3</a></li>
                        </ul>
                        <form class="form-inline ml-auto" target="_self">
                            <div class="form-group"><label for="search-field"><i class="fa fa-search" style="color:#68a852;"></i></label><input class="form-control search-field" type="search" name="search" id="search-field" style="font-family:Muli, sans-serif;"></div>
                        </form><a class="btn btn-light action-button" role="button" href="#" style="font-family:Muli, sans-serif;background-color:#68a852;">Buscar</a></div>
        </div>
            </nav>
        </div>
    </div>
</div>


<?php get_footer(); ?>		