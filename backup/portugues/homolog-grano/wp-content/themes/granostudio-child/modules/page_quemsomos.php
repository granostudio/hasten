<?php get_header(); ?>

<div class="indicador">
	<p>Você está aqui: <span>Quem somos</span></p>
</div>


<div class="container div-1-quemsomos">
	
	<div class="col-sm-6" style="margin-bottom: 30px;" data-aos="fade-up">
		<h4>Quem somos</h4>
		<p style="font-size: 16px;line-height: 22px;">A Hasten LLC Corp tem como objetivo criar possibilidades para que empresas inovadoras da área de Saúde conquistem e divulguem seu potencial máximo a outros mercados mundiais. Nossa equipe é formada por profissionais com mais de 30 anos de experiência em Saúde e gestão, oferecendo soluções essenciais, networking e troca de informações específicas a fim de abrir novos mercados para produtos, tecnologias e serviços.</p>
	</div>

	<div class="col-sm-6" data-aos="fade-up">

		<?php
         $args = array( 'post_type' => 'colaboradores', 'orderby' => 'date', 'order' => 'ASC');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>
		
		<div class="col-sm-6 col-md-6 col-lg-4 descricao-pessoas" data-toggle="modal" data-target="#<?php echo $post_id?>">
			<img src="http://placehold.it/250x250" alt="">
			<div class="descricao">
				<p><?php echo get_the_title(); ?></p>
				<p><?php the_field('cargo_colaborador'); ?></p>
			</div>
			<p class="email"><?php the_field('email_colaborador'); ?></p>
			<p><img src="<?php echo get_stylesheet_directory_uri();?>/img/plus2.png" class="">Ler bio</p>
		</div>

		<?php endwhile; // end of the loop. ?>
        <?php endif; ?>

	</div>

		<?php
         $args = array( 'post_type' => 'colaboradores', 'orderby' => 'date', 'order' => 'ASC');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


		<!-- Modal -->
	  	<div class="modal fade" id="<?php echo $post_id?>" role="dialog">
		    <div class="modal-dialog modal-lg">
		      <div class="modal-content">

				<div class="indicador">
					<p>Você está aqui: Quem somos > <span><?php echo get_the_title(); ?></span></p>
				</div>

				<div class="col-sm-5">
					<img src="http://placehold.it/500x500" alt="">
				</div>
				<div class="col-sm-7">
					<p><?php echo get_the_title(); ?></p>
					<p><?php the_field('cargo_colaborador'); ?></p>
					<p class="email"><?php the_field('email_colaborador'); ?></p>
					<p><?php the_content(); ?></p>
				</div>

				<p class="sair" data-dismiss="modal">< Voltar</p>	

		      </div>
			</div>
		</div>

		<?php endwhile; // end of the loop. ?>
        <?php endif; ?>


</div>


<?php get_footer(); ?>		