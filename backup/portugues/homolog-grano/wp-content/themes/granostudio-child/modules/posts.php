<?php

/**
* Módulo:
* ***** Posts - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_posts(){ ?>
        <div class="article-list">
            <div class="container" data-aos="fade-up">
                <div class="row articles">

                    <?php
                     $args = array( 'post_type' => 'post', 'posts_per_page' => 6); 
                     $loop = new WP_Query( $args );

                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


                    <div class="col-sm-6 col-md-4 item">
                        <a href="<?php echo get_the_permalink(); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" class="img-responsive post-thumbnail">
                        </a> 
                        <h3 class="name"><?php echo get_the_title(); ?></h3>
                        <p class="description"><?php echo the_excerpt_max_charlength(200); ?></p>
                        <a href="<?php echo get_the_permalink(); ?>" class="action" style="background-color:#68a852;">
                            <i class="fa fa-angle-right" style="color:rgb(246,248,251);font-size:17px;padding:0px;/*line-height:-26px;*/"></i>
                        </a>
                    </div>

                    <?php endwhile; // end of the loop. ?>
                    <?php endif; ?>

                </div>
            </div>   
        </div>     

<?php } ?>
