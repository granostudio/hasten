<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<?php if (is_search()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- Page Content -->
    <div class="container">

        <div>

            <!-- Blog Entries Column -->
            <div class="" style="margin-top: 140px;">

                <div>
                    <h2 class="page-header header-search">
                        Você pesquisou por: 
                        <?php printf( __( '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> %s', 'twentysixteen' ), '<span style="color: #0D2D25;">' . esc_html( get_search_query() ) . '</span>' ); ?>
                    </h2>
                    
                </div>

                <div class="row articles">

                    <?php
                    if( have_posts() ) {
                      while ( have_posts() ) {
                        the_post(); ?>
                        
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="col-sm-4 posts">
                                <div class="img-thumb" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                                <div class="content">
                                    <ul class="lista-categoria">
                                    <?php
                                      foreach((get_the_category()) as $category) {
                                        echo '<li class="hashtags">' . $category->cat_name . '</li>';
                                      }
                                     ?>
                                    </ul> 
                                    <p><?php echo get_the_title(); ?></p>
                                </div>
                                <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
                            </div>
                        </a>                                           


                      <?php } ?>

                        <!-- Pager -->
                        <ul class="pager row">

                            <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                            <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                        </ul>

                        <div style="margin: 0px 15px;">
                            <h4 style="float: left;">Não encontrou o que desejava?</h4>
                            <form class="form-inline mr-auto form-pesquisa form-pesquisa-erro" role="search" method="get" id="searchform">
                                <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                    <div class="form-group" style="padding: 0;margin-top: 20px;">
                                        <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s" style="padding: 0;">
                                    </div>
                                </form>
                            </form>
                        </div>

                    <?php } else { ?>

                        <div class="else-busca">
                    
                            <h4 style="float: left;">Não há resultados encontrados, tente fazer uma nova pesquisa:</h4>

                            <form class="form-inline mr-auto form-pesquisa form-pesquisa-erro" role="search" method="get" id="searchform">
                                <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
                                    <div class="form-group" style="padding: 0;margin-top: 20px;">
                                        <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s" style="padding: 0;">
                                    </div>
                                </form>
                            </form>

                        </div>

                    <?php } ?>
                    
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
