$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


// bxslider crousel ===========================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });


// Back to Top ================================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================

// /ação do select principal ==================================

$('#select').change(function(){
  if($(this).val() == '0'){
    $('#botao-avancar').attr("href","/portugues/homolog-grano/");
  }
  if($(this).val() == '1'){
    $('#botao-avancar').attr("href","/portugues/homolog-grano/fabricantes/");
  }
  if($(this).val() == '2'){
    $('#botao-avancar').attr("href","/portugues/homolog-grano/hospitais/");
  }
  if($(this).val() == '3'){
    $('#botao-avancar').attr("href","/portugues/homolog-grano/distribuidores-de-saude/");
  }
});

// /ação do select principal ==================================

// /Efeito Contator ===========================================

$('.count').each(function (i) {
    var bottom_of_object = $(this).position().top + $(this).outerHeight();
    var bottom_of_window = $(window).scrollTop() + $(window).height();
    /* se o objeto estiver completamente "scrollado" pra dentro da janela, fazer o fade_in*/
    if (bottom_of_window > bottom_of_object) {
      $(this).prop('Counter',0).animate({
           Counter: $(this).text()
      }, {
          duration: 5000,
          easing: 'swing',
          step: function (now) {
          $(this).text(Math.ceil(now));
          }
        });                       
    }
});

// /Efeito Contator ===========================================

// /Ações dos botões das páginas de perfis ====================

jQuery(' .item1 ').click(function() {
  $( ".content1" ).fadeIn( "slow" );
  $( ".content2" ).fadeOut( "slow" );
  $( ".content3" ).fadeOut( "slow" );
  $( ".content4" ).fadeOut( "slow" );  
  $( ".item1 div" ).css({"background-color": "#135581"});
  $( ".item2 div" ).css({"background-color": "#55a54a"});
  $( ".item3 div" ).css({"background-color": "#55a54a"});
  $( ".item4 div" ).css({"background-color": "#55a54a"});  
});
jQuery(' .item2 ').click(function() {
  $( ".content1" ).fadeOut( "slow" );
  $( ".content2" ).fadeIn( "slow" );
  $( ".content3" ).fadeOut( "slow" );
  $( ".content4" ).fadeOut( "slow" );  
  $( ".item2 div" ).css({"background-color": "#135581"});
  $( ".item1 div" ).css({"background-color": "#55a54a"});
  $( ".item3 div" ).css({"background-color": "#55a54a"});
  $( ".item4 div" ).css({"background-color": "#55a54a"});  
});
jQuery(' .item3 ').click(function() {
  $( ".content1" ).fadeOut( "slow" );
  $( ".content2" ).fadeOut( "slow" );
  $( ".content3" ).fadeIn( "slow" );
  $( ".content4" ).fadeOut( "slow" );  
  $( ".item3 div" ).css({"background-color": "#135581"});
  $( ".item1 div" ).css({"background-color": "#55a54a"});
  $( ".item2 div" ).css({"background-color": "#55a54a"});
  $( ".item4 div" ).css({"background-color": "#55a54a"});
});
jQuery(' .item4 ').click(function() {
  $( ".content1" ).fadeOut( "slow" );
  $( ".content2" ).fadeOut( "slow" );
  $( ".content3" ).fadeOut( "slow" );
  $( ".content4" ).fadeIn( "slow" );  
  $( ".item4 div" ).css({"background-color": "#135581"});
  $( ".item1 div" ).css({"background-color": "#55a54a"});
  $( ".item2 div" ).css({"background-color": "#55a54a"});
  $( ".item3 div" ).css({"background-color": "#55a54a"});
});


// /Ações dos botões das páginas de perfis ====================

// /Ações dos clicks dos endereços ============================

$(".bullet-usa").click(function(){  
  $(this).css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});
  $(".endereco-usa").css({"opacity": "1"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
});

$(".bullet-br").click(function(){ 
  $(this).css({"background-color": "#135581"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"}); 
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-br").css({"opacity": "1"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
});

$(".bullet-mex").click(function(){  
  $(this).css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});  
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": "1"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
});

$(".bullet-cost").click(function(){  
  $(this).css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});  
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": "1"});
  $(".endereco-chin").css({"opacity": ".4"});
});

$(".bullet-chin").click(function(){ 
  $(this).css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"}); 
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": "1"});
});

$(".endereco-usa").click(function(){
  $(this).css({"opacity": "1"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
  $(".bullet-usa").css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});   
});

$(".endereco-br").click(function(){
  $(this).css({"opacity": "1"});
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
  $(".bullet-br").css({"background-color": "#135581"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});   
});

$(".endereco-mex").click(function(){
  $(this).css({"opacity": "1"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
  $(".bullet-mex").css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});   
});

$(".endereco-cost").click(function(){
  $(this).css({"opacity": "1"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-usa").css({"opacity": ".4"});
  $(".endereco-chin").css({"opacity": ".4"});
  $(".bullet-cost").css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"});
  $(".bullet-chin").css({"background-color": "#55a54a"});   
});

$(".endereco-chin").click(function(){
  $(this).css({"opacity": "1"});
  $(".endereco-br").css({"opacity": ".4"});
  $(".endereco-mex").css({"opacity": ".4"});
  $(".endereco-cost").css({"opacity": ".4"});
  $(".endereco-usa").css({"opacity": ".4"});
  $(".bullet-chin").css({"background-color": "#135581"});
  $(".bullet-br").css({"background-color": "#55a54a"});
  $(".bullet-mex").css({"background-color": "#55a54a"});
  $(".bullet-cost").css({"background-color": "#55a54a"});
  $(".bullet-usa").css({"background-color": "#55a54a"});   
});

// /Ações dos clicks dos endereços ============================


// /Animação seta ============================================= 

  $('.img-seta').on('click', function(){
      $('html, body').animate({
          scrollTop: $('.div-2-saudehumana').offset().top -40
      }, 500);
    });

// /Animação seta =============================================  


// /Função para a barra de progresso de leitura ===============

window.onscroll = function() {myFunction()};

function myFunction() {
  var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
  var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
  var scrolled = (winScroll / height) * 100;
  document.getElementById("myBar").style.width = scrolled + "%";
}

// /Função para a barra de progresso de leitura ===============


// /adição de classes no elemento =============================

  $(".attachment-large").each(function(){
        $(this).attr("class","img-responsive img-projetos margem-b-5");
  });

// /adição de classes no elemento =============================


// Grano Scroll ANIMATION =====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 } 

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ===================================

AOS.init({
  disable: 'mobile',
  duration: 1200
});


}); 


       