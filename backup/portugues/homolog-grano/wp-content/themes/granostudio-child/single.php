<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>
    

<?php if (is_single()) { ?> 
  
<style type="text/css">
  #menu-item-462 a{
    color: #68A852;
  }
  .progress-container{
    display: block;
  }
  .navbar{
    border: none;
  }
</style>

<?php } ?>

<div class="container"> 


    <!-- Blog Post Content Column -->
    <div class="single-blog">

        <!-- Blog Post -->

        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


        <!-- Post Content -->

        <div class="row bounce animated padding-10">
            <div class="col-md-9">
                <h1><?php echo get_the_title(); ?></h1>
                <p class="lead margem-t-20"></p>
                
                <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" width="100%">

                <div class="row">
                    <ul class="lista-categoria" style="padding: 15px 15px 0;">
                        <p style="float: left;font-size: 14px;margin-right: 5px;padding-top: 5px;">Categorias: </p>
                        <?php
                          foreach((get_the_category()) as $category) {
                            echo '<li>' . $category->cat_name . '</li>';
                          }
                         ?>
                    </ul>
                </div>

                <div class="margem-t-20 content-single"><?php the_content(); ?></div>
               
            </div>
            <div class="col"></div>
        </div>


        <div class="article-list">
            <div class="container" data-aos="fade-up">
                <div class="intro"></div>
                <div class="row margem-t-b-20 borda-bottom">
                    <div class="col">
                        <h4>Últimas postagens</h4>
                        <hr>
                    </div>
                </div>

                <div class="row articles">

                    <?php
                     $args = array( 'post_type' => 'post', 'posts_per_page' => 3);
                     $loop = new WP_Query( $args );

                     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>


                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="col-sm-4 posts">
                                <div class="img-thumb" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                                <div class="content">
                                    <ul class="lista-categoria">
                                    <?php
                                      foreach((get_the_category()) as $category) {
                                        echo '<li class="hashtags">' . $category->cat_name . '</li>';
                                      }
                                     ?>
                                    </ul> 
                                    <p><?php echo get_the_title(); ?></p>
                                </div>
                                <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary">Saiba mais</a>
                            </div>
                        </a>

                    <?php endwhile; // end of the loop. ?>
                    <?php endif; ?>

                </div>
            </div>

        </div>

    </div>

    <?php endwhile; // end of the loop. ?>
        

</div>
<!-- /.container -->

<?php get_footer(); ?>
