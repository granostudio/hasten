<?php

/**
* Módulo:
* ***** Externo !Exemplo! - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */
 ?>
<!-- NOME DO SEU MODULO  -->

<div class="header-home">

  <?php
  // social links
  $social_media = get_option('info_contato');
  // remover elementos que não irão aparecer no header
  if(!empty($social_media['cont_googlepmaps'])){
      unset($social_media['cont_googlepmaps']);
  }
  if(!empty($social_media['cont_telefone'])){
      unset($social_media['cont_telefone']);
  }
  if(!empty($social_media['cont_email'])){
      unset($social_media['cont_email']);
  }
  if(!empty($social_media['cont_endereço'])){
      unset($social_media['cont_endereço']);
  }

  if (!empty($social_media)) {
    ?>
  <ul class="navbar-social navbar-right social2">
    <li class="newslettler" id="newslettler"><i class="fa fa-address-card-o fa-lg" aria-hidden="true"></i>NEWSLETTLER</li>
    <?php foreach ($social_media as $key => $value) {

            switch ($key) {
              case 'cont_facebook':
                if($value!='Default Text'){
                  echo '<li class="icon facebook"><a href="'.$value.'" target="_blank">Facebook</a></li>';
                }
              break;
              case 'cont_twitter':
                if($value!='Default Text'){
                  echo '<li class="icon twitter"><a href="'.$value.'" target="_blank">Twitter</a></li>';
                }
              break;
              case 'cont_instagram':
                if($value!='Default Text'){
                  echo '<li class="icon instagram"><a href="'.$value.'" target="_blank">Instagram</a></li>';
                }
              break;
              case 'cont_youtube':
                if($value!='Default Text'){
                  echo '<li class="icon youtube"><a href="'.$value.'" target="_blank">Youtube</a></li>';
                }
              break;
              case 'cont_flickr':
                if($value!='Default Text'){
                  echo '<li class="icon flickr"><a href="'.$value.'" target="_blank">Flickr</a></li>';
                }
              break;
              case 'cont_linkedin':
                if($value!='Default Text'){
                  echo '<li class="icon linkedin"><a href="'.$value.'" target="_blank">Linkedin</a></li>';
                }
              break;
              case 'cont_googleplus':
                if($value!='Default Text'){
                  echo '<li class="icon googleplus"><a href="'.$value.'">Google +</a></li>';
                }
              break;
            }
    } ?>

    <?php
    // Link para contato
    $moduloContato = false;
    if (is_page()) {
      // verificar se página tem o modulo de contato
      $group = get_post_meta( get_the_ID(), 'page_layout', true );
      for ($i=0; $i < count($group); $i++) {
        if($group[$i]['page_modulo'] == 'Contato'){
          $moduloContato = true;
        }
      }
    }
    if($moduloContato){
      echo '<li class="icon contato"><a href="#contato" target="_blank">Contato</a></li>';
    }else{
      echo '<li class="icon contato"><a href="contato" target="_blank">Contato</a></li>';
    }
     ?>
    </ul>
    <?php
    }
    ?>


    <?php
        wp_nav_menu( array(
            'menu'              => 'primary',
            'theme_location'    => 'primary',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'menu',
            'container_id'      => '',
            'menu_class'        => 'nav navbar-nav hidden-xs',
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'walker'            => new wp_bootstrap_navwalker())
        );
    ?>

    <a href="#" class="buscar buscar2">
      <i class="fa fa-search" aria-hidden="true"></i>
    </a>

</div>

<div class="banner-home hidden-xs">
  <div class="content-quemsomos">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/banner-logo.png" alt=""><br/>
    <a href="http://www.gessaude.com.br/quem-somos2/" class="btn btn-primary">Quem Somos</a>
  </div>
    <div class="col-1">
      <div class="bg"></div>
      <div class="mask"></div>
    </div>
    <div class="col-2">
      <div class="linha-1">
        <div class="bg"></div>
        <div class="mask"></div>
        <div class="content">
          <div class="barra-vermelha"></div>
          <h3><a href="#"><span>PRO</span>AMA</a></h3>
          <p><span>
            Programa de <strong>Aceleração</strong><br/>da <strong>Maturidade</strong><br/>de <strong>Gestão da Saúde</strong>
          </span></p>
          <a href="http://www.gessaude.com.br/proama/" class="btn btn-primary">
            Leia Mais
          </a>
        </div>
      </div>
      <div class="linha-2">
        <div class="mask"></div>
        <div class="content">
          <h3><a href="#">Maturidade de<br />Gestão Hospitalar</a></h3>
          <a href="http://www.gessaude.com.br/maturidade-de-gestao-hospitalar/" class="btn btn-primary">
            Leia Mais
          </a>
        </div>
        <canvas></canvas>
      </div>
    </div>
  </div>
</div>
