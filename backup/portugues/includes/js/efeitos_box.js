$(function(){
	$('.box-geral').data("status",false).click(function(){
		var elem=$(this);
		var classe=elem.attr("class").replace(" box-geral","");		
		
		if(!elem.data("status")){
			$('p.'+classe).slideDown(500,function(){
				
				elem.data("status",true);
			}); 
		}
		else{
			
			$('p.'+classe).slideUp(500,function(){
				elem.data("status",false);
			});
		}
	})
})