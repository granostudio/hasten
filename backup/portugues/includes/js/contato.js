
$(function(){
	$("input[name='telefone']").mask("9999-9999?9")  
        .live('focusout', function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');  
            element = $(target);  
            element.unmask();  
            if(phone.length > 8) {  
                element.mask("99999-999?9");  
            } else {  
                element.mask("9999-9999?9");  
            }  
        }); 
	//FORM SUBMIT
	$("form").submit(function(event){
    	event.preventDefault();
		
	
		//RESETA AVISOS
		$("input:text,input:radio,textarea,select").css("background","#FFF");
		$("span.erro2").hide();
		
		var nome=$("input[name='nome']").val();
		var ddd=$("input[name='ddd']").val();
		var telefone=$("input[name='telefone']").val();
		var email=$("input[name='email']").val();
		var mensagem=$("textarea[name='mensagem']").val();
		var action=$("input[name='action']").val();

		$.post("includes/php/validacao_contato.php",{nome:nome,ddd:ddd,telefone:telefone,email:email,mensagem:mensagem,action:action,method:"ajax"},function(data){																									
			var retorno=data.split("|||");
			var campo=retorno[0];
			var mensagem=retorno[1];
			if(campo=="ok"){
				alert(mensagem);
				document.location.assign("contato.php");
			}
			else if(campo=="falha")
				alert(mensagem);
			else{
				erros(campo,mensagem);
				return false;
			}
		});
	});
});

function erros(campo, mensagem){
	if(campo=="mensagem")
		$("textarea[name='"+campo+"']").css("background","#FFFF99").focus();
	else
		$("input[name='"+campo+"']").css("background","#FFFF99").focus();
	$("span.erro2").html(mensagem).fadeIn("fast");
}

function soNum(inputData, e){
	if(document.all) // Internet Explorer
		var tecla = event.keyCode;
	else //Outros Browsers
		var tecla = e.which;
	
	if(!(tecla > 47 && tecla < 58)&&tecla!=8&&tecla!=0){
		alert("Digite somente n�meros");
		return false;
	}
}