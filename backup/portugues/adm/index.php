<?php
	$folder='administrador';
	define('FOLDER',$folder);

	define("NMIND",true);
	include 'includes/php/loader.php';
	include_once($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/administracao/notificacao.class.php");


	if(!empty($_SESSION['admin']['login'])&&$_SESSION['admin']['login']) header("Location: sistema.php");
	include 'includes/php/administrador/login.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<!-- METATAGS -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="content-language" content="pt-br" />

        <meta name="rating" content="general" />
        <meta name="author" content="New Mind Comunicacao - newmind.com.br" />
        <meta name="author" content="Programador - Edwin B. Pancoti" />

        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="googlebot" content="all, follow" />
        <meta name="robots" content="all, follow" />
        <meta name="language" content="Portugues" />

        <!-- TÍTULO -->
        <title>Hasten - Administração</title>

        <!-- IMPORTAÇÃO DE FOLHAS DE ESTILO -->
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
        <link href="includes/css/login.css" rel="stylesheet" type="text/css" />
        <link href="includes/css/colorbox.css" rel="stylesheet" type="text/css" />

        <!-- SCRIPTS GLOBAIS PARA O SITE -->
        <script src="includes/scripts/jquery.js" type="text/javascript"></script>
        <script src="includes/scripts/navegacao.js" type="text/javascript"></script>
        <script src="includes/scripts/maskedinput.js" type="text/javascript"></script>
        <script src="includes/scripts/colorbox.js" type="text/javascript"></script>
    </head>
    <body>
		<div id="login">
        	<div id="topo">
            	<?php
            		$logo='<img src="imagens/cliente/hasten_home.png" alt="Hasten" />';
					echo (!empty($_GET['login'])&&$_GET['login']==='cadastro')?'<a href="index.php">'.$logo.'</a>':$logo;
				?>
			</div>

            <div id="conteudo">
            	<form action="" method="post" <?php if(!empty($_GET['login'])&&$_GET['login']==='cadastro') echo 'id="cadastro"'; ?>>
					<?php
						$notificacao=new notificacao();
						$notificacao->getNotificacao();
					?>
					<h2>Dados de acesso:</h2>

                    <label>Usuário:</label>
                    <input type="text" name="usuario" value="<?php echo isset($_POST['usuario'])?$_POST['usuario']:NULL; ?>" maxlength="14" />

                    <label>Senha:</label>
                    <input type="password" name="senha" value="<?php echo isset($_POST['codigo'])?$_POST['codigo']:NULL; ?>" maxlength="18" />

                    <input type="hidden" name="action" value="administrador_login" />
                    <input type="submit" value="Acessar" />
                </form>
        	</div>
        </div>
    </body>
</html>
