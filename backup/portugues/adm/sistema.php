<?php

	include_once($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/administracao/notificacao.class.php");
  include_once($_SERVER['DOCUMENT_ROOT']."portugues/adm/includes/php/administrador/modulo/artigo.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    	<!-- METATAGS -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="content-language" content="pt-br" />

        <meta name="rating" content="general" />
        <meta name="author" content="New Mind Comunicacao - newmind.com.br" />
        <meta name="author" content="Programador - Edwin B. Pancoti" />

        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="googlebot" content="all, follow" />
        <meta name="robots" content="all, follow" />
        <meta name="language" content="Portugues" />

        <!-- TÍTULO -->
        <title>Hasten - Administração</title>

        <!-- IMPORTAÇÃO DE FOLHAS DE ESTILO -->
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
        <link href="includes/css/sistema.css" rel="stylesheet" type="text/css" />
        <link href="includes/css/colorbox.css" rel="stylesheet" type="text/css" />

        <!-- SCRIPTS GLOBAIS PARA O SITE -->
        <script src="includes/scripts/jquery.js" type="text/javascript"></script>
        <script src="includes/scripts/jquery-ui.js" type="text/javascript"></script>
        <script src="includes/scripts/jquery-slider.js" type="text/javascript"></script>

        <script src="includes/scripts/maskedMoney.js" type="text/javascript"></script>
        <script src="includes/scripts/maskedinput.js" type="text/javascript"></script>
        <script src="includes/scripts/navegacao.js" type="text/javascript"></script>
        <script src="includes/scripts/colorbox.js" type="text/javascript"></script>

        <!-- SCRIPTS DE FUNÇÕES IMPLEMENTADAS -->
        <script src="includes/scripts/nicedit/nicEdit.js" type="text/javascript"></script>
        <!--<script src="includes/scripts/upload/upload.js" type="text/javascript"></script>-->

        <script src="includes/scripts/datepicker.js" type="text/javascript"></script>
        <link href="http://code.jquery.com/ui/1.8.21/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    	<div id="menu_back"></div>
        <div id="menu">
            <img src="imagens/cliente/hasten.png" width="180" alt="BCH" />

            <h2 id="ola">Olá <?php echo $_SESSION['admin']['nome']; ?>,<br />Você não tem novas notificações</h2>

            <div class="links_rapidos">
                <a href="includes/php/administrador/logout.php" title="Ir para o site">Sair</a>
                <span>|</span>
                <a href="sistema.php" title="Home">Home</a>
            </div>

            <div class="modulos">
            	<?php /*
            	<div class="modulo">
                	<h2>Perfil</h2>
                    <ul>
                    	<li <?php echo (!empty($_GET['modulo'])&&$_GET['modulo']==="perfil")&&empty($_GET['acao'])?"class='ativo'":""; ?>><a href="sistema.php?modulo=perfil" title="Meus dados">Meus dados</a></li>
                        <li <?php echo (!empty($_GET['modulo'])&&$_GET['modulo']==="perfil")&&(!empty($_GET['acao'])&&$_GET['acao']==="senha")?"class='ativo'":""; ?>><a href="sistema.php?modulo=perfil&acao=senha" title="Meus dados">Alterar senha</a></li>
                    </ul>
                </div>
                */ ?>

                <div class="modulo">
                	<h2>Artigos</h2>
                    <ul>
                    	<li <?php echo (!empty($_GET['modulo'])&&$_GET['modulo']==="artigo")&&(empty($_GET['acao']))?"class='ativo'":""; ?>><a href="sistema.php?modulo=artigo" title="Cadastrar">Cadastrar</a></li>
                    	<li <?php echo (!empty($_GET['modulo'])&&$_GET['modulo']==="artigo")&&(!empty($_GET['acao'])&&$_GET['acao']==='gerenciar')?"class='ativo'":""; ?>><a href="sistema.php?modulo=artigo&acao=gerenciar" title="Gerenciar">Gerenciar</a></li>
                        <li <?php echo (!empty($_GET['modulo'])&&$_GET['modulo']==="artigo_comentario")&&(!empty($_GET['acao'])&&$_GET['acao']==="gerenciar")?"class='ativo'":""; ?>><a href="sistema.php?modulo=artigo_comentario&acao=gerenciar" title="Comentários">Comentários</a></li>
                    </ul>
                </div>
            </div>
        </div>

		<div id="geral">
            <h2 class="bem_vindo">Bem-Vindo!</h2>
            <h3 class="o_que_fazer">O que você deseja fazer?</h3>

            <noscript><div class="notificacao erro"><span>O JavaScript de seu navegador encontra-se desabilitado e pode impedir o funcionamento de alguns recursos, habilite-o e <a href="<?php echo basename($_SERVER['SCRIPT_NAME']); ?>">clique aqui</a> para recarregar a página.</span></div></noscript>

            <div id="atalhos">
            	<?php
					if(!empty($_GET['modulo']))
						echo '<a href="index.php" title=""><img src="imagens/icones/modulos/home.png" alt="" /><span>Voltar para<br />Home</span></a>';

					if(!(!empty($_GET['modulo'])&&$_GET['modulo']==="artigo"&&!empty($_GET['acao'])&&$_GET['acao']==='gerenciar'))
						echo '<a href="sistema.php?modulo=artigo&acao=gerenciar" title=""><img src="imagens/icones/modulos/artigos.png" alt="" /><span>Gerenciar<br />Artigos</span></a>';

					if(!(!empty($_GET['modulo'])&&$_GET['modulo']==="artigo"&&empty($_GET['acao'])))
						echo '<a href="sistema.php?modulo=artigo" title=""><img src="imagens/icones/modulos/novo_item.png" class="novo" alt="" /><img src="imagens/icones/modulos/artigos.png" alt="" /><span>Adicionar<br />Artigo</span></a>';
				?>
            </div>

            <div id="conteudo">
            	<?php
					$notificacao=new notificacao();
					$notificacao->getNotificacao();

                	if(isset($_SESSION['retorno'])&&$_SESSION['retorno']!=""){ echo $_SESSION['retorno']; unset($_SESSION['retorno']); }
					else if(!empty($retorno)) echo '<div class="notificacao alerta"><span>Existem erros de preenchimento no formulário, verifque os campos abaixo:</span></div>';

					echo '<div class="tabela">';

						//MÓDULO
						if(!empty($_GET['modulo'])) include 'includes/php/'.$folder.'/modulo/'.$_GET['modulo'].'.php';
						else echo '<div class="tab_content">Navegue pelo menu lateral para gerenciar o conteúdo do site.</div>';

					echo '</div>';
				?>
            </div>
        </div>
    </body>
</html>
