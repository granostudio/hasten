<?php
	class email{
		private $quebra;
		
		private $envio_nome;
		private $envio_email;
		
		private $destino_nome;
		private $destino_email;
		
		private $resposta_nome;
		private $resposta_email;
		
		private $copia;
		private $copia_oculta;
		
		private $anexo="";
		private $anexo_nome="";
		private $anexo_tipo="";
		
		private $titulo;
		private $assunto;
		
		private $mensagem;
		private $mensagem_txt;
		
		function __construct(){
			/* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
			if(strtoupper(PHP_OS)=="LINUX") $this->quebra="\n"; //Se for Linux
			elseif(strstr(strtoupper(PHP_OS),"WIN")) $this->quebra="\r\n"; //Se for Windows
			else die("Este script não esta preparado para funcionar com o sistema operacional de seu servidor");
		}
		
		//Métodos Públicos ------------------------------------------------------------------------------		
		public function setEnvioNome($var){
			$this->envio_nome=$var;
			$this->destino_nome=$var;
			$this->resposta_nome=$var;
		}
		public function setEnvioEmail($var){
			$this->envio_email=$var;
			$this->destino_email=$var;
			$this->resposta_email=$var;
		}
		
		public function setDestinoNome($var){ $this->destino_nome=$var; }
		public function setDestinoEmail($var){ $this->destino_email=$var; }
		
		public function setRespostaNome($var){ $this->resposta_nome=$var; }
		public function setRespostaEmail($var){ $this->resposta_email=$var; }
		
		public function setBC($var){  $this->copia=$var; 		}
		public function setBCC($var){ $this->copia_oculta=$var; }
		
		public function setTitulo($var){ $this->titulo=$var; }
		public function setAssunto($var){ $this->assunto=$var; }
		
		public function setMensagem($mensagem,$mensagem_texto=NULL){
			if(empty($this->titulo)) $this->titulo='Nova mensagem';
			if(empty($this->assunto)) $this->assunto=$this->titulo;
			
			$this->mensagem=$mensagem;
			$this->mensagem_txt=!empty($mensagem_texto)?$mensagem_texto:$mensagem;
			
			//CABEÇALHO
			$msg_header='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
			$msg_header.='<html xmlns="http://www.w3.org/1999/xhtml">';
			$msg_header.='<head>';
			$msg_header.='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
			$msg_header.='<title>Embelezando</title>';
			$msg_header.='</head>';
			$msg_header.='<body style="margin:0px;">';
			$msg_header.='<table border="0" cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:13px; background-image:url(http://www.embelezando.net/beta/imagens/background.png); background-repeat:repeat;">';
			$msg_header.='<tr>';
			$msg_header.='<td width="650" colspan="2" style="background-color:#DD127B; font-size:11px;">';
			$msg_header.='<img src="http://www.embelezando.net/beta/imagens/email/embelezando.png" height="50" alt="Embelezando.net" style="margin:10px; float:left;" />';
			$msg_header.='<p style="margin:9px 0 0 15px; float:right; color:#CCC;"></p>';
			$msg_header.='</td>';
			$msg_header.='</tr>';
			$msg_header.='<tr>';
			$msg_header.='<td width="650" colspan="2" style="font-size:14px;">';
			$msg_header.='<p style="width:600px; margin:5px 25px; float:left; color:#DD127B; padding-bottom:5px; border-bottom:1px solid #e5c2d4; font-size:16px;">'.$this->assunto.'</p>';
			$msg_header.='</td>';
			$msg_header.='</tr>';
			$msg_header.='<tr>';
			$msg_header.='<td>';
			$msg_header.='<table width="600" border="0" cellpadding="0" cellspacing="0" style="margin:0 25px 15px 25px; float:left;">';
			$msg_header.='<tr>';
			$msg_header.='<td>';
			
			//RODAPÉ
			$msg_footer='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='</table>';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='<tr>';
			$msg_footer.='<td width="650" colspan="2" style="background-color:#DD127B; font-size:11px;">';
			$msg_footer.='<img src="http://www.embelezando.net/beta/imagens/email/embelezando-rodape.png" alt="Embelezando.net" height="24" style="margin:7px; float:right;" />';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='<tr>';
			$msg_footer.='<td width="650" colspan="2" style="background-color:#ebebeb; font-size:11px;">';
			$msg_footer.='<p style="margin:5px; float:left; color:#999;">Esta é uma mensagem automática e não deve ser respondida. <small>(mensagem enviada no dia '.date('d/m/Y').' às '.date('H:i:s').')</small></p>';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='</table>';
			$msg_footer.='</body>';
			$msg_footer.='</html>';

			$this->mensagem='<style>p{width:100%; margin:3px 0 0 0; float:left;}</style>'.$msg_header.$this->mensagem.$msg_footer;
			$this->mensagem=$this->mensagem;
			
			$this->mensagem_txt=$msg_header.$this->mensagem_txt.$msg_footer;
			$this->mensagem_txt=htmlentities(strip_tags(str_replace('<br />','\n',$this->mensagem_txt)),ENT_COMPAT,'UTF-8');
		}
		
		//Anexação de Arquivos
		public function anexar($file){
			if(file_exists($file["tmp_name"]) and !empty($file)){
				$fp = fopen($file["tmp_name"],"rb");
				$anexo = fread($fp,filesize($file["tmp_name"]));
				$anexo = base64_encode($file);				
				fclose($fp);				
				$this->anexo = chunk_split($file);
				$this->anexo_nome= $file["nome"];
				$this->anexo_tipo = $file["type"];
				
				$this->formato = "boundary";
			}
		}
		
		public function preview($part){
			if($part==="headers"){
				$preview='<plaintext>Headers: ------------------------'.$this->quebra.$this->headers;
			}
			else if($part==="mensagem"){
				$preview='Mensagem: ------------------------<br />'.$this->quebra.$this->mensagem;
			}
			else if($part==="config"){
				$preview='<plaintext>De: '.$this->envio_nome.' ('.$this->envio_email.')'.$this->quebra;
				$preview.='Para: '.$this->destino_nome.' ('.$this->destino_email.')'.$this->quebra;
				$preview.='Responder a: '.$this->resposta_nome.' ('.$this->resposta_email.')'.$this->quebra;
				$preview.='Assunto: '.$this->assunto;
			}
			echo $preview;
			exit();
		}
						
		public function enviaEmail(){
			if($this->envio_email=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Destinatário não especificado!');
			if($this->destino_email=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Destinatário não especificado!');
			if($this->assunto=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Assunto não informado!');
			if($this->mensagem=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Nenhuma mensagem definida.');
			
			try{				
				$sistema = new sistema();
				//require_once '/home/webillus/php/Mail.php';
				
				//CONFIGURAÇÕES - http://pear.php.net/package/Mail_Mime/docs/latest/elementindex_Mail_Mime.html
				$from_name="Embelezando";
				$from='no-reply@embelezando.net';
				$password='mh2wi65uh17t';
				
				//$from='sistema@webillusion.com.br';
				//$password='i11us10nWeb';
				
				$replyto_name="Embelezando";
				$replyto = $this->envio_email;
							
				//CABEÇALHO
				$assunto=$this->assunto;
				$assunto = '=?UTF-8?B?'.base64_encode($assunto).'?=';
				
				//MENSAGEM
				$mensagem_texto=$this->mensagem_txt;
				$mensagem_html=$this->mensagem;
								
				$headers = array(
				  'To'            => $this->destino_email,
				  'From'          => (!empty($from_name)?$from_name.' <'.$from.'>':$from),
				  'Return-Path'   => $from,
				  'Reply-To'      => (!empty($replyto_name)?$replyto_name.' <'.$replyto.'>':$replyto),
				  'Subject'       => $assunto,
				  'Errors-To'     => 'sistema@webillusion.com.br',
				  'MIME-Version'  => '1.0',
				);
				 
				// Set up parameters for both the HTML and plain text mime parts.
				$textparams = array(
				  'charset'       => 'UTF-8',
				  'content_type'  => 'text/plain',
				  'encoding'      => 'quoted/printable',
				);
				$htmlparams = array(
				  'charset'       => 'UTF-8',
				  'content_type'  => 'text/html',
				  'encoding'      => 'quoted/printable',
				);
					   
				// Create the email itself. The content is blank for now.
				$email = new Mail_mimePart('', array('content_type' => 'multipart/alternative'));
				
				//Cópia
				if(!empty($this->copia)) $email->addBc($this->copia);
				
				//Cópia Oculta
				//if(!empty($this->copia_oculta)) $email->addBcc($this->copia_oculta);
				//if($this->destino_email!=$sistema->desenvolvedorEmail()) $email->addBcc($sistema->desenvolvedorEmail());
					   
				// Add the text and HTML versions as parts within the main email.
				$textmime = $email->addSubPart($mensagem_texto, $textparams);
				$htmlmime = $email->addSubPart($mensagem_html, $htmlparams);
					   
				// Get back the body and headers from the MIME object. Merge the headers with
				// the ones we defined earlier.
				$final = $email->encode();
				$final['headers'] = array_merge($final['headers'], $headers);
				 
				// Perform the actual send.
				$smtp_params = array();
				$smtp_params['host'] = 'mail.webillusion.com.br';
				$smtp_params['port'] = '587';
				$smtp_params['persist'] = true;
				$smtp_params['auth'] = true;
				$smtp_params['username'] = $from;
				$smtp_params['password'] = $password;
										 
				$mail =& Mail::factory('smtp', $smtp_params);
				
				if(PEAR::isError($mail)) $enviado=false;
				else $enviado = $mail->send($this->destino_email, $final['headers'], $final['body']);

				if(!$enviado) throw new EmailException('Ocorreu uma falha de conexão e o e-mail não foi enviado.<br />Tente novamente mais tarde.','[Mail] Falha na tentativa de conexão, impossível conectar a servidor SMTP!');
			}
			catch(EmailException $e){
				$relatorio=new relatorio();
				$relatorio->setPrefixo('[E-mail]');
				$relatorio->setDestinatario('email');
				$relatorio->gerarRelatorio('Assunto: '.$this->assunto.'<br />Mensagem: '.$this->mensagem);
				
				throw new SistemaException($e->getMensagem(),NULL,NULL);
			}
		}		
	} 
?>