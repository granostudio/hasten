<?php
	class email{
		private $hospedagem;
		private $quebra;
		
		private $servidorNome;
		private $servidorEmail;
		
		private $remetenteNome;
		private $remetenteEmail;
		
		private $returnNome;
		private $returnEmail;
		
		private $destinatarioNome;
		private $destinatarioEmail;
		
		private $destinatarioCopia;
		private $destinatarioCopiaOculta;
				
		private $anexos;
		
		private $assunto;		
		private $mensagemHtml;
		private $mensagemTexto;
		
		private $headers;
		
		//SETTERS
		public function setHospedagem($var){ $this->hospedagem=$var; }
		
		public function setServidorNome($var){ $this->servidorNome=$var; }
		public function setServidorEmail($var){ $this->servidorEmail=$var; }
		
		public function setRemetenteNome($var){ $this->remetenteNome=$var; }
		public function setRemetenteEmail($var){ $this->remetenteEmail=$var; }
		
		public function setReturnNome($var){ $this->returnNome=$var; }
		public function setReturnEmail($var){ $this->returnEmail=$var; }
		
		public function setDestinatarioNome($var){ $this->destinatarioNome=$var; }
		public function setDestinatarioEmail($var){ $this->destinatarioEmail=$var; }
		
		public function addCopia($var){ array_push($this->destinatarioCopia,$var); }
		public function addCopiaOculta($var){ array_push($this->destinatarioCopiaOculta,$var); }
		
		public function addAnexo($var){ array_push($this->anexos,$var); }
		
		public function setAssunto($var){ $this->assunto='=?UTF-8?B?'.base64_encode($var).'?='; }
		public function setMensagemHtml($var){ $this->mensagemHtml=$var; }
		public function setMensagemTexto($var){ $this->mensagemTexto=$var; }
		
		//ENVIO	
		public function enviar($enviar=true){
			$this->validarParametros();
			$this->finalizarParametros();
		
			$this->gerarEmail();
			
			if(!$enviar){ //VISUALIZAÇÃO			
				echo 'E-mail servidor: '.$this->servidorNome.htmlentities(' <'.$this->servidorEmail.'>').'<br />';
				echo 'E-mail remetente: '.$this->remetenteNome.htmlentities(' <'.$this->remetenteEmail.'>').'<br />';
				echo 'E-mail destinatário: '.$this->destinatarioNome.htmlentities(' <'.$this->destinatarioEmail.'>').'<br /><br />';
				
				echo 'Assunto: '.$this->assunto.'<br /><br />';
				
				echo 'Headers: '.$this->headers.'<br /><br />';
				
				echo 'Mensagem HTML: '.$this->mensagemHtml.'<br /><br />';
				echo 'Mensagem Texto: '.$this->mensagemTexto.'<br /><br />';
				
				exit();
			}
			else{ //ENVIO					
				if(!$envio=mail($this->destinatarioEmail,$this->assunto,$this->mensagemHtml,$this->headers,'-r'.$this->servidorEmail)){ //Se for Postfix
					$headers .= 'Return-Path: '.$this->servidorEmail.$this->quebra; //Se 'não for Postfix'
					$envio=mail($this->destinatarioEmail,$this->assunto,$this->mensagemHtml,$this->headers);
				}
				return $envio;
			}
		}
		
		//VALIDAÇÃO
		private function validarParametros(){
			if(empty($this->servidorEmail)) throw new validacaoException('Nenhum e-mail de origem definido para envio do e-mail.');
			//else if(empty($this->destinatarioEmail)) throw new validacaoException('Nenhum e-mail de destinatário definido para envio do e-mail.');
			
			else if(empty($this->assunto)) throw new validacaoException('Nenhum assunto definido para envio do e-mail.');
			else if(empty($this->mensagemHtml)) throw new validacaoException('Nenhum conteúdo definido para envio do e-mail.');
		}
		
		private function finalizarParametros(){
			if(empty($this->hospedagem)) $this->hospedagem='linux';
			$this->quebra=$this->hospedagem==='windows'?"\r\n":"\n";

			if(empty($this->remetenteNome)) $this->remetenteNome=$this->servidorNome;
			if(empty($this->remetenteEmail)) $this->remetenteEmail=$this->servidorEmail;
			
			if(empty($this->destinatarioNome)) $this->destinatarioNome=$this->servidorNome;
			if(empty($this->destinatarioEmail)) $this->destinatarioEmail=$this->servidorEmail;
			
			if(empty($this->returnNome)) $this->returnNome=$this->servidorNome;			
			if(empty($this->returnEmail)) $this->returnEmail=$this->servidorEmail;
			
			if(empty($this->mensagemTexto)) $this->mensagemTexto=strip_tags($this->mensagemHtml);
			
			$mensagem='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
			$mensagem.='<html xmlns="http://www.w3.org/1999/xhtml">';
			$mensagem.='<body>';
			$mensagem.=$this->mensagemHtml;
			$mensagem.='</body>';
			$mensagem.='</html>';
			
			$this->mensagemHtml=$mensagem;
		}
		
		//CONTEÚDO
		private function gerarEmail(){
			//CABEÇALHO - ANEXOS
			if(!empty($this->anexos)){
				$boundary='XYZ-'.date('dmYis').'-ZYX';
				$headerAdicional=NULL;
				
				$headerAdicional='--'.$boundary.$this->quebra;
				$headerAdicional.='Content-Transfer-Encoding: 8bits'.$this->quebra;
				$headerAdicional.='Content-Type: text/html; charset="UTF-8"'.$this->quebra.$this->quebra; //plain
				$headerAdicional.=$this->mensagemHtml.$this->quebra;
				
				$comAnexo=0;
				foreach($this->anexos as $arquivo){
					if(file_exists($arquivo['tmp_name'])&&!empty($arquivo)){
						$comAnexo++;
						
						$fp=fopen($arquivo['tmp_name'],'rb');
						$anexo=fread($fp,filesize($arquivo['tmp_name']));
						$anexo=base64_encode($anexo);				
						fclose($fp);
						$anexo=chunk_split($anexo);
										
						$headerAdicional.='--'.$boundary.$this->quebra;						
						$headerAdicional.='Content-Type: '.$arquivo['type'].'\n';
						$headerAdicional.='Content-Disposition: attachment; filename="'.$arquivo["name"].'"'.$this->quebra;
						$headerAdicional.='Content-Transfer-Encoding: base64'.$this->quebra.$this->quebra;
						$headerAdicional.=$anexo.$this->quebra;
						$headerAdicional.='--'.$boundary.'--'.$this->quebra;
					}
				}
				
				if(!empty($comAnexo)) $headerAdicional='Content-type: multipart/mixed; boundary="'.$boundary.'"'.$this->quebra;
				else $headerAdicional.=$boundary.$this->quebra;
			}
			else $headerAdicional='Content-type: text/html; charset=UTF-8'.$this->quebra;
			
			//HEADER
			$this->headers='MIME-Version: 1.1'.$this->quebra;
			
			if($this->hospedagem!=='windows'){
				$this->headers.='From: '.$this->servidorEmail.$this->quebra;
				$this->headers.='Return-Path: '.$this->servidorEmail.$this->quebra;
				$this->headers.='Reply-To: '.$this->returnEmail.$this->quebra;
			}
			else{
				$this->headers.='From: '.$this->servidorNome.' <'.$this->servidorEmail.'>'.$this->quebra;
				$this->headers.='Return-Path: '.$this->servidorNome.' <'.$this->servidorEmail.'>'.$this->quebra;
				$this->headers.='Reply-To: '.$this->returnEmail.' <'.$this->returnNome.'>'.$this->quebra;
			}
			$this->headers.=$headerAdicional;
		}
	}
?>