<?php
	class email{
		private $servidor=PLATAFORMA;
		
		private $quebra_linha="";
		private $charset=CONFIG_CHARSET;
		private $formato;
		private $host=HOSPEDAGEM;
		
		private $envio_nome;
		private $envio_email;
		
		private $destino_nome;
		private $destino_email;
		
		private $resposta_nome;
		private $resposta_email;
		
		private $copia;
		private $copia_oculta;
		
		private $anexo="";
		private $anexo_nome="";
		private $anexo_tipo="";
		
		private $titulo;
		private $assunto;
		private $headers;
		
		private $mensagem;
		private $mensagem_txt;
		
		function __construct($servidor=NULL){
			if($servidor!=NULL) $this->servidor=$servidor;
		
			/* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
			if($this->servidor=="linux") $this->quebra_linha="\n"; //Se for Linux
			elseif($this->servidor=="windows") $this->quebra_linha="\r\n"; //Se for Windows
			else die("Este script não esta preparado para funcionar com o sistema operacional de seu servidor");
		}
		
		//Métodos Públicos ------------------------------------------------------------------------------
		public function setCharset($var){ $this->charset=$var; }
		public function setHost($var){ $this->host=$var; }
		
		public function setEnvioNome($var){
			$this->envio_nome=$var;
			$this->destino_nome=$var;
			$this->resposta_nome=$var;
		}
		public function setEnvioEmail($var){
			$this->envio_email=$var;
			$this->destino_email=$var;
			$this->resposta_email=$var;
		}
		
		public function setDestinoNome($var){ $this->destino_nome=$var; }
		public function setDestinoEmail($var){ $this->destino_email=$var; }
		
		public function setRespostaNome($var){ $this->resposta_nome=$var; }
		public function setRespostaEmail($var){ $this->resposta_email=$var; }
		
		public function setBC($var){ 	$this->copia=$var; 			}
		public function setBCC($var){ 	$this->copia_oculta=$var; 	}
		
		public function setTitulo($var){ $this->titulo=$var; }
		public function setAssunto($var){ $this->assunto=$var; }
		public function setMensagem($var,$noreply=true){ $this->mensagem=$var; $this->mailConstruct($noreply); }
		
		//Anexação de Arquivos
		public function anexar($file){
			if(file_exists($file["tmp_name"]) and !empty($file)){
				$fp = fopen($file["tmp_name"],"rb");
				$anexo = fread($fp,filesize($file["tmp_name"]));
				$anexo = base64_encode($file);				
				fclose($fp);				
				$this->anexo = chunk_split($file);
				$this->anexo_nome= $file["nome"];
				$this->anexo_tipo = $file["type"];
				
				$this->formato = "boundary";
			}
		}
		
		public function preview($part){
			if($part==="headers"){
				$preview='<plaintext>Headers: ------------------------'.$this->quebra_linha.$this->headers;
			}
			else if($part==="mensagem"){
				$preview='Mensagem: ------------------------<br />'.$this->quebra_linha.$this->mensagem;
			}
			else if($part==="config"){
				$preview='<plaintext>De: '.$this->envio_nome.' ('.$this->envio_email.')'.$this->quebra_linha;
				$preview.='Para: '.$this->destino_nome.' ('.$this->destino_email.')'.$this->quebra_linha;
				$preview.='Responder a: '.$this->resposta_nome.' ('.$this->resposta_email.')'.$this->quebra_linha;
				$preview.='Assunto: '.$this->assunto;
			}
			echo $preview;
			exit();
		}
				
		//Métodos Privados ----------------------------------------------------------------------------
		private function headersDefine(){
			switch($this->formato){
				case "boundary";
					$boundary="XYZ-".date("dmYis")."-ZYX";
					
					$mens = "--".$boundary.$this->quebra_linha;
					$mens .= "Content-Transfer-Encoding: 8bits".$this->quebra_linha;
					$mens .= "Content-Type: text/html; charset='".$this->charset."'".$this->quebra_linha;
					$mens .= $this->mensagem.$this->quebra_linha;
					$mens .= "--".$boundary.$this->quebra_linha;
					$mens .= "Content-Type: ".$this->anexo_tipo."\n";
					$mens .= "Content-Disposition: attachment; filename='".$this->anexo_nome."'".$this->quebra_linha;
					$mens .= "Content-Transfer-Encoding: base64".$this->quebra_linha.$this->quebra_linha;
					$mens .= $this->anexo.$this->quebra_linha;
					$mens .= "--".$boundary."--".$this->quebra_linha;
					$this->mensagem=$mens;
					
					$headplus="Content-type: multipart/mixed; boundary='".$boundary."'".$this->quebra_linha;
					$headplus.=$boundary.$this->quebra_linha;
					break;
					
				default:
					$headplus="Content-type: text/html; charset=".$this->charset.$this->quebra_linha;
					break;	
			}
			
			$this->headers = "MIME-Version: 1.1".$this->quebra_linha;;
			$this->headers .= "From: ".$this->envio_nome."<".$this->envio_email.">".$this->quebra_linha;
			//$this->headers .= "Return-Path: ".$this->envio_nome.$this->quebra_linha;
			$this->headers .= "Reply-To: ".$this->resposta_nome."<".$this->resposta_email.">".$this->quebra_linha;
			$this->headers .= $headplus;
		}
		
		public function enviaEmail(){
			if($this->envio_email=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Destinatário não especificado!');
			if($this->destino_email=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Destinatário não especificado!');
			if($this->assunto=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Assunto não informado!');
			if($this->mensagem=="") throw new EmailException('Falha nas configurações de envio, tente novamente mais tarde','[Mail] Nenhuma mensagem definida.');
			
			try{
				$mail = new phpmailer();
				$sistema = new sistema();
				
				$mail->IsSMTP(); // Define que a mensagem será SMTP
				$mail->Host='mail.webillusion.com.br'; // Endereço do servidor SMTP
				//$mail->Port=587; // Endereço do servidor SMTP
				$mail->SMTPAuth = true; // Autenticação
				$mail->Username = 'sistema@webillusion.com.br'; // Usuário do servidor SMTP
				$mail->Password = 'i11us10nWeb'; // Senha da caixa postal utilizada
				
				$mail->From = $mail->Username; 
				$mail->FromName = $sistema->cliente();				
				
				$mail->AddReplyTo($this->envio_email);
				$mail->AddAddress($this->destino_email);
				
				//$mail->AddAddress('destinatario@dominio.com.br', 'Nome do Destinatário');
				//$mail->AddAddress('e-mail@destino2.com.br');
				//$mail->AddCC('copia@dominio.com.br', 'Copia'); 
				
				if(!empty($this->copia_oculta)) $mail->AddBCC($this->copia_oculta);
				if($this->destino_email!=$sistema->desenvolvedorEmail()) $mail->AddBCC($sistema->desenvolvedorEmail(),$sistema->cliente());
				
				$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
				$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
				
				//CABEÇALHO				
				$mail->Subject  = '=?UTF-8?B?'.base64_encode($this->assunto).'?='; // Assunto da mensagem
				$mail->Body = $this->mensagem;
				$mail->AltBody = $this->mensagem_txt;
			
				//anexos
				//$mail->AddAttachment("e:\home\login\web\documento.pdf", "novo_nome.pdf");
				$enviado=$mail->Send();
				$mail->ClearAllRecipients();
		
				if(!$enviado) throw new EmailException('Ocorreu uma falha de conexão e o e-mail não foi enviado.<br />Tente novamente mais tarde.','[Mail] Falha na tentativa de conexão, impossível conectar a servidor SMTP!');
			}
			catch(EmailException $e){
				$relatorio=new relatorio();
				$relatorio->setPrefixo('[E-mail]');
				$relatorio->setDestinatario('email');
				$relatorio->gerarRelatorio('Assunto: '.$this->assunto.'<br />Mensagem: '.$this->mensagem);
				
				throw new SistemaException($e->getMensagem(),NULL,NULL);
			}
		}
		
		private function mailConstruct($noreply){
			if(empty($this->titulo)) $this->titulo=$this->assunto;
			
			//CABEÇALHO
			$msg_header='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
			$msg_header.='<html xmlns="http://www.w3.org/1999/xhtml">';
			$msg_header.='<head>';
			$msg_header.='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
			$msg_header.='<title>Embelezando</title>';
			$msg_header.='</head>';
			$msg_header.='<body style="margin:0px;">';
			$msg_header.='<table border="0" cellpadding="0" cellspacing="0" style="font-family:Tahoma; font-size:13px; background-image:url(http://www.embelezando.net/imagens/background.png); background-repeat:repeat;">';
			$msg_header.='<tr>';
			$msg_header.='<td width="650" colspan="2" style="background-color:#DD127B; font-size:11px;">';
			$msg_header.='<img src="http://www.embelezando.net/imagens/email/embelezando.png" height="50" alt="Embelezando.net" style="margin:10px; float:left;" />';
			$msg_header.='<p style="margin:9px 0 0 15px; float:right; color:#CCC;"></p>';
			$msg_header.='</td>';
			$msg_header.='</tr>';
			$msg_header.='<tr>';
			$msg_header.='<td width="650" colspan="2" style="font-size:14px;">';
			$msg_header.='<p style="width:600px; margin:5px 25px; float:left; color:#DD127B; padding-bottom:5px; border-bottom:1px solid #e5c2d4; font-size:16px;">'.$this->assunto.'</p>';
			$msg_header.='</td>';
			$msg_header.='</tr>';
			$msg_header.='<tr>';
			$msg_header.='<td>';
			$msg_header.='<table width="600" border="0" cellpadding="0" cellspacing="0" style="margin:0 25px 15px 25px; float:left;">';
			$msg_header.='<tr>';
			$msg_header.='<td>';
			
			//RODAPÉ
			$msg_footer='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='</table>';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='<tr>';
			$msg_footer.='<td width="650" colspan="2" style="background-color:#DD127B; font-size:11px;">';
			$msg_footer.='<img src="http://www.embelezando.net/imagens/email/embelezando-rodape.png" alt="Embelezando.net" height="24" style="margin:7px; float:right;" />';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='<tr>';
			$msg_footer.='<td width="650" colspan="2" style="background-color:#ebebeb; font-size:11px;">';
			$msg_footer.='<p style="margin:5px; float:left; color:#999;">'.($noreply?'Esta é uma mensagem automática e não deve ser respondida.':'').' <small>(mensagem enviada no dia '.date('d/m/Y').' às '.date('H:i:s').')</small></p>';
			$msg_footer.='</td>';
			$msg_footer.='</tr>';
			$msg_footer.='</table>';
			$msg_footer.='</body>';
			$msg_footer.='</html>';

			$this->mensagem='<style>p{width:100%; margin:3px 0 0 0; float:left;}</style>'.$msg_header.$this->mensagem.$msg_footer;
			$this->mensagem=$this->mensagem;
			
			$this->mensagem_txt=$msg_header.$this->mensagem_txt.$msg_footer;
			$this->mensagem_txt=htmlentities(strip_tags(str_replace('<br />','\n',$this->mensagem_txt)),ENT_COMPAT,'UTF-8');
							
			$this->headersDefine();			
		}
	} 
?>