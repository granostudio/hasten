<?php
	class busca extends notificacao{
		private $db;
		private $termo;
		private $registros;
		
		//GETTER
		public function getTermo(){ return $this->termo; }
		
		function __construct(){
			$this->db=new db();
			
			try{
				if(empty($_POST['termo'])) throw new validacaoException('Nenhum termo definido','');
				
				validacao::setCampo('termo','Termo');
				$this->termo=validacao::validar('texto',$_POST[validacao::getCampo()],true);
				$this->buscar();
				
				return "IJ";
			}
			catch(validacaoException $e){ }
		}
		
		//COMENTARIOS
		public function getResultados(){
			if(!empty($this->registros)){
				$retorno='';
				foreach($this->registros as $registro){
					$retorno.='<a href="'.$registro['pagina'].'?id='.$registro['id'].'" class="white_papers_noticias">
									<h3>'.(strstr($registro['pagina'],'conteudo')?'Artigo':'WhitePaper').'</h3>
									<p>'.$registro['titulo'].'</p>
							   </a>';
				}
				return $retorno;				
			}
			else return 'Nenhuma página relacionada ao termo buscado.';
		}
		
		//CONSULTA
		private function buscar(){
			$registros=array();
			
			$sql='SELECT * FROM artigo WHERE (titulo LIKE "%'.$this->termo.'%" OR resumo LIKE "%'.$this->termo.'%" OR texto LIKE "%'.$this->termo.'%") AND status=1';
			$artigos=$this->db->fetch($sql);
			
			if(!empty($artigos)){
				foreach($artigos as $dado){
					$dado['pagina']='conteudo_interno.php';
					array_push($registros,$dado);
				}
			}
			
			
			$sql='SELECT * FROM whitepaper WHERE (titulo LIKE "%'.$this->termo.'%" OR resumo LIKE "%'.$this->termo.'%" OR texto LIKE "%'.$this->termo.'%") AND status=1';
			$whitepapers=$this->db->fetch($sql);
			
			if(!empty($whitepapers)){
				foreach($whitepapers as $dado){
					$dado['pagina']='blog_interno.php';
					array_push($registros,$dado);
				}
			}

			$this->registros=$registros;
		}		
	}
?>