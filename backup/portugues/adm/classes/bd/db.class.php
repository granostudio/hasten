<?php
class db{
	private $host;
	private $user;
	private $pass;
	private $database;

	private $_DATA;
	private $fetch;
	private $rows;
	private $sql;

	private $resource;
	private $tabela;

	public function __construct(){
		if($_SERVER['HTTP_HOST'] == 'localhost'){
			$this->host = "localhost";
			$this->user = "root";
			$this->pass = "";
			$this->database="infoarts";
		}
		else{
			$this->host = "hasten.mysql.dbaas.com.br";
			$this->user = "hasten";
			$this->pass = "G0uHQxv46jT1DB";
			$this->database="hasten";
		}

		$this->connectDb();
		$this->selectDb();
	}

	//VALIDA TABELA EXISTENTE
	protected function validaTabelaExistente($tabela){
		$this->tabela=db::fetch('SHOW COLUMNS FROM '.$tabela);

		if(empty($this->tabela)) throw new SistemaException('[Query] Falha tentando adquirir dados da tabela ['.$tabela.']. Tabela inacessível ou inexistente.');
	}

	protected function getTabela(){ return $this->tabela; }

	//FUNCOES GERAIS
	public function connectDb(){
		$this->resource = mysqli_connect($this->host, $this->user, $this->pass);
		return $this->resource;
	}

	public function selectDb(){
		mysqli_select_db($this->resource,$this->database);
		mysqli_set_charset($this->resource,'utf8');
	}

	public function query($var){
		return mysqli_query($this->resource,$var);
	}

	public function fetch($var){
		$this->_DATA = array();
		if($this->sql = mysqli_query($this->resource,$var))
			while($this->fetch = mysqli_fetch_array($this->sql)){
				$this->_DATA[] = $this->fetch;
			}
		return $this->_DATA;
	}

	public function getDbName(){
		return $this->database;
	}

	public function encerra(){
		mysqli_close($this->resource);
	}
}
?>
