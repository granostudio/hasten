<?php
	class artigo extends notificacao{
		private $tabela='artigo';
		
		private $id;
		private $titulo_pt;
		private $titulo_en;
		private $texto_pt;
		private $texto_en;
		private $imagem;
		private $data;
		private $tags_pt;
		private $tags_en;
		private $status;
		
		function __construct(){
			parent::__construct();
			
			try{
				if(!empty($_GET['registro'])){
					validacao::setCampo('registro','Id');
					$this->id=validacao::validar('inteiro',$_GET[validacao::getCampo()],true);
				}
			}
			catch(validacaoException $e){ return NULL; }
		}
		
		//GETTERS		
		public function getRegistro(){	
			try{				
				$registro=$this->getRegistroBD();
			
				if(count($registro)!=1) return NULL;
				else return $registro[0];
			}
			catch(validacaoException $e){ return NULL; }
		}
		
		public function getRegistros(){
			$artigos=$this->getRegistrosBD();
			
			if(!empty($artigos)){
				foreach($artigos as $idArt=>$artigo){
					$comentarios=new artigo_comentario();
					$comentarios=$comentarios->getArtigoComentarios($artigo['id']);
					
					$artigos[$idArt]['comentarios']=array();
					$artigos[$idArt]['comentarios']=$comentarios;
				}
			}
			return $artigos;		
		}
		
		//VALIDAÇÃO		
		public function validarFormulario(){
			validacao::setCampo('titulo_pt','Título (português)');
			$this->titulo_pt=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('titulo_en','Título (inglês)');
			$this->titulo_en=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('texto_pt','Artigo (português)');
			$this->texto_pt=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('texto_en','Artigo (inglês)');
			$this->texto_en=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('imagem','Imagem');
			if(empty($this->id)||!empty($_FILES[validacao::getCampo()]['tmp_name'])){
				validacao::setMensagem(0,'Nenhum arquivo de Imagem selecionado');
				validacao::validar('texto',$_FILES[validacao::getCampo()]['name'],true);				
				$this->imagem=$_FILES[validacao::getCampo()];
			}
			
			validacao::setCampo('data','Data');
			$this->data=validacao::validar('datahora',data::converterData($_POST[validacao::getCampo()],true,'bd'),true);
			
			validacao::setCampo('tags_pt','Tags (português)');
			$this->tags_pt=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('tags_en','Tags (inglês)');
			$this->tags_en=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('status','Status');
			$this->status=validacao::validar('inteiro',$_POST[validacao::getCampo()],true);
		}
		
		//ATUALIZAÇÃO/CADASTRO//REMOÇÃO
		public function salvarRegistro(){
			try{
				//IMAGEM
				if(!empty($this->imagem['name'])){
					try{
						$arquivo=new arquivo();
						$arquivo->uploadImagem($this->imagem,2,array('jpg','jpeg','gif','png'),'artigo');
						
						//AMPLIADA
						$arquivo->redimensionarImagem(363,573);
						$arquivo->salvarImagem(SITE_DIR.'imagens/artigo/');
						
						//FINALIZA ENVIO DA IMAGEM E ADQUIRE NOME FINAL
						$this->imagem=$arquivo->finalizar();
						$this->imagem=$this->imagem[0];
					}
					catch(uploadException $e){
						throw new validacaoException($e->getMensagem(),'logotipo');
					}
				}
				
				//CADASTRO
				if(empty($this->id)){
					$this->cadastrar();
					notificacao::sucesso('Artigo cadastrado com sucesso!');
				}
				else{										
					$this->atualizar();
					notificacao::sucesso('Artigo atualizado com sucesso!');
				}
			}
			catch(notificacaoException $e){ header('Location: sistema.php'); }
		}
		
		public function removerRegistro(){
			try{
				if(empty($this->id)) throw new validacaoException('Falha coletando dados de registro para remoção.');
				
				//REMOVE IMAGEM (mantem backup)
				$registro=$this->getRegistroBD();
				$registro=$registro[0];
				if(!empty($registro['imagem'])&&file_exists(SITE_DIR.'imagens/artigo/'.$registro['imagem'])){
					diretorio::criaDir(ADM_DIR.'_temp/removidas/artigo/',0777);
					rename(SITE_DIR.'imagens/artigo/'.$registro['imagem'],ADM_DIR.'_temp/removidas/artigo/'.$registro['imagem']);
				}
				
				$this->status=0;
				$this->salvarRegistro();
				
				notificacao::erro('Artigo removido com sucesso!');
			}
			catch(notificacaoException $e){ header('Location: sistema.php?modulo='.$_GET['modulo'].'&acao=gerenciar'); }
		}
		
		//CONEXÕES
		private function cadastrar(){
			db::validaTabelaExistente($this->tabela);
			foreach(db::getTabela() as $coluna){
				if($coluna['Field']==='id') continue;
				else if(!empty($this->$coluna['Field'])||$coluna['Null']==='YES'){
					if(strstr($coluna['Type'],'datetime')){
						if(strstr(strtolower($this->$coluna['Field']),'now')) $valor=$this->$coluna['Field'];
						else $valor='"'.$this->$coluna['Field'].'"';
					}
					else if($coluna['Null']==='YES'&&!isset($this->$coluna['Field'])) continue;
					else if(strstr($coluna['Type'],'int')||strstr($coluna['Type'],'decimal')||strstr($coluna['Type'],'date')) $valor=$this->$coluna['Field'];
					else if(isset($this->$coluna['Field'])) $valor='"'.$this->$coluna['Field'].'"';
					
					if(isset($valor)){
						$campos=empty($campos)?$coluna['Field']:$campos.','.$coluna['Field'];
						$valores=empty($valores)?$valor:$valores.','.$valor;
					}
				}
			}		
			$query='INSERT INTO '.$this->tabela.'('.$campos.') VALUE('.$valores.')';
			db::query($query);
		}
		
		private function atualizar(){
			db::validaTabelaExistente($this->tabela);
			foreach(db::getTabela() as $coluna){
				$valor=NULL;
				if($coluna['Field']==='id') continue;
				else if(isset($this->$coluna['Field'])||$coluna['Null']==='YES'){
					if(strstr($coluna['Type'],'datetime')){
						if(strstr(strtolower($this->$coluna['Field']),'now')) $valor=$this->$coluna['Field'];
						else $valor='"'.$this->$coluna['Field'].'"';
					}
					else if($coluna['Null']==='YES'&&$this->$coluna['Field']==='') $valor='NULL';
					else if(strstr($coluna['Type'],'int')||strstr($coluna['Type'],'decimal')||strstr($coluna['Type'],'date')) $valor=$this->$coluna['Field'];
					else if(isset($this->$coluna['Field'])) $valor='"'.$this->$coluna['Field'].'"';
					
					if(isset($valor)) $query=empty($query)?$coluna['Field'].'='.$valor:$query.', '.$coluna['Field'].'='.$valor;
				}
			}
			$query='UPDATE '.$this->tabela.' SET '.$query.' WHERE id='.$this->id;
			db::query($query);
		}
		
		//SELECTS		
		private function getRegistroBD(){
			$sql='SELECT * FROM '.$this->tabela.' WHERE status>0 AND id="'.$this->id.'"';
			return db::fetch($sql);
		}
		
		private function getRegistrosBD(){
			$sql='SELECT * FROM '.$this->tabela.' WHERE status>0 ORDER BY data DESC';			
			return db::fetch($sql);
		}	
	}
?>