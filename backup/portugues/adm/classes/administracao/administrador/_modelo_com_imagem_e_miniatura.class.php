<?php		
	//VALIDAÇÃO		
	public function validarFormulario(){
		validacao::setCampo('titulo','Título');
		$this->titulo=validacao::validar('texto',$_POST[validacao::getCampo()],true);
		
		validacao::setCampo('imagem','Imagem');
		if(empty($this->id)||!empty($_FILES[validacao::getCampo()]['tmp_name'])){
			validacao::setMensagem(0,'Nenhuma imagem selecionada');
			validacao::validar('texto',$_FILES[validacao::getCampo()]['name'],true);
			$this->imagem=$_FILES[validacao::getCampo()];
		}
		
		validacao::setCampo('status','Status');
		$this->status=validacao::validar('inteiro',$_POST[validacao::getCampo()],true);			
	}
	
	//ATUALIZAÇÃO/CADASTRO//REMOÇÃO
	public function salvarRegistro(){
		try{
			//IMAGEM
			if(!empty($this->imagem['name'])){
				try{
					$arquivo=new arquivo();
					$arquivo->uploadImagem($this->imagem,2,array('jpg','jpeg','gif','png'),'banner');
					
					//AMPLIADA
					$arquivo->redimensionarImagem($this->dimensoes[0],$this->dimensoes[1]);
					$arquivo->salvarImagem(SITE_DIR.'imagens/banner/');
					
					//MINIATURA A PARTIR DA AMPLIADA
					$arquivo->redimensionarImagem(130,NULL,100);
					$arquivo->salvarImagem(SITE_DIR.'imagens/banner/miniatura/'); //THUMBNAIL
					
					//FINALIZA ENVIO DA IMAGEM E ADQUIRE NOME FINAL
					$this->imagem=$arquivo->finalizar();
					$this->imagem=$this->imagem[0];
				}
				catch(uploadException $e){
					throw new validacaoException($e->getMensagem(),'imagem');
				}
			}
			
			//CADASTRO
			if(empty($this->id)){					
				$this->cadastrar();
				notificacao::sucesso('Banner cadastrado com sucesso!');
			}
			else{										
				$this->atualizar();
				notificacao::sucesso('Banner atualizado com sucesso!');
			}
		}
		catch(notificacaoException $e){ header('Location: sistema.php'); }
	}
	
	public function removerRegistro(){
		try{
			if(empty($this->id)) throw new validacaoException('Falha coletando dados de registro para remoção.');
							
			//MOVE IMAGEM (PARA BACKUP)
			$registro=$this->getRegistroBD();
			$registro=$registro[0];
							
			if(!empty($registro['imagem'])&&file_exists(SITE_DIR.'imagens/banner/'.$registro['imagem'])){
				diretorio::criaDir(ADM_DIR.'_temp/removidas/banner/miniatura/',0777);
				rename(SITE_DIR.'imagens/banner/'.$registro['imagem'],ADM_DIR.'_temp/removidas/banner/'.$registro['imagem']);
				rename(SITE_DIR.'imagens/banner/miniatura/'.$registro['imagem'],ADM_DIR.'_temp/removidas/banner/miniatura/'.$registro['imagem']);
			}
			
			$this->status=0;
			$this->salvarRegistro();		
			
			notificacao::erro('Banner removido com sucesso!');
		}
		catch(notificacaoException $e){ header('Location: sistema.php?modulo='.$_GET['modulo'].'&acao=gerenciar'); }
	}
?>