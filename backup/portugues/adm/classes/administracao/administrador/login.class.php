<?php
include($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/uteis/nomenclatura.class.php");
include_once($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/administracao/notificacao.class.php");
	class login extends notificacao{
		private $tabela='admin';
		private $tabelaLog='admin_log';

		private $db;

		private $id;
		private $nome;
		private $usuario;
		private $senha;

		private $sessao;
		private $timeOut=10;

		function __construct(){
			$this->db=new db();

			if(!empty($_SESSION['admin'])){
				try{
					if(!$_SESSION['admin']['login']) throw new validacaoException('');
					if(empty($_SESSION['admin']['id'])) throw new validacaoException('');
					if(empty($_SESSION['admin']['sessao'])) throw new validacaoException('');
					if(empty($_SESSION['admin']['usuario'])) throw new validacaoException('');
					if(empty($_SESSION['admin']['nome'])) throw new validacaoException('');

					$this->id=$_SESSION['admin']['id'];
					$this->sessao=$_SESSION['admin']['sessao'];
					$this->usuario=$_SESSION['admin']['usuario'];
					$this->nome=$_SESSION['admin']['nome'];

				}
				catch(validacaoException $e){
					$this->logout();

					$notificacao=new notificacao();
					$notificacao->alerta('Inconsistência de dados de acesso, faça login novamente.');

					header("Location: sistema.php");
					exit();
				}
			}
		}

		//LOGIN
		public function loginSistema(){
			validacao::setCampo('usuario','Usuário');
			$usuario=nomenclatura::nomeSeguro($_POST[validacao::getCampo()],true); //TORNAR DADOS MINUSCULOS E SEM ESPAÇOS
			$this->usuario=validacao::validar('texto',$usuario,true);

			validacao::setCampo('senha','Senha');
			validacao::setLen(4,12);
			$this->senha=md5(validacao::validar('senha',$_POST[validacao::getCampo()],true));

			$consulta=$this->buscaLogin();
			$this->logout();

			$this->id=$consulta['id'];
			$this->registraLogin();

			$_SESSION['admin']['login']=true;
			$_SESSION['admin']['id']=$this->id;
			$_SESSION['admin']['sessao']=$this->sessao;
			$_SESSION['admin']['usuario']=$this->usuario;
			$_SESSION['admin']['nome']=$consulta['nome'];

			header('Location: '.(!empty($_GET['hash'])?base64_decode($_GET['hash']):'sistema.php'));
			exit();
		}

		//LOGOUT
		public function logout(){
			unset($_SESSION['admin']);
		}

		//VALIDAÇÕES
		public function timeOut(){
			$desconecta=false;

			if(empty($_SESSION['admin']['login'])||empty($_SESSION['admin']['sessao'])) $desconecta=1;
			else{
				try{
					if(!$admin=$this->verificaSessao()) $desconecta=1;

					$ultimoAcesso=empty($admin['dataUpdate'])?$admin['dataAcesso']:$admin['dataUpdate'];
					$inatividade=floor(data::subtrairData($ultimoAcesso,NULL,'minutos'));

					if($inatividade>$this->timeOut) $desconecta=1;
					else $this->atualizarLogin();
				}
				catch(dataException $e){
					$this->logout();

					$notificacao=new notificacao();
					$notificacao->alerta('Sua sessão expirou por inatividade, faça login novamente.');
				}
			}

			if($desconecta){
				$this->logout();

				$notificacao=new notificacao();
				$notificacao->alerta('Sua sessão expirou por inatividade, faça login novamente.');

				//URL
				$gets=NULL;
				if(!empty($_GET['modulo'])) $gets.=(empty($gets)?'?':'&').'modulo='.$_GET['modulo'];
				if(!empty($_GET['acao'])&&!strstr($_GET['acao'],'remover')) $gets.=(empty($gets)?'?':'&').'acao='.$_GET['acao'];
				if(!empty($_GET['registro'])) $gets.=(empty($gets)?'?':'&').'registro='.$_GET['registro'];

				$url=basename($_SERVER['SCRIPT_FILENAME']).$gets;
				$url=base64_encode($url);

				header("Location: index.php?hash=".$url);
				exit();
			}
		}

		//CONSULTAS
		private function buscaLogin(){
			$admin=$this->db->fetch('SELECT id, nome FROM '.$this->tabela.' WHERE usuario="'.$this->usuario.'" AND senha="'.$this->senha.'"');

			if(count($admin)==0) throw new validacaoException('Acesso não autorizado.','form');
			else if(count($admin)>1) throw new validacaoException('Dados de login corrompidos.','form');

			return $admin[0];
		}

		private function verificaSessao(){
			$admin=$this->db->fetch('SELECT dataAcesso, dataUpdate FROM '.$this->tabelaLog.' WHERE admin='.$_SESSION['admin']['id'].' AND sessao="'.$_SESSION['admin']['sessao'].'"');

			if(count($admin)==0) return false;
			else if(count($admin)>1) return false;

			return $admin[0];
		}

		private function registraLogin(){
			$ip=$_SERVER['REMOTE_ADDR'];
			$dispositivoName=gethostname();
			$dispositivoAgent=$_SERVER['HTTP_USER_AGENT'];
			$this->sessao=md5(uniqid());

			$this->db->query('INSERT INTO '.$this->tabelaLog.' (admin,dataAcesso,ip,dispositivo,agente,sessao) VALUES ('.$this->id.',NOW(),"'.$ip.'","'.$dispositivoName.'","'.$dispositivoAgent.'","'.$this->sessao.'")');
		}

		private function atualizarLogin(){
			$this->db->query('UPDATE '.$this->tabelaLog.' SET dataUpdate=NOW() WHERE admin='.$this->id.' AND sessao="'.$this->sessao.'"');
		}

		//FUNÇÕES
		private function get_real_up_address() {
			if(isset($_SERVER)){
				if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) return $_SERVER["HTTP_X_FORWARDED_FOR"];
				if(isset($_SERVER["HTTP_CLIENT_IP"])) return $_SERVER["HTTP_CLIENT_IP"];
				return $_SERVER["REMOTE_ADDR"];
			}

			if (getenv('HTTP_X_FORWARDED_FOR')) return getenv('HTTP_X_FORWARDED_FOR');
			if (getenv('HTTP_CLIENT_IP')) return getenv('HTTP_CLIENT_IP');
			if (getenv('REMOTE_ADDR')) return getenv('REMOTE_ADDR');

			return NULL;
		}
	}
?>
