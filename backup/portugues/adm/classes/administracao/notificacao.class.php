<?php
include_once($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/seguranca/validacao.class.php");
	class notificacao extends validacao{
		function __construct(){
			parent::__construct();
		}

		public static function sucesso($mensagem){
			$_SESSION['notificacaoUser']['status']=1;
			$_SESSION['notificacaoUser']['mensagem']=$mensagem;
		}

		public static function alerta($mensagem){
			$_SESSION['notificacaoUser']['status']=2;
			$_SESSION['notificacaoUser']['mensagem']=$mensagem;
		}

		public static function erro($mensagem){
			$_SESSION['notificacaoUser']['status']=0;
			$_SESSION['notificacaoUser']['mensagem']=$mensagem;
		}

		public static function getNotificacao(){
			if(!empty($_SESSION['notificacaoUser'])){
				if($_SESSION['notificacaoUser']['status']===0) $classe='erro';
				else if($_SESSION['notificacaoUser']['status']===1) $classe='sucesso';
				else if($_SESSION['notificacaoUser']['status']===2) $classe='alerta';

				echo '<div class="notificacao '.$classe.'"><span>'.$_SESSION['notificacaoUser']['mensagem'].'</span></div>';
				unset($_SESSION['notificacaoUser']);
			}
		}
	}
?>
