<?php
	class diretorio{
		//CRIA DIRETÓRIOS ESTRUTURADAMENTE
		static function criaDir($dir,$permissao){
			if(!file_exists($dir)&&!is_dir($dir)){
				$permissao=str_pad(intval($permissao),4,"0",STR_PAD_LEFT);		
				$dir=str_replace('\\','/',$dir);
				
				$old_umask=umask(0);
				mkdir($dir,0777,true);
				umask($old_umask);
			}
		}
		
		public function deletaDir($dir){
			if(is_dir($dir)){
				$sc = scandir($dir);
				foreach($sc as $a){
					if($a == '.' || $a == '..') continue;
					if(!is_dir($dir . '/' . $a)) unlink($dir . '/' . $a);
					else $this->deleteDir($dir . '/' . $a);
				}
				return rmdir($dir);
			}
		}
	}
?>