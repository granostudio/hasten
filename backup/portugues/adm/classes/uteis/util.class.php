<?php
class util{
	static function teste($var){
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}
	
	static function alertJS($var){
		echo '<script type="text/javascript">';
		echo 'alert("'.$var.'")';
		echo '</script>';
	}
	
	static function locationJS($var){
		echo '<script type="text/javascript">';
		echo 'window.location.assign("'.$var.'")';
		echo '</script>';
	}
}
?>