<?php
class monetario{
	//FUNÇÕES DE CONVERSÃO PARA DADOS MONETÁRIOS
	static function toReal($valor,$cifrao,$formato='mascara'){
		if($formato=='banco') return number_format($valor,2,'.','');
		else if($formato=='mascara') return ($cifrao?'R$':'').number_format($valor,2,'.',',');
		else return ($cifrao?'R$':'').number_format($valor,2,',','.');
	}
}
?>