<?php
	class nomenclatura{
		static function nomeSeguro($var,$lowercase=true,$exception=NULL){
			$from=array('á','ã','â','à','ä',
						'é','ê','è','ë',
						'í','î','ì','ï',
						'ó','ô','ò','õ','ö',
						'ú','û','ù','ü',
						'ç',
						'+',' ','_',';','(',')','?','!',':','.',',','-',
						'´','`','^','~','¨','#','$','%','&','*','/','\\','\'','"','@');
						  
			$to=array('a','a','a','a','a',
					  'e','e','e','e',
					  'i','i','i','i',
					  'o','o','o','o','o',
					  'u','u','u','u',
					  'c',
					  '-','-','-','-','-','-','-','-','-','-','-','',
					  '','','','','','','','','','','','','','','');

			//EXCEPTIONS (VALORES DE PREÇOS)
			if(!empty($exception)){
				foreach($exception as $remover){
					$indice=array_search($remover,$from);
					unset($from[$indice]);
					unset($to[$indice]);
				}
			}

			//SENSITIVE (URLS)
			if($lowercase) $retorno=str_replace($from,$to,trim(mb_strtolower($var,CONFIG_CHARSET)));
			else $retorno=str_replace($from,$to,trim($var));
			
			//REMOVER MULTITRACES (IMAGENS)
			do{ $retorno=str_replace('--','-',$retorno); }
			while(strstr($retorno,'--'));
			
			if(substr($retorno,0,1)==='-') $retorno=substr($retorno,1);
			if(substr($retorno,strlen($retorno)-1,1)==='-') $retorno=substr($retorno,0,strlen($retorno)-1);
			
			return $retorno;
		}
				
		static function removeEspacos($var){
			$var=trim($var);
			$var=str_replace("\r","",$var);
			$var=str_replace("\n","",$var);
			$var=str_replace("\r\n","",$var);
			$var=str_replace("\t","",$var);
			$var=str_replace(" ","",$var);
			$var=preg_replace("/(<br.*?>)/i","",$var);	
			
			return $var;
		}
	}
?>