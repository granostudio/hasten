<script type="text/javascript">
    $(function(){
        $('.icone.coments').click(function(){
            $(this).parent().children('div.coments').slideToggle();
        })
    })
</script>

<?php if($_GET['modulo']==="artigo_comentario"){ ?>
    <h3>Artigos - Comentários</h3>
    <div class="tab_content">                        
        <?php
            if(!empty($_GET['acao'])&&$_GET['acao']==='gerenciar'){
                $artigos=new artigo();
                $artigos=$artigos->getRegistros();
                
                if(!empty($artigos)){                           
                    foreach($artigos as $artigo){
                        if(isset($artigo['comentarios'])&&!empty($artigo['comentarios'])){
                            echo "<div class='registro'>";
                                echo "<div class='conteudo' style='float:left; width:100%; margin:4px 0;'>";
                                    echo "<p><strong>".data::converterData($artigo['data'],true,'html',true)."</strong> - ".texto::chamada($artigo['titulo_pt'],80).'</p>';
                                  
                                        echo "<a href='javascript:void();' class='icone coments'><img src='imagens/icones/acoes/duplicar.png' title='Exibir todos os comentários' alt='Exibir todos os comentários' /></a>";
                                        
                                        $comentarios='<div class="coments" style="margin-top:10px; float:left; width:100%; display:none;">';

                                        $alerta=0;
                                        foreach($artigo['comentarios'] as $comentario){
                                            if($comentario['status']==2) $alerta++;

                                            $comentarios.="<div class='conteudo' style='float:left; width:98%; background:#fdffd9;'>
                                                    <p><strong>".data::converterData($comentario['dataPost'],true,'html',true)."</strong> - ".texto::chamada($comentario['comentario'],80).'</p>';

                                            $comentarios.='<a href="'.$_SERVER['PHP_SELF'].'?modulo=artigo_comentario&acao=remover&registro='.$comentario['id'].'" class="icone deletar"><img src="imagens/icones/acoes/deletar.png" alt="Deletar Registro" /></a>
                                                           <a href="'.$_SERVER['PHP_SELF'].'?modulo=artigo_comentario&registro='.$comentario['id'].'" class="icone"><img src="imagens/icones/acoes/editar.png" alt="Editar Registro" /></a>';

                                                $statusMS=$comentario['status']==2?"Aguardando aprovação":($comentario['status']==3?"Rejeitado":"Aprovado");
                                                $status=$comentario['status']==2?"aguardando":($comentario['status']==3?"inativo":"ativo");
                                                $comentarios.='<a><img src="imagens/icones/acoes/'.$status.'.png" title="Comentário '.$statusMS.'" alt="Comentário '.$statusMS.'" /></a>';

                                            $comentarios.='</div>';
                                        }
                                        $comentarios.='</div>';
                                    
                                    if($alerta>1) echo "<a><img src='imagens/icones/acoes/atencao.png' title='".$alerta." comentários aguardando aprovação!' alt='".$alerta." comentários aguardando aprovação!' /></a>";
                                    else if($alerta>0) echo "<a><img src='imagens/icones/acoes/atencao.png' title='".$alerta." comentário aguardando aprovação!' alt='".$alerta." comentário aguardando aprovação!' /></a>";

                                    echo $comentarios;
                                echo "</div>";
                            echo "</div>";
                        }
                    }
                }
                else echo "<span class='no_regs'>Nenhum artigo cadastrado, cadastre agora mesmo <a href='sistema.php?modulo=".$_GET['modulo']."'>clicando aqui</a>.</span>";
            }
            else{
        ?>
                <form action="" method="post">
                    <label>Comentário:*</label>
                    <textarea name="<?php echo $campo='comentario'; ?>"><?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?stripslashes($dataRegistro[$campo]):""); ?></textarea>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Status:*</label>
                    <select name="<?php echo $campo='status'; ?>">
                        <option value="">Selecione...</option>
                        <option value='1' <?php echo isset($_POST[$campo])?($_POST[$campo]==1?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==1?'selected="selected"':''):''); ?>>Aprovado</option>
                        <option value='3' <?php echo isset($_POST[$campo])?($_POST[$campo]==3?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==3?'selected="selected"':''):''); ?>>Reprovado</option>
                    </select>
                    <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                    
                    <input type="hidden" name="action" value="salvar_artigo_comentario" />
                    <input type="submit" value="<?php echo !empty($dataRegistro)?'Alterar':'Cadastrar'; ?>" />
                    <input type="button" value="Cancelar" />
                </form>
        <?php } ?>
    </div> 
<?php } ?>