<?php
include($_SERVER['DOCUMENT_ROOT']."portugues/adm/classes/administracao/administrador/login.class.php");

	try{
		if($_SERVER['REQUEST_METHOD']==='POST'){
			if(basename($_SERVER['SCRIPT_NAME'])==="index.php"&&$_POST['action']==="administrador_login"){
				$login=new login();
				$login->loginSistema();
			}
		}
	}
	catch(validacaoException $e){
		if(!empty($_POST['method'])&&$_POST['method']=="ajax"){
			$retorno_message['status']=false;
			$retorno_message['campo']=$e->getCampo();
			$retorno_message['mensagem']=$e->getMensagem();

			echo json_encode($retorno);
			exit();
		}
		else notificacao::alerta($e->getMensagem());
	}
	catch(SistemaException $e){ exit($e->getMensagem()); }
?>
