/* DESENVOLVEDOR - EDWIN B. PANCOTI */
var destaques_len=0;
var destaques=0;
var destaque_trans;

$(function(){	
	/* DESTAQUES */
	destaques_len=$("#destaque_slider .flutua a").length;
	
	if(destaques_len>1){
		$("#destaque_slider .flutua").css("width",destaques_len*225);
		$("#destaque_slider .navigation.left").fadeTo(0,0.1).css("cursor","default");
		
		destaque_trans=setTimeout("destaque_animate(1)",3000);
		
		$("#destaque_slider .navigation").data("status",true).click(function(){
			if($(this).data("status")){
				clearTimeout(destaque_trans);
				destaque_trans=destaque_animate($(this).hasClass("left")?0:1);
			}
		});
	}
	else $("#destaque_slider .navigation").fadeTo(0,0.1).css("cursor","default");
});

function destaque_animate(move){	
	$("#destaque_slider .navigation").data("status",false);
	move?destaques++:destaques--;
	if(destaques==destaques_len) destaques=0;
			
	$("#destaque_slider .flutua").animate({"left":-(destaques*225)},function(){
		if(destaques>0) $("#destaque_slider .navigation.left").fadeTo(400,1).css("cursor","pointer").data("status",true);
		else $("#destaque_slider .navigation.left").fadeTo(400,0.1).css("cursor","default").data("status",false);
	
		if(destaques==destaques_len-1) $("#destaque_slider .navigation.right").fadeTo(400,0.1).css("cursor","default").data("status",false);
		else $("#destaque_slider .navigation.right").fadeTo(400,1).css("cursor","pointer").data("status",true);
		
		destaque_trans=setTimeout("destaque_animate(1)",3000);
	});
}