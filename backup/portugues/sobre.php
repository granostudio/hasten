<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_sobre.png" width="920" height="202" /></div>
        </div>
        
        <div class="conteudo_interno">
        <p>Apoiada na experiência de seu fundador, <strong>Eduardo de Melo</strong>, profissional com mais de 30 anos dedicados à Saúde, a <strong>Hasten</strong> surge para oferecer apoio total à gestão de empresas do segmento que queiram atuar na América Latina. Sabemos do poder econômico dessa indústria nos países emergentes da região, que registram crescimento setorial de 10% ao ano, bem diferente da consolidação percebida em nações mais desenvolvidas.</p>

<p>Com sede nos Estados Unidos e escritórios no Brasil, Chile e México, a Hasten oferece serviços da estratégia à operação:</p>

<ul><li>Representação de marcas</li>
<li>Elaboração e execução de plano de negócio</li>
<li>Desenvolvimento de negócios internacionais</li>
<li>Gestão de vendas complexas</li>
<li>Consultoria</li>
<li>Marketing estratégico</li>
<li>Relações públicas</li>
<li>Formação de redes de distribuição locais</li>
<li>Logística</li>
<li>Orientação sobre particularidades regulatórias e tributárias em cada país</li>
<li>Operação</li></ul><br />

<p>Nosso conhecimento, que inclui as passagens de Eduardo de Melo e equipe por diversos cargos de liderança em países como Estados Unidos, Holanda e Brasil, nos permitiu desenvolver um olhar atento para as oportunidades de negócios e uma metodologia eficaz para que as empresas atinjam crescimento rápido e sustentável.</p>

<p>Um de nossos principais diferenciais é a capacidade de combinar o conhecimento da cultura e das demandas do investidor internacional ao jeito de fazer negócios no Brasil. Desta forma, promovemos a aceleração das vendas, seja em importação ou exportação, o aumento de receita e redução de riscos, nos segmentos de saúde humana e animal.</p>

<p><strong><i>Hasten – Accelerating businesses in healthcare.</i></strong></p>
        </div>
        
        
    </div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
