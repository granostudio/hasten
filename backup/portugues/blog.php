<?php
    define("NMIND",true);
    $idioma=$_SERVER['REQUEST_URI'];
	$idioma=strstr($idioma,"ingles")?"en":"pt";
	$caminho=$idioma!="pt"?"../portugues/":"";

    include($caminho."includes/php/loader.php");
    include($artigo."adm/classes/frontend/artigo.class.php");

    if($_SERVER['REQUEST_METHOD']==="POST"){
    	if(isset($_POST['id'])){
	        $comentario=new artigo_comentario($idioma);

			if($comentario->cadastro($_POST['id'],$_POST['comentario']))
				$_SESSION['retorno']['msg']="Seu comentário foi enviado e aguarda aprovação, obrigado.";
			else $_SESSION['retorno']['msg']="Falha no envio do comentário, tente novamente mais tarde.";

			$_SESSION['retorno']['id']=$_POST['id'];
			header("Location: ".$_SERVER['HTTP_REFERER']);
		}
    }
    else if(isset($_SESSION['retorno'])){
    	$retorno['id']=$_SESSION['retorno']['id'];
    	$retorno['msg']=$_SESSION['retorno']['msg'];

    	unset($_SESSION['retorno']);
    }

    $artigo=new artigo($idioma);
    $artigos=$artigo->todos();

    if(isset($_GET['filtro'])&&!empty($_GET['filtro']))
		$artigos_conteudo=$artigo->tags($_GET['filtro']);
	else $artigos_conteudo=$artigos;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/blog.css" rel="stylesheet" type="text/css" />

    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
    <script src="includes/js/jquery.js" type="text/javascript"></script>
    <script src="includes/js/efeitos_box.js" type="text/javascript"></script>
    <script type="text/javascript">
	    $(function(){
			$(".artigos_antigos").data('status',0).click(function(){
				$(this).data('status',$(this).data('status')+1);
				if($(this).data('status')%2==1)
					$(this).html('Ocultar comentários');
				else
					$(this).html('Mostrar comentários');

				$(this).parent().children('.hide').slideToggle();
			})
	    })
    </script>
</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_blog.png" width="920" height="202" /></div>
        </div>

        <div class="cont_esquerda">

<h1  class="titulo">ARTIGOS RECENTES</h1>

<?php
	//RECENTES
	if(!empty($artigos)){
		$ultimos=array_slice($artigos, 0, 5);

		foreach($ultimos as $artigo){
			echo '<div class="recentes">
			<a href="#article'.$artigo['id'].'">'.$artigo['titulo'].'</a>
			</div>';
		}
	}
	else echo 'Nenhum registro disponível.';
?>

<br class="quebra" />

<h1 class="titulo">ARQUIVOS</h1>

<?php
	//ARQUIVOS
	if(!empty($artigos)){
		$meses=array('01'=>"Janeiro",
					 '02'=>"Fevereiro",
					 '03'=>"Março",
					 '04'=>"Abril",
					 '05'=>"Maio",
					 '06'=>"Junho",
					 '07'=>"Julho",
					 '08'=>"Agosto",
					 '09'=>"Setembro",
					 '10'=>"Outubro",
					 '11'=>"Novembro",
					 '12'=>"Dezembro");

		$close=0;
		$mes=$printed="";
		foreach($artigos as $artigo){
			$mes=explode(' ',$artigo['data']);
			$mes=explode('-',$mes[0]);
			$mesVR=$mes[1].'-'.$mes[0];

			if($mesVR!=$printed){
				if($close>0) echo '</p>';
				$close++;
				$printed=$mesVR;

				echo '<div class="'.$mesVR.' box-geral">
						<div class="arquivos">'.$meses[$mes['1']].'</div>
					</div>
					<p class="'.$mesVR.' hide texto">';
			}

			echo '<span class="recentes">
					<a href="#article'.$artigo['id'].'">'.$artigo['titulo'].'</a>
				</span>';
		}
		echo '</p>';
	}
	else echo 'Nenhum arquivo disponível.';
?>

<br class="quebra" /><br />

<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/MemberProfile" data-id="https://www.linkedin.com/in/eduardodemelohasten" data-format="hover" data-related="false" data-text="Eduardo de Melo"></script>
</div>


<div class="cont_direita">
<?php
	//ARQUIVOS
	if(!empty($artigos_conteudo)){
		foreach($artigos_conteudo as $artigo){
			$data=data::converterData($artigo['data'],true,'html');
			$data=explode(' ',$data);

			$imagem=$idioma!="pt"?"../portugues/":"";
			$imagem.="imagens/artigo/".$artigo['imagem'];

			echo '<a name="article'.$artigo['id'].'"><p class="tit_not">'.$artigo['titulo'].'</p></a>
				<p class="data">'.$data[0].'</p>
				<img class="not_img" src="'.$imagem.'" width="289" height="188" />
				<p>'.$artigo['texto'].'</p>
				<br class="quebra" />

				<p class="tit_not">Tags</p>';

				$tags=explode(',',$artigo['tags']);
				if(!empty($tags)){
					echo '<div class="tags_linha">';

					foreach($tags as $tag){
						echo '<div class="tags">
								<form method="GET" action="">
									<input type="hidden" name="filtro" value="'.$tag.'"/>
									<input type="submit" value="#'.$tag.'"/>
								</form>
							</div>';
					}
					echo '</div>';
				}

			echo '<br class="quebra" />';

			if(array_key_exists('comentarios',$artigo)){
				$total_comments=count($artigo['comentarios']);
				$total_comments=$total_comments.' comentário'.($total_comments>1?'s':'');

				echo '<p class="tit_not">'.$total_comments.'</p>';

				echo '<div class="comentarios_show">';
					$show=3;
					foreach($artigo['comentarios'] as $comentario){
						echo '<div class="comentarios '.($show<=0?"hide":"").'"><p>'.$comentario['comentario'].'</p></div>';
						$show--;
					}
					if($show<0){
						echo '<div class="artigos_antigos">Mostrar comentários</div>';
					}
				echo '</div>';
			}

			echo '<br class="quebra" />
				<p class="tit_not">Deixe um Comentário</p>
				<form method="POST" action="">
					<textarea name="comentario"></textarea>
					<input type="hidden" name="id" value="'.$artigo['id'].'">';

			echo '<input class="botao" type="submit" value="Publicar comentário">
				</form>
				<br class="quebra" />';
				if(isset($retorno['msg'])&&!empty($retorno['msg'])&&$retorno['id']==$artigo['id'])
					echo '<p style="width: 400px; margin: -19px 0px 40px;">'.$retorno['msg'].'</p>';
		}
	}
	else echo 'Nenhum artigo cadastrado.';
?>

</div>
    </div><!-- fecha div centro -->

   <?php include_once('includes/php/rodape.php') ?>

</div><!-- fecha div tudo -->
</body>
</html>
