<?php
	ini_set('display_errors', 1); error_reporting(E_ALL);
	if(!defined('NMIND')) die('Acesso Negado');
	session_start();
	
	date_default_timezone_set('America/Sao_Paulo');
	header ('Content-type: text/html; charset=UTF-8');
	define('CONFIG_CHARSET','UTF-8');

	//if($_SERVER['HTTP_HOST']=='localhost') define('CLASS_DIR','/Applications/XAMPP/xamppfiles/htdocs/www/bch/public_html/classes/');
	if($_SERVER['HTTP_HOST']=='localhost'){
		define('SITE_DIR','D:\workspace\localhost\clientes\newqi\infoarts\portugues\\');
		define('ADM_DIR','D:\workspace\localhost\clientes\newqi\infoarts\portugues\adm\\');
		define('CLASS_DIR','D:\workspace\localhost\clientes\newqi\infoarts\portugues\adm\classes\\');
	}
	else{
		define('SITE_DIR','/home/hasten-llc/www/portugues/');
		define('ADM_DIR','/home/hasten-llc/www/portugues/adm/');
		define('CLASS_DIR','/home/hasten-llc/www/portugues/adm/classes/');
	}
	define('SITE_URL','http://www.hasten-llc.com');
	
	//VALIDAÇÕES GERAIS DE CONFIGURAÇÕES NECESSÁRIAS
	if(!defined('SITE_DIR')){ echo 'A especificação do diretório do site é obrigatória.'; exit(); }
	if(!defined('ADM_DIR')){ echo 'A especificação do diretório de administração é obrigatória.'; exit(); }
	if(!defined('CLASS_DIR')){ echo 'A especificação do diretório de classes é obrigatória.'; exit(); }
	if(!defined('SITE_URL')){ echo 'A especificação da URL do site é obrigatória.'; exit(); }

	function __autoload($class_name){
		$directories = array(
			CLASS_DIR.'frontend/',
			CLASS_DIR.'frontend/exceptions/',
			CLASS_DIR.'email/',
			CLASS_DIR.'seguranca/',
			CLASS_DIR.'uteis/',
			CLASS_DIR.'bd/'
		);
		
		foreach($directories as $directory){
			if(file_exists($directory.$class_name.'.class.php')){
				require_once($directory.$class_name.'.class.php');
				return;
			}            
		}
	}	
	
	/*IDIOMA*/
	$idioma=$_SERVER['REQUEST_URI'];
	$idioma=strstr($idioma,"ingles")?"en":"pt";
?>