<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_humana.png" width="920" height="202" /></div>
        </div>
        
        <div class="conteudo_interno">
      <p> Com o aumento da expectativa de vida e uma população de idosos em ascensão, o Brasil vive um momento bastante favorável aos investimentos da indústria de saúde.</p>
<p>As pessoas começam a tomar mais consciência sobre sua saúde e demandam mais acesso aos serviços. Dos mais de 15 mil hospitais da América Latina, 7 mil estão no Brasil e, de 1 milhão de médicos, quase 400 mil são brasileiros. São 51 milhões de beneficiários de planos de saúde e 21 milhões de planos odontológicos.</p>
<p>Tudo isso resulta num maior consumo de materiais, equipamentos, dispositivos e medicamentos. Em 2015, o mercado de saúde brasileiro ultrapassará os US$ 52 bilhões, representando 43,1% das receitas da América Latina. </p>
<p>Boa parte desse volume terá como lastro programas de incentivo do Governo Federal:</p>
<ul><li>Os investimentos do Programa para o Desenvolvimento do Complexo Industrial da Saúde (Procis), destinados a aumentar a capacidade de inovação, totalizarão US$ 1 bilhão.</li>
<li>Órgãos de fomento, como o Banco Nacional para o Desenvolvimento Econômico e Social (BNDES) e a Financiadora de Estudos e Projetos (Finep), oferecem condições de financiamento atrativas para o setor.</li>
<li>Iniciativas do Ministério da Saúde, como a destinação de R$ 280 milhões para as unidades oncológicas, vão gerar investimentos de até R$ 100 milhões em equipamentos de radioterapia.</li>
<li>O ministério também deve adquirir mais de 2 mil ambulâncias em 2015, renovar 8 mil unidades básicas de saúde, criar2 mil leitos para urgência e emergência e implementar 500 unidades de pronto-socorro.</li>
<li>Normas e padrões para o setor foram harmonizados com as regulamentações internacionais, para facilitar a comercialização de produtos e a transferência de tecnologia.</li></ul><br />
<p><strong>A consultoria Frost&Sullivan aponta, ainda, que, até o fim de 2015, a indústria farmacêutica e de biotecnologia irá liderar o mercado, com uma fatia de 78,9%, mas os maiores percentuais de crescimento serão obtidos pelas áreas de dispositivos médicos, com 15,8%, e TI, com 14,8%.</strong></p><br />
<img src="imagens/grafico_humana.jpg" width="921" height="438" />
<p>Fontes: IBGE, Ministério da Saúde, Ministério do Desenvolvimento, ANS, Anvisa, Abimo, Abimed, Brazilian Health Devices e Frost&Sullivan</p>
</div>
        
        
    </div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
