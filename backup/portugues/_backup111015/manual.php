<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    	<link href="includes/css/manual.css" rel="stylesheet" type="text/css" />

    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_manual.png" width="920" height="202" /></div>
        </div>
        
        <div class="conteudo_interno">
<p class="texto1">Com dimensões continentais, mais de 200 milhões de habitantes e uma demanda crescente por produtos e serviços de saúde, o Brasil é, certamente, um mercado muito atrativo. Mas, para obter os melhores resultados financeiros e operacionais, é necessário conhecer as oportunidades, incentivos e particularidades profundamente.</p>

<h1>ÓRGÃOS DO FOMENTO</h1>
<p><strong>BNDES</strong> – O Banco Nacional para o Desenvolvimento Econômico e Social (BNDES) é um órgão estatal, que oferece diversos mecanismos de apoio financeiro para companhias privadas com sede e administração no Brasil. A liberação do financiamento tem como premissa três fatores estratégicos: inovação, desenvolvimento local e desenvolvimento socioambiental. Outros bancos, nacionais e internacionais, também oferecem linhas de créditos para empresas e investidores.</p>

<p><strong>Finep</strong> – A Financiadora de Estudos e Projetos (Finep) tem o objetivo de financiar ou subsidiar a inovação em todos os seus estágios, da pesquisa básica à comercialização dos produtos e serviços, em empresas instaladas no Brasil. Saúde é uma das áreas prioritárias da agência, especialmente nos segmentos de oncologia, biotecnologia e dispositivos médicos.</p>

<p><strong>Incentivos fiscais</strong> – O governo brasileiro oferece incentivos fiscais para setores-chave da economia, incluindo a indústria de saúde, em seus três níveis: federal, estadual e municipal. Essas reduções ou isenções de impostos são garantidas após a apresentação, pela empresa, do projeto a ser desenvolvido no País e suas contrapartidas, como volume de investimento e criação de empregos. Também há incentivos para importação de maquinário e equipamentos não produzidos no Brasil.</p>

<p><strong>Propriedade Intelectual</strong> – As leis brasileiras de propriedade intelectual seguem as boas práticas mundiais e o governo é signatário de diversas convenções internacionais sobre o tema. Como em qualquer país, os direitos de propriedade industrial precisam ser registrados no Brasil para serem usufruídos. Em Saúde Humana, também é necessário obter a licença da Agência Nacional de Vigilância Sanitária (Anvisa) para a comercialização dos produtos.</p>

<p><strong>Impostos e taxas</strong> – No Brasil, cada esfera de governo tem autonomia para instituir taxas e outras contribuições. Por isso, as variações tributárias são grandes para cada localidade. Esse sistema intrincado requer profissionais especializados, com conhecimento sobre o cenário nacional e que busquem as melhores formas de garantir o dia a dia de uma operação comercial local. </p>

<h1>PROMOÇÃO DOS INVESTIMENTOS</h1>

<p><strong>Apex Brasil</strong> – A agência de promoção do comércio e investimento no Brasil trabalha para atrair recursos para setores considerados estratégicos. O foco está não só nas exportações, mas também na atração de empresas estrangeiras que tragam inovações tecnológicas e novos modelos de negócios para o País.</p>

<p><strong>Ministério das Relações Exteriores</strong> – É o órgão responsável pela implementação da diplomacia comercial, o que inclui a promoção de investimentos. Entre suas atividades estão a identificação de parceiros-chave, preparação de visitas programadas e organização de informações sobre oportunidades de negócios, aspectos regulatórios e indicadores econômicos, entre outros.</p>

<h2>Para saber mais sobre como fazer negócios no Brasil, entre em contato com a Hasten!</h2></div>
        
        
    </div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
