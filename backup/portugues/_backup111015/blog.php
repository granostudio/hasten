<?php
    define("NMIND",true);
    include("includes/php/loader.php");

    if($_SERVER['REQUEST_METHOD']==="POST"){
        $comentario=new artigo_comentario($idioma);
        $comentario->cadastro($_POST['id'],$_POST['comentario']);
    }
	
	$artigo=new artigo($idioma);
	$artigos=$artigo->todos();	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    	<link href="includes/css/blog.css" rel="stylesheet" type="text/css" />

    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
    <script src="includes/js/jquery.js" type="text/javascript"></script>
    <script src="includes/js/efeitos_box.js" type="text/javascript"></script>

</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_blog.png" width="920" height="202" /></div>
        </div>
        
        <div class="cont_esquerda">

<h1  class="titulo">ARTIGOS RECENTES</h1>

<?php
	//RECENTES
	if(!empty($artigos)){
		$ultimos=array_slice($artigos, 0, 5);

		foreach($ultimos as $artigo){
			echo '<div class="recentes">
			<a href="#article'.$artigo['id'].'">'.$artigo['titulo'].'</a>
			</div>';
		}
	}
	else echo 'Nenhum registro disponível.';
?>

<br class="quebra" />

<h1 class="titulo">ARQUIVOS</h1>

<?php
	//ARQUIVOS
	if(!empty($artigos)){
		$meses=array(01=>"Janeiro",
					 02=>"Fevereiro",
					 03=>"Março",
					 04=>"Abril",
					 05=>"Maio",
					 06=>"Junho",
					 07=>"Julho",
					 08=>"Agosto",
					 09=>"Setembro",
					 10=>"Outubro",
					 11=>"Novembro",
					 12=>"Dezembro");
		
		$mes;
		foreach($artigos as $artigo){
			$mes=explode(' ',$artigo['data']);
			$mes=explode('-',$mes[0]);
			$mes=$mes[1].'-'.$mes[0];
			util::teste($mes);
			
			'<div class="janeiro box-geral">
				<div class="arquivos ">Janeiro</div>
			</div>
			<p class="janeiro hide texto">
			
				<span class="recentes">
				<a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
				</span>
				<span class="recentes">
				<a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
				</span>
				<span class="recentes">
				<a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
				</span>
				<span class="recentes">
				<a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
				</span>
			</p>';
		}
	}
	else echo 'Nenhum arquivo disponível.';
?>

<!--JANEIRO-->
<div class="janeiro box-geral">
	<div class="arquivos ">Janeiro</div>
</div>
<p class="janeiro hide texto">

    <span class="recentes">
    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
    </span>
    <span class="recentes">
    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
    </span>
    <span class="recentes">
    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
    </span>
    <span class="recentes">
    <a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec leo dolor in the tuil.</a>
    </span>
</p>

<br class="quebra" />

<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<script type="IN/MemberProfile" data-id="https://www.linkedin.com/pub/tania-machado/21/114/a6" data-format="hover" data-related="false" data-text="Tania Machado"></script>
</div>   
        
        
<div class="cont_direita">
<?php
	//ARQUIVOS
	if(!empty($artigos)){
		foreach($artigos as $artigo){
			$data=data::converterData($artigo['data'],true,'html');
			$data=explode(' ',$data);
			
			$imagem=$idioma!="pt"?"../portugues/":"";
			$imagem.="imagens/artigo/".$artigo['imagem'];
			
			echo '<a name="article'.$artigo['id'].'"><p class="tit_not">'.$artigo['titulo'].'</p></a>
				<p class="data">'.$data[0].'</p> 
				<img class="not_img" src="'.$imagem.'" width="289" height="188" />
				<p>'.$artigo['texto'].'</p>
				<br class="quebra" />
				
				<p class="tit_not">Tags</p>';
				
				$tags=explode(',',$artigo['tags']);
				if(!empty($tags)){
					echo '<div class="tags_linha">';
		
					foreach($tags as $tag){
						echo '<p class="tags"><a href="javascript:void()">#'.$tag.'</a></p>';
					}
					echo '</div>';
				}
			
			echo '<br class="quebra" />';
			
			if(array_key_exists('comentarios',$artigo)){
				$total_comments=count($artigo['comentarios']);
				$total_comments=$total_comments.' comentário'.($total_comments>1?'s':'');
				
				echo '<p class="tit_not">'.$total_comments.'</p>';
				
				foreach($artigo['comentarios'] as $comentario){
					echo '<div class="comentarios"><p>'.$comentario['comentario'].'</p></div>';
				}
			}
			echo '<div class="artigos_antigos">mais comentários</div>
				<br class="quebra" />
				<p class="tit_not">Deixe um Comentário</p>
				<form method="POST" action="">
					<textarea name="comentario"></textarea>
					<input type="hidden" name="id" value="'.$artigo['id'].'">
				
					<input class="botao" type="submit" value="Publicar comentário">
				</form>
				<br class="quebra" />';
		}
	}
	else echo 'Nenhum artigo cadastrado.';
?>

</div>        
    </div><!-- fecha div centro --> 
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div><!-- fecha div tudo --> 
</body>
</html>
