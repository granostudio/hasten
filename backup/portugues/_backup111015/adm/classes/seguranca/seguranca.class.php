<?php
	class seguranca extends db{
		protected $remove;
		
		function __construct(){
			parent::__construct();
		}
		
		//MÉTODOS PROTEGIDOS	
		protected function gerarSenhaSegura($tamanho,$maiusculas,$minusculas,$numeros,$simbolos){			
			$ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiusculas
			$mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
			$nu = "0123456789"; // $nu contem os numeros
			$si = "!@#$%¨&*()_+="; // $si contem os sibolos
											
			if($maiusculas) $senha.= str_shuffle($ma);
			if($minusculas) $senha.= str_shuffle($mi);
			if($numeros) 	$senha.= str_shuffle($nu);
			if($simbolos) 	$senha.= str_shuffle($si);
			
			//retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variavel $tamanho
			return substr(str_shuffle($senha),0,$tamanho);
		}
		
		//HASH DE VALIDAÇÃO DE E-MAILS
		protected function geraHash(){			
			$hash=uniqid('hash'.date('h:i:s d/m/Y'));
			$hasher=md5($hash);
			
			return $hasher;
		}
		
		//CRIPTOGRAFIAS
		protected function hashArray($valor,$modo){
			if($modo) $retorno=str_replace('=','hash_',strrev(base64_encode(http_build_query($valor))));
			else parse_str(base64_decode(strrev(str_replace('hash_','=',$valor))),$retorno);
			
			return $retorno;
		}
		
		protected function criptografar($id){
			$criptografia=urlencode($id);
			$criptografia=base64_encode($criptografia);
			$criptografia=strrev($criptografia);
			$criptografia=base64_encode($criptografia);
			
			return $criptografia;
		}
		
		protected function descriptografar($id){
			$criptografia=base64_decode($id);
			$criptografia=strrev($criptografia);
			$criptografia=base64_decode($criptografia);
			$criptografia=urldecode($criptografia);
			
			return $criptografia;
		}		
		
		//TRATAMENTO DE DADOS PARA BANCO
		protected function tratarDado($dado){
			return trim($dado);
		}
	}
?>