<?php
	class validacao extends seguranca{
		//DEFINIÇÕES
		private $metodo;
		private $campo;
		private $valor;		
		private $mensagem;
		private $obrigatorio;
		
		//ADICIONAIS
		private $minlen;
		private $maxlen;
		private $criptografia;
		private $relacionado;
		
		function __construct(){
			parent::__construct();
		}
		
		private function reseta(){
			foreach(get_class_vars(get_class($this)) as $variavel=>$valor){
				if($variavel=='session_nome') continue;
				else if($variavel=='campo') $this->$variavel=array('name'=>'form','label'=>'form');
				else if($variavel=='mensagem') $this->$variavel=array();
				else $this->$variavel=NULL;
			}
		}
		
		//SETTERS
		protected function setSessao($var){		$this->session_nome=$var;	}
		protected function setValor($var){		$this->valor=$var;			}
		
		public function setLen($min,$max){
			$this->minlen=$min;
			$this->maxlen=$max;
		}
		
		protected function setCriptografia($var){	$this->criptografia=$var;	}
		protected function setRelacao($var){		$this->relacionado=$var;	}
		
		public function setMensagem($indice,$var){
			$this->mensagem[$indice]=$var;
		}
		
		public function setCampo($campo,$label){
			$this->reseta();
			$this->campo['label']=$label;
			$this->campo['name']=$campo;
		}
		
		//GETTERS
		public function getCampo(){ return $this->campo['name']; }
		
		//FUNÇÕES
		//GERAÇÃO DE SESSÕES
		private function geraSessoes(){
			if(!strstr($this->campo['name'],'senha'))
				$_SESSION[$this->session_nome][$this->campo['name']]=$this->valor;
		}

		//DEFINIÇÕES GERAIS DE DADOS
		public function validar($tipo_dado,$valor,$obrigatorio){	
			//TRATAMENTO
			$this->valor=seguranca::tratarDado(str_replace('"','\'',$valor));
			$this->obrigatorio=$obrigatorio?true:($this->valor!=''?true:false);
			if(empty($this->valor)&&!$this->obrigatorio) return NULL;
			
			//PRÉ-DEFINIÇÕES
			try{
				switch($tipo_dado){
					//NUMÉRICOS
					case 'id':
					case 'inteiro':
						$this->validaInteiro();
						break;
					
					case 'numerico':
						$this->validaNumerico();
						break;
						
					case 'boleano':
						$this->validaBoleano();
						break;	
													
					case 'telefone':
					case 'celular':			
						$this->validaTelefone();
						if(!empty($this->valor)) $this->valor='('.substr($this->valor,0,2).')'.substr($this->valor,2,($position=$tipo==='celular'?5:4)).'-'.substr($this->valor,$position+2);
						break;
						
					case 'cep':
						$this->validaCep();
						if(!empty($this->valor)) $this->valor=substr($this->valor,0,5).'-'.substr($this->valor,5,3);
						break;
						
					case 'preco':
						$this->validaPreco();
						if($this->valor!='') $this->valor=monetario::toReal($this->valor,false,'banco');
						break;
						
					case 'fator':
						$this->validaFator();
						if($this->valor!='') $this->valor=number_format($this->valor,4);
						break;
						
					case 'cpf':
						$this->validaCPF();
						if(!empty($this->valor)) $this->valor=substr($this->valor,0,3).'.'.substr($this->valor,3,3).'.'.substr($this->valor,6,3).'-'.substr($this->valor,9);
						break;
						
					case 'cnpj':
						$this->validaCNPJ();
						if(!empty($this->valor)) $this->valor=substr($this->valor,0,2).'.'.substr($this->valor,2,3).'.'.substr($this->valor,5,3).'/'.substr($this->valor,8,4).'-'.substr($this->valor,12);
						break;
						
					//DATAS
					case 'data':
					case 'hora':
					case 'datahora':
						if($this->valor!='NOW()') $this->validaData($tipo_dado);
						break;
					
					//SPAN
					case 'analisespan':
					case 'span':
						$this->analisespan();
						break;
					
					//STRINGS
					case 'livre':
						$this->validaFree();
						break;
					
					case 'senha':
						$this->validaSenha();
						break;
							
					case 'email':
						$this->validaEmail();
						break;
						
					case 'link':
						$this->validaLink();
						break;
					
					default:
					case 'padrao':
					case 'string':
						$this->validacaoPadrao();				
						break;
				}
				if(!empty($this->session_nome)) $this->geraSessoes();
				return $this->valor;
			}
			catch(SistemaException $e){
				throw new validacaoException('Ocorreu uma falha de verificação de dados e uma notificação foi enviada ao administrador. Tente novamente mais tarde','form');
			}
			catch(Exception $e){
				throw new validacaoException(str_replace('|||LABEL|||',$this->campo['label'],$e->getMessage()),$this->campo['name'],$this->valor);
			}
		}
		
		//VALIDAÇÕES
		//DADOS NUMÉRICOS
		private function validaInteiro(){
			$this->validaNumerico();
			if($this->valor<1) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Campo |||LABEL||| deve conter um número inteiro maior que zero.');
		}
				
		private function validaNumerico($config=NULL){
			$this->valor=mascara::unmask($this->valor,$config['nounmask']);
			if(strlen($this->valor)>0) $this->validaTamanho();
			
			if($this->valor===''&&!$this->obrigatorio) return true;
			if($this->valor===''&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Campo |||LABEL||| em branco.');
			
			if(!is_numeric(str_replace(array('.',','),'',$this->valor))&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[1])?$this->mensagem[1]:'O |||LABEL||| informado não é numérico.');
		}
		
		private function validaBoleano(){
			$this->valor=empty($this->valor)?0:$this->valor;
			$this->validaNumerico();
			
			if(!($this->valor==1||$this->valor==0)) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'O |||LABEL||| informado não é um valor booleano.');
		}
		
		private function validaTelefone(){
			$this->validaNumerico();
			if(strlen($this->valor)<10) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Número de |||LABEL||| informado está incompleto.');
			if(strlen($this->valor)>12) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado é inválido.');
		}
		
		private function validaCep(){
			$this->validaNumerico();
			if(strlen($this->valor)<8) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Número de |||LABEL||| informado está incompleto.');
			if(strlen($this->valor)>8) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado é inválido.');
		}
		
		private function validaCPF(){
			$this->validaNumerico();
			if(strlen($this->valor)<11) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Número de |||LABEL||| informado está incompleto.');
			if(strlen($this->valor)>11) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado é inválido.');
			
			if(!$this->CPFvalido($this->valor)) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado não é válido.');
		}
		
		private function validaCNPJ(){
			$this->validaNumerico();
			if(strlen($this->valor)<14) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Número de |||LABEL||| informado está incompleto.');
			if(strlen($this->valor)>14) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado é inválido.');
			
			if(!$this->CNPJvalido($this->valor)) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Número de |||LABEL||| informado não é válido.');
		}
		
		private function validaPreco(){
			$this->validaNumerico(array('nounmask'=>array('.')));
			if($this->valor==0&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'Campo |||LABEL||| não pode ser zero.');
		}
		
		private function validaFator(){
			$this->validaNumerico(array('nounmask'=>array('.')));
		}
				
		//DADOS DE STRING, TEXTO
		private function validaTexto(){
			if(strlen(strip_tags($this->valor))>0) $this->validaTamanho();
			
			if(strip_tags($this->valor)===''&&!$this->obrigatorio) return true;
			if(strip_tags($this->valor)===''&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Campo |||LABEL||| em branco.');	
		}
		
		private function validaEmail(){
			$this->validaTexto();
			if(!preg_match("/^[0-9a-zA-Z_.-]{1,64}@([a-z0-9_]{2,64}\.)([a-z]{3}(\.[a-z]{2})?|[a-z]{2})$/",$this->valor)) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'O |||LABEL||| informado é inválido');
		}
		
		private function validaLink(){
			$this->validaTexto();
			if(!preg_match("|^http(s)?://[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i",$this->valor)) throw new Exception(!empty($this->mensagem[1])?$this->mensagem[1]:'O |||LABEL||| informado é inválido');
		}
		
		private function validaFree(){
			$this->validaTexto();
		}
		
		private function validacaoPadrao(){
			$this->validaTexto();
			//if(is_numeric(nomenclatura::removeEspacos($this->valor))) throw new Exception(!empty($this->mensagem[1])?$this->mensagem[1]:'Campo |||LABEL||| não pode ser somente número.');			
		}
		
		//SENHA (DEFINIÇÕES -> MINLEN, MAXLEN, TIPO, LIKE)
		private function validaSenha(){
			$this->validaTexto();
			
			if(!empty($this->relacionado))
				if($this->valor!=$_POST[$this->relacionado]) throw new Exception(!empty($this->mensagem[3])?$this->mensagem[3]:'Confirmação de senha não confere com senha informada.');
			
			if(!empty($this->criptografia)){
				switch($this->criptografia){
					case 'md5':
						$this->valor=md5($this->valor);
						break;
					
					case 'base64':
						$this->valor=base64_encode($this->valor);
						break;
						
					default:
						throw new SistemaException('[Validacao] Tentativa de validação de conversão de senha para formato indefinido ['.(implode($definicoes)).'].');
						break;
				}
			}				
		}
		
		//VERIFICAÇÃO DE ROBOTS
		private function analisespan(){
			if($this->valor!='') throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Tentativa considerada ameaça de span. Mensagem não foi enviada!');
			return true;
		}
		
		//DATAS
		private function validaData($formato){
			switch($formato){
				case 'data':
					if(empty($this->valor)&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Nenhuma |||LABEL||| informada.');
					if(!preg_match("/^(\d{4})-(\d{2})-(\d{2})$/",$this->valor)) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'A |||LABEL||| informada é inválida. Valores não equivalem a dados de data.');
					
					$data=explode('-',$this->valor);
					if(($data[0]<1850||$data[0]>2300)||($data[1]<=0||$data[1]>12)||($data[2]<=0||$data[2]>31)) throw new Exception(!empty($this->mensagem[1])?$this->mensagem[1]:'A |||LABEL||| informada não parece uma data válida.');
					
					break;
					
				case 'hora':
					if(empty($this->valor)&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Nenhuma |||LABEL||| informada.');
					if(!preg_match("/^([01][0-9]|2[0-3]):([0-5][0-9])(:([0-5][0-9]))?$/",$this->valor)) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'A |||LABEL||| informada é inválida. Valores não equivalem a dados de hora.');
					break;
				
				case 'datahora':
				default:
					if(empty($this->valor)&&$this->obrigatorio) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'Nenhuma |||LABEL||| informada.');
					if(!preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9])(:([0-5][0-9]))?$/",$this->valor)) throw new Exception(!empty($this->mensagem[0])?$this->mensagem[0]:'A |||LABEL||| informada é inválida. Valores não equivalem a dados de data e hora.');
					break;
			}
			return true;
		}
		
		//GERAIS
		//TAMANHOS DE STRING
		private function validaTamanho(){
			if(!empty($this->minlen)){
				if(empty($this->maxlen)) throw new SistemaException('[Validacao] Maxlen não definido quando informado Minlen ['.$this->minlen.']');
				
				if($this->minlen>$this->maxlen) throw new SistemaException('[Validacao] Maxlen definido com valor superior ao Minlen ['.$this->minlen.']');
			
				if(strlen($this->valor)<$this->minlen) throw new Exception(!empty($this->mensagem[1])?$this->mensagem[1]:'A |||LABEL||| deve ter ao menos '.$this->minlen.' caracteres.');
				if(strlen($this->valor)>$this->maxlen) throw new Exception(!empty($this->mensagem[2])?$this->mensagem[2]:'A |||LABEL||| deve ter no máximo '.$this->maxlen.' caracteres.');
			}
		}
		
		//CPF
		private function CPFvalido($cpf){
			if(strlen($cpf)!=11||$cpf=='00000000000'||$cpf=='11111111111'||$cpf=='22222222222'||$cpf=='33333333333'||$cpf=='44444444444'||$cpf=='55555555555'||$cpf=='66666666666'||$cpf=='77777777777'||$cpf=='88888888888'||$cpf=='99999999999') return false;
			else{
				//Calcula os números para verificar se o CPF é verdadeiro
				for($t=9;$t<11;$t++){
					for($d=0,$c=0;$c<$t;$c++){
						$d+=$cpf{$c}*(($t+1)-$c);
					}
		
					$d=((10*$d)%11)%10;
		
					if($cpf{$c}!=$d) return false;
				}
				return true;
			}
		}
		
		//CNPJ
		private function CNPJvalido($cnpj){
			//Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
			$j=0;
			for($i=0; $i<(strlen($cnpj)); $i++){
				if(is_numeric($cnpj[$i])){
					$num[$j]=$cnpj[$i];
					$j++;
				}
			}
			//Etapa 2: Conta os dígitos, um Cnpj válido possui 14 dígitos numéricos.
			if(count($num)!=14) $isCnpjValid=false;
			
			//Etapa 3: O número 00000000000 embora não seja um cnpj real resultaria um cnpj válido após o calculo dos dígitos verificares e por isso precisa ser filtradas nesta etapa.
			if ($num[0]==0 && $num[1]==0 && $num[2]==0 && $num[3]==0 && $num[4]==0 && $num[5]==0 && $num[6]==0 && $num[7]==0 && $num[8]==0 && $num[9]==0 && $num[10]==0 && $num[11]==0) $isCnpjValid=false;
			else{ //Etapa 4: Calcula e compara o primeiro dígito verificador.
				$j=5;
				for($i=0; $i<4; $i++){
					$multiplica[$i]=$num[$i]*$j;
					$j--;
				}
				
				$soma = array_sum($multiplica);
				$j=9;
				
				for($i=4; $i<12; $i++){
					$multiplica[$i]=$num[$i]*$j;
					$j--;
				}
				
				$soma = array_sum($multiplica);	
				$resto = $soma%11;	
				
				if($resto<2) $dg=0;
				else $dg=11-$resto;
				
				if($dg!=$num[12]) $isCnpjValid=false;
			}
			
			if(!isset($isCnpjValid)){ //Etapa 5: Calcula e compara o segundo dígito verificador.
				$j=6;
				for($i=0; $i<5; $i++){
					$multiplica[$i]=$num[$i]*$j;
					$j--;
				}
				
				$soma = array_sum($multiplica);
				$j=9;
				
				for($i=5; $i<13; $i++){
					$multiplica[$i]=$num[$i]*$j;
					$j--;
				}
				
				$soma = array_sum($multiplica);	
				$resto = $soma%11;	
				
				if($resto<2) $dg=0;
				else $dg=11-$resto;
				
				if($dg!=$num[13]) $isCnpjValid=false;
				else $isCnpjValid=true;
			}
			return $isCnpjValid;
		}
	}
?>