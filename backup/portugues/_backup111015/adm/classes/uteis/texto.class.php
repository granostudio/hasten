<?php
	class texto{
		//ENCURTA TEXTO CONFORME QUANTIDADE DESEJADA (string,quantidade)
		static function chamada($str,$ix){
			$str=strip_tags(str_replace(array('<br />','<br>'),array(' ',' '),$str));
			
			if(strlen($str)>$ix){
				$trecho = substr($str,0,$ix);
				$esp = strrpos($trecho," ");//pega a última ocorrência do caractere " " no trecho acima
				$chamada = substr($str,0,$esp);//faz de novo o substr usando o indice $esp para pegar o espaço
				
				if($chamada!="") return $chamada;
				else{
					$first=explode(" ",$str);
					return $first[0].'...';
				}
			}
			else return $str;
		}
		
		//REVERSÃO DE NL2BR
		static function br2nl($text){
			//$brs=array("<br />","<br>","<br/>");
			//return str_replace($brs,"",$text); 
			return $text;
		}
	}
?>