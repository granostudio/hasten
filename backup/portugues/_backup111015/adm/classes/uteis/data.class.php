<?php
	class data{
		//CONVERSÃO DE DATAS PARA HTML OU BD
		static function converterData($data,$comHora,$formatoDestino=NULL,$inverse=false){
			if(!empty($data)){
				$separadorOrigem;
				$separadorDestino;
				
				if($formatoDestino=='banco'||$formatoDestino=='bd'||$formatoDestino=='db'){
					$separadorOrigem = '/';
					$separadorDestino = '-';
				}
				else{
					$separadorOrigem = '-';
					$separadorDestino = '/';
				}
				
				if($comHora){
					$espaco = strpos($data,' ');
					$dataOk = substr($data,0,$espaco);
					$hora = substr($data,$espaco);
					$arr = explode($separadorOrigem,$dataOk);
					
					if($inverse) return $hora." - ".$arr[2].$separadorDestino.$arr[1].$separadorDestino.$arr[0];
					else return $arr[2].$separadorDestino.$arr[1].$separadorDestino.$arr[0].$hora;
				}
				else{
					$arr = explode($separadorOrigem,$data);
					if(strstr($arr[2],':'))
						$arr[2] = substr($arr[2],0,2);
					return $arr[2].$separadorDestino.$arr[1].$separadorDestino.$arr[0];
				}
			}
		}
				
		//SUBSTRAI DATAS
		static function subtrairData($inicio=NULL,$fim=NULL,$retorno='minutos',$timeview=false){
			if($timeview) echo date_default_timezone_get();
			
			$dataHora=date('Y-m-d H:i:s');
			$data=date('Y-m-d');
			$hora=date('H:i:s');
					
			if(empty($inicio)&&empty($fim)) throw new dataException('Nenhuma data para cálculo informada.');
			else if(!empty($inicio)&&!empty($fim)){
				$vrinicio=explode(" ",$inicio);
				$vrfim=explode(" ",$fim);
				
				if(count($vrinicio)!=count($vrfim)) exit('Datas informadas em formatos incompatíveis.');
				//else if(count($vrinicio)===1){
					//ver se é data ou hora	
				//}
			}
			else{
				if(!empty($inicio)){
					$vrinicio=explode(" ",$inicio);				
					if(count($vrinicio)===1){
						if(strstr($vrinicio[0],'-')) $fim=$data;
						else $fim=$hora;
					}
					else $fim=$dataHora;
				 }
				 else{
					$vrfim=explode(" ",$fim);								
					if(count($vrfim)===1){
						if(strstr($vrfim[0],'-')) $inicio=$data;
						else $inicio=$hora;
					}
					else $inicio=$dataHora;
				 }
			}
			
			$inicio=strtotime($inicio);
			$fim=strtotime($fim);
			
			if($retorno=='meses') $retorno=($inicio-$fim)/1036800;
			else if($retorno=='dias') $retorno=($inicio-$fim)/86400;
			else if($retorno=='horas') $retorno=($inicio-$fim)/3600;
			else if($retorno=='minutos') $retorno=($inicio-$fim)/60;
			else if($retorno=='segundos') $retorno=($inicio-$fim);
			
			return -$retorno;
		}	
	}
?>