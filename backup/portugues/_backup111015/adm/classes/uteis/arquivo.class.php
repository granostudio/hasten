<?php
	class arquivo{
		private $formatos;
		private $imagens;
		
		public function renomear($old,$new){
			return rename($old, $new);
		}
		
		public function deletar($arquivo){
			return unlink($arquivo);
		}
		
		//UPLOAD ARQUIVO
		public function uploadArquivo($arquivos,$destino,$tamanho=2,$extensoes=array('doc','docx','pdf','zip','rar'),$nome=NULL){
			//VALIDAÇÕES DE DESTINO
			if(empty($destino)) throw new uploadException("Nenhum destino especificado para upload do arquivo.");
			if(!file_exists($destino)) diretorio::criaDir($destino,0777);
			if(!is_dir($destino)) throw new uploadException("O destino especificado para upload do arquivo não é válido.");
			if(!is_writable($destino)) throw new uploadException("Acesso recusado ao diretório do arquivo.");
			
			//UPLOAD
			$this->formatos=array('application');			
			$arquivos=arquivo::validaArquivoEnviado($arquivos,$tamanho,$extensoes,$nome);
						
			//UPLOAD PARA DESTINO FINAL
			$upx=0;
			$total=count($arquivos['name']);
			foreach($arquivos as $arquivo){
				if(is_file($arquivo['tmp_name'])){
					if(!rename($arquivo['tmp_name'],$destino.$arquivo)) $upx++;
				}
				else $upx++;
				
			}
			if($upx>0) throw new uploadException("Falha no Upload do arquivo ao diretório final, ".($total-$upx)."/".$total." arquivos foram enviados.");
			
			return $arquivos['name'];
		}	
		
		//UPLOAD IMAGEM
		public function uploadImagem($imagens,$tamanho=2,$extensoes=array('jpeg','jpg','gif','png'),$nome=NULL){
			$this->formatos=array('image');
			$this->imagens=arquivo::validaArquivoEnviado($imagens,$tamanho,$extensoes,$nome);
		}
		
		public function redimensionarImagem($comprimento=NULL,$altura=NULL,$qualidade=74,$escala=1){
			$upx=0;
			$total=count($this->imagens['tmp_name']);
			
			foreach($this->imagens['tmp_name'] as $imagem){				
				//IMAGEM ENVIADA
				$m2br=new m2brimagem($imagem);
				$enviadaComprimento=$m2br->getLargura();
				$enviadaAltura=$m2br->getAltura();
				
				//REDIMENSIONAMENTO EM CASO DE TAMANHO SUPERIOR AO DESEJADO
				if(!empty($comprimento)&&!empty($altura)){
					//ESCALAS
					$enviadaEscala=$enviadaComprimento/$enviadaAltura;
					$escalaFinal=$comprimento/$altura;
					
					if($escala) $enviadaEscala>$escalaFinal?$m2br->redimensiona('',$altura,''):$m2br->redimensiona($comprimento,'','');
					else $m2br->redimensiona($comprimento,$altura,'');
				}
				//IMAGEM MAIS COMPRIDA QUE DESEJADA
				else if(!empty($comprimento)&&$enviadaComprimento>$comprimento) $m2br->redimensiona($comprimento,'','');
				//IMAGEM MAIS ALTA QUE DESEJADA
				else if($altura!=NULL&&($enviadaAltura>$altura)) $m2br->redimensiona('',$altura,''); 
				
				if(!$m2br->grava($imagem,$qualidade)) $upx++;			
			}
			if($upx>0) throw new uploadException("Falha no redimensionamento de imagens, ".($total-$upx)."/".$total." imagens foram redimensionadas.");
		}
		
		public function salvarImagem($destino){			
			//UPLOAD PARA DESTINO FINAL
			if(empty($destino)) throw new uploadException("Nenhum destino especificado para migração da imagem.");
			if(!file_exists($destino)) diretorio::criaDir($destino,0777);
			if(!is_dir($destino)) throw new uploadException("O destino especificado para upload da imagem não é válido.");
			if(!is_writable($destino)) throw new uploadException("Acesso recusado ao diretório da imagem.");
			
			//ARQUIVO FINAL
			$upx=0;
			$total=count($this->imagens['name']);
			
			foreach($this->imagens['tmp_name'] as $id=>$imagem){					
				if(is_file($imagem)){
					if(!copy($imagem,$destino.$this->imagens['name'][$id])) $upx++;				
				}
				else $upx++;
			}
			if($upx>0) throw new uploadException("Falha no Upload da imagem ao diretório final, ".($total-$upx)."/".$total." imagens foram enviadas.");
		}
		
		public function finalizar(){			
			//REMOÇÃO DE IMAGEM TEMPORÁRIA
			$upx=0;
			$total=count($this->imagens['name']);
			
			foreach($this->imagens['tmp_name'] as $imagem){
				if(is_file($imagem)){
					if(!unlink($imagem)) $upx++;
				}
				else $upx++;
			}
			if($upx>0) throw new uploadException("Falha na remoção do arquivo temporário de imagem, ".($total-$upx)."/".$total." imagens foram removidas.");
			
			return $this->imagens['name'];
		}
		
		//VALIDAÇÃO DE ARRAYS DE CAMPOS FILES
		public function validaArquivoEnviado($arquivos,$tamanho,$extensoes,$nomeDesejado=NULL){			
			if(empty($arquivos)) throw new uploadException("Nenhum arquivo para upload enviado.");
			if(!is_array($arquivos['tmp_name'])){
				$arqsToArray=array();
				foreach($arquivos as $indice=>$valor){
					$arqsToArray[$indice][0]=$valor;
				}
			}
			$arquivos=$arqsToArray;
			
			$quantidade=count($arquivos['tmp_name']);
			for($x=0;$x<$quantidade;$x++){
				//NAME
				$nome=strrev($arquivos['name'][$x]);
				$nome=explode('.',$nome);
				$extensao=strtolower(strrev($nome[0])); //NAME
				
				if(empty($nomeDesejado)){
					$nomeFinal=NULL;
					foreach($nome as $part)
						$nomeFinal.=$part.(empty($nomeFinal)?'.':'');
					
					$nome=strrev($nomeFinal);
				}
				else $nome=$nomeDesejado.'-'.uniqid().'.'.$extensao;
				
				$arquivos['name'][$x]=nomenclatura::nomeSeguro($nome,true,array('.'));
				$arquivos['extensao'][$x]=pathinfo($arquivos['name'][$x],PATHINFO_EXTENSION); //APPLICATION
				
				//ERROR
				if($arquivos['error'][$x]!=0) throw new uploadException("O arquivo enviado parece estar corrompido, salve o arquivo novamente e tente o reenvio.");
				
				//TYPE
					//ARQUIVO TYPE (APLICACAO) - IMAGE/APPLICATION
					$trullyFile=0;
					foreach($this->formatos as $formato)
						if(strstr($arquivos['type'][$x],$formato)) $trullyFile++;
						
					if(!($trullyFile)) throw new uploadException("O tipo de aplicação do arquivo enviado não é permitido.");
					
					//EXTENSOES DESEJADAS
					$z=$y=0;
					foreach($extensoes as $extensao_permitida){ //PERMITIDAS
						//EXTENSÃO NOME DO ARQUIVO (NOMEADO)
						if($extensao==$extensao_permitida) $z++;
						
						//EXTENSÃO ARQUIVO TYPE (APLICAÇÃO)							
						if($extensao_permitida=='jpg') $extensao_permitida='jpeg';
						else if($extensao_permitida=='docx') $extensao_permitida='doc';
						
						if(strstr($arquivos['type'][$x],$extensao_permitida)) $y++;		
					}
					if(!$z||!$y) throw new uploadException("O tipo de extensão do arquivo enviado não é permitido.");
								
				//ERROR
				if($arquivos['error'][$x]>0) throw new uploadException("O arquivo enviado está corrompido.");				
				
				//SIZE
				if($arquivos['size'][$x]>($tamanho*1024000)) throw new uploadException("O arquivo enviado é maior que o permitido (megabytes).");
				
				//UPLOAD PARA SISTEMA
				if(!file_exists(ADM_DIR."_temp")) diretorio::criaDir(ADM_DIR."_temp",0777);
				if(!is_writable(ADM_DIR."_temp")) throw new uploadException("Acesso recusado ao diretório temporário.");
				if(!move_uploaded_file($arquivos['tmp_name'][$x],$newTemp=ADM_DIR."_temp/".$arquivos['name'][$x])) throw new uploadException("Falha no Upload de arquivo para diretório temporário.");
				$arquivos['tmp_name'][$x]=$newTemp;
				
				return $arquivos;
			}
		}
	}
?>