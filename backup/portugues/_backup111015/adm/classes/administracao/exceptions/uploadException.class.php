<?php
	class uploadException extends Exception{
		private $mensagem;
		private $campo;
		
		public function __construct($mensagem,$campo=NULL){
			$this->mensagem=$mensagem;
			$this->campo=$campo;
		}
		
		public function getMensagem(){ 	return $this->mensagem;	}
		public function getCampo(){		return $this->campo; 	}
	}
?>