<?php
	class perfil extends cliente_categoria{
		private $tabela='admin';
		private $usuario;
		
		private $id=1;
		private $senha;
		private $status;
		
		function __construct(){
			parent::__construct();
		}
		
		public function validarFormulario(){
			validacao::setCampo('senha','Senha');
			validacao::setCriptografia('md5');
			//validacao::setLen(6,16);
			$this->senha=validacao::validar('senha',$_POST[validacao::getCampo()],true);
		}
		
		//REGISTROS
		public function salvarRegistro(){
			try{
				$this->atualizarProduto();
				notificacao::sucesso('Senha alterada com sucesso!');
			}
			catch(notificacaoException $e){ header('Location: sistema.php'); }
		}
		
		//CONEXÕES
		private function atualizarProduto(){
			db::validaTabelaExistente($this->tabela);
			foreach(db::getTabela() as $coluna){
				$valor=NULL;
				if($coluna['Field']==='id') continue;
				else if(isset($this->$coluna['Field'])||$coluna['Null']==='YES'){
					if(strstr($coluna['Type'],'datetime')){
						if(strstr(strtolower($this->$coluna['Field']),'now')) $valor=$this->$coluna['Field'];
						else $valor='"'.$this->$coluna['Field'].'"';
					}
					else if($coluna['Null']==='YES'&&$this->$coluna['Field']==='') $valor='NULL';
					else if(strstr($coluna['Type'],'int')||strstr($coluna['Type'],'decimal')||strstr($coluna['Type'],'date')) $valor=$this->$coluna['Field'];
					else if(isset($this->$coluna['Field'])) $valor='"'.$this->$coluna['Field'].'"';
					
					if(isset($valor)) $query=empty($query)?$coluna['Field'].'='.$valor:$query.', '.$coluna['Field'].'='.$valor;
				}
			}
			$query='UPDATE '.$this->tabela.' SET '.$query.' WHERE id='.$this->id;
			db::query($query);
		}
		
		//SELECTS		
		public function buscaAdm($usuario,$senha){
			return db::fetch('SELECT codigo FROM usuario WHERE codigo="'.$codigo.'" AND status=1'.(!empty($this->id)?' AND id!='.$this->id:''));
		}	
	}
?>