<?php
	class artigo_comentario extends artigo{
		private $tabela='artigo_comentario';
		
		private $id;
		private $artigo;
		private $comentario;
		private $dataAprovacao=NULL;
		private $status;
		
		private $registro;
		private $mensagem;
		
		function __construct(){
			parent::__construct();
			
			try{
				if(!empty($_GET['registro'])){
					validacao::setCampo('registro','Id');
					$this->id=validacao::validar('inteiro',$_GET[validacao::getCampo()],true);
					
					$this->registro=$this->getRegistroBD();
					$this->registro=$this->registro[0];
				}
			}
			catch(validacaoException $e){ return NULL; }
		}
		
		//GETTERS
		public function getRegistro(){	
			try{				
				$registro=$this->getRegistroBD();
			
				if(count($registro)!=1) return NULL;
				else return $registro[0];
			}
			catch(validacaoException $e){ return NULL; }
		}
		
		public function getArtigoComentarios($artigo){
			$tipos=array(1=>'aprovados',2=>'pendentes',3=>'reprovados');
			$filtrados=array();
			
			$comentarios=$this->getRegistrosBD($artigo);
			if(!empty($comentarios)){
				foreach($tipos as $tipo=>$titulo){
					foreach($comentarios as $comentario){
						if($comentario['status']==$tipo){
							if(empty($filtrados[$tipo])) $filtrados[$tipo]=array();
							array_push($filtrados[$tipo],$comentario);
						}
					}
				}
			}
			return $comentarios;
		}	
		
		//VALIDAÇÃO		
		public function validarFormulario(){			
			validacao::setCampo('comentario','Comentário');
			$this->comentario=validacao::validar('texto',$_POST[validacao::getCampo()],true);
			
			validacao::setCampo('status','Status');
			$this->status=validacao::validar('inteiro',$_POST[validacao::getCampo()],true);
			
			if(empty($this->registro['dataAprovacao'])||strstr($this->registro['dataAprovacao'],"0000")){
				$this->dataAprovacao="NOW()";
				
				if($this->status==3) $this->mensagem='reprovado';
				else $this->mensagem='aprovado';
			}
			else $this->mensagem='editado';
		}	
		
		//REGISTROS
		public function salvarRegistro(){
			try{
				$this->atualizarProduto();
				notificacao::sucesso('Comentário '.$this->mensagem.' com sucesso!');
			}
			catch(notificacaoException $e){ header('Location: '.$_SERVER['HTTP_REFERER']); }
		}
		
		//CONEXÕES
		private function atualizarProduto(){
			db::validaTabelaExistente($this->tabela);
			foreach(db::getTabela() as $coluna){
				$valor=NULL;
				if($coluna['Field']==='id') continue;
				else if(isset($this->$coluna['Field'])||$coluna['Null']==='YES'){
					if(strstr($coluna['Type'],'datetime')){
						if(strstr(strtolower($this->$coluna['Field']),'now')) $valor=$this->$coluna['Field'];
						else if(!empty($this->$coluna['Field'])) $valor='"'.$this->$coluna['Field'].'"';
					}
					else if($coluna['Null']==='YES'&&$this->$coluna['Field']==='') $valor='NULL';
					else if(strstr($coluna['Type'],'int')||strstr($coluna['Type'],'decimal')||strstr($coluna['Type'],'date')) $valor=$this->$coluna['Field'];
					else if(isset($this->$coluna['Field'])) $valor='"'.$this->$coluna['Field'].'"';
					
					if(isset($valor)) $query=empty($query)?$coluna['Field'].'='.$valor:$query.', '.$coluna['Field'].'='.$valor;
				}
			}
			$query='UPDATE '.$this->tabela.' SET '.$query.' WHERE id='.$this->id;
			db::query($query);
		}
		
		public function removerRegistro(){
			try{
				if(empty($this->id)) throw new validacaoException('Falha coletando dados de registro para remoção.');
								
				$this->status=0;
				$this->salvarRegistro();
				
				notificacao::erro('Comentário removido com sucesso!');
			}
			catch(notificacaoException $e){ header('Location: sistema.php?modulo='.$_GET['modulo'].'&acao=comentarios'); }
		}
		
		//CONSULTAS
		private function getRegistroBD(){
			$sql='SELECT * FROM '.$this->tabela.' WHERE status>0 AND id="'.$this->id.'"';
			return db::fetch($sql);
		}
		
		private function getRegistrosBD($artigo){
			$sql='SELECT * FROM '.$this->tabela.' WHERE status>0 AND artigo='.$artigo.' ORDER BY status ASC';			
			return db::fetch($sql);
		}	
	}
?>