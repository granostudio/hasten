<?php
	class notificacao extends validacao{
		function __construct(){
			parent::__construct();
		}
		
		static protected function sucesso($mensagem){
			$_SESSION['notificacao']['status']=1;
			$_SESSION['notificacao']['mensagem']=$mensagem;
		}
		
		public static function alerta($mensagem){
			$_SESSION['notificacao']['status']=2;
			$_SESSION['notificacao']['mensagem']=$mensagem;
		}
		
		static protected function erro($mensagem){
			$_SESSION['notificacao']['status']=0;
			$_SESSION['notificacao']['mensagem']=$mensagem;
		}
		
		static public function getNotificacao(){
			if(!empty($_SESSION['notificacao'])){
				if($_SESSION['notificacao']['status']===0) $classe='erro';
				else if($_SESSION['notificacao']['status']===1) $classe='sucesso';
				else if($_SESSION['notificacao']['status']===2) $classe='alerta';
				
				echo '<div class="notificacao '.$classe.'"><span>'.$_SESSION['notificacao']['mensagem'].'</span></div>';
				unset($_SESSION['notificacao']);
			}
		}
	}
?>