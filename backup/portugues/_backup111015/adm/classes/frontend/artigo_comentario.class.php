<?php
	class artigo_comentario{
		private $id;
		private $artigo;
		private $comentario;
		private $idioma;
		
		function __construct($idioma){
			$this->db=new db();
			$this->idioma=$idioma;
		}

		public function cadastro($id,$comentario){
			try{
				$validacao=new validacao();
				$validacao->setCampo('id','Referência');
				$this->id=$validacao->validar('inteiro',$id,true);

				$validacao->setCampo('comentario','Comentário');
				$this->comentario=$validacao->validar('texto',$comentario,true);

				//VERIFICAÇÃO DE EXISTENCIA
				$artigo=new artigo($this->idioma);
				$artigo=$artigo->validarArtigoId($this->id);
				if(!$artigo) throw new validacaoException("Falha no cadastro do comentário.");
				
				exit("OK");
			}
			catch(validacaoException $e){
				$retorno=$e->getMensagem();
			}
		}
		
		public function getComentarios($id){
			return $this->getComentariosBD($id);
		}

		/* PRIVATE METHODS */
		private function cadastraComentarioArtigo(){
			$sql="INSERT INTO artigo_comentario (artigo,comentario,idioma,status)
			VALUES ($this->id,$this->comentario,$idioma,2)";

			exit($sql);
		}
		
		private function getComentariosBD($id){
			$sql="SELECT * FROM artigo_comentario WHERE artigo=".$id." AND idioma='".$this->idioma."' AND status=1";
			$sql.=" ORDER BY dataPost";
			
			$comentarios=$this->db->fetch($sql);
			
			return $comentarios;
		}
	}
?>