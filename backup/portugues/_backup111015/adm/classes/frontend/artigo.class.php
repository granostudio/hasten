<?php
	class artigo{
		private $db;
		private $idioma;
		private $id;
		
		function __construct($idioma){
			$this->db=new db();
			$this->idioma=$idioma;
		}
		
		public function todos(){
			return $this->getArtigos();
		}
		
		/* PRIVATE METHODS */
		private function getArtigos(){
			$sql="SELECT id,
						 titulo_".$this->idioma." titulo,
						 texto_".$this->idioma." texto,
						 imagem,
						 data,
						 tags_".$this->idioma." tags,
						 status							
			 	  FROM artigo";
				  
			$artigos=$this->db->fetch($sql);
			
			if(!empty($artigos)){
				$comentarios=new artigo_comentario($this->idioma);
				
				foreach($artigos as $id=>$artigo){
					$comments=$comentarios->getComentarios($artigo['id']);	
					if(!empty($comments)){
						$artigos[$id]['comentarios']=$comments;
					}
				}				
			}
			return $artigos;
		}
		
		public function validarArtigoId($id){
			$sql="SELECT count(*) FROM artigo WHERE id=".$id." AND status=1";
			
			$total=$this->db->fetch($sql);
			return $total[0][0];
		}
	}
?>