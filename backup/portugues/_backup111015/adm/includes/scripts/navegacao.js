/* DESENVOLVEDOR - EDWIN B. PANCOTI */
var show_error=1;
$(function(){
	$("#menu .modulos .modulo").mouseover(function(){
		$(this).children("h2").stop().animate({"padding-right":30,"width":176},200);		
	}).mouseout(function(){
		$(this).children("h2").stop().animate({"padding-right":12,"width":194},200);
	});

	if((el=$("[name=fator]")).length>0) el.maskMoney();
	if((el=$("[name=icms]")).length>0) el.maskMoney({precision:4});
	
	if((el=$("[name=cnpj]")).length>0) el.mask("99.999.999/9999-99");
	if((el=$("[name=cpf]")).length>0) el.mask("999.999.999-99");
	if((el=$("[name=cep]")).length>0) el.mask("99999-999");
	
	maskTel('telefone1');
	maskTel('telefone2');
	
	if($('textarea').length>0) nicEditors.allTextAreas();
	if((el=$("input.datahora")).length>0) el.datetimepicker();
	if((el=$("input.data")).length>0) el.datepicker();
	if((el=$("input.hora")).length>0) el.timepicker();
	
	$("#imprimir").click(function(){
		window.print();
	});
			
	/* MULTIPLOS */
	if((el=$("a#add_contato")).length>0){
		el.click(function(){
			$.ajax({url:'includes/php/fornecedor/ajax/add_contato.php',data:{method:'ajax'},type:'POST',dataType:'json',
					success:function(data){
						$(data).insertAfter('.parte2 #contatos p').animate({'width':'100%'}).animate({'height':100});
						$('.cpf').mask("999.999.999-99");
					},
					error:function(data){
						if(show_error){
							var acc=[];
							$.each(data,function(index,value){ acc.push(index+': '+value); });
							alert(JSON.stringify(acc));
						}
					}
			});
		});
	}
	
	if((el=$("a#add_certificado")).length>0){
		el.click(function(){
			$.ajax({url:'includes/php/fornecedor/ajax/add_certificado.php',data:{method:'ajax'},type:'POST',dataType:'json',
					success:function(data){
						$(data).insertAfter('.parte2 #certificados p').animate({'width':'100%'}).animate({'height':100});
					},
					error:function(data){
						if(show_error){
							var acc=[];
							$.each(data,function(index,value){ acc.push(index+': '+value); });
							alert(JSON.stringify(acc));
						}
					}
			});
		});
	}
	
	//COLORBOX
	if((elemento=$("a.cb")).length>0) elemento.colorbox({'rel':false});
	if((elemento=$("#links_ajuda a")).length>0) elemento.colorbox({'rel':false});
	
	$(window).scroll(function(){
		$.colorbox.resize();
	});
	
	/* CONFIRMAÇÃO PARA REMOÇÃO DE REGISTROS */
	$("a.deletar").click(function(){
		if($(this).hasClass("album")) var mens="Deseja mesmo remover este álgum e suas fotos?\nEssa ação não poderá ser desfeita.";
		else var mens="Deseja mesmo remover este registro?\nEssa ação não poderá ser desfeita.";
		
		if(!confirm(mens)) return false;
	});
	
	if((el=$('.notificacao')).length>0) el.delay(3200).slideUp();
	
	/* CANCELAR EDIÇÃO */
	$('input[type=button]').click(function(){
		if($(this).val()==='Cancelar') window.location.assign(document.URL.replace('registro','referer'))
	});
	
	//FOCO EM ERROS
	if((el=$('span.retorno')).length>0){
		var offset=el.offset();
		$(window).scrollTop(offset.top-50);
	}
});