<?php if($_GET['modulo']==="perfil"){ ?>
    <h3>Perfil - Editar</h3>
    <div class="tab_content">                        
        <?php
			if(!empty($_GET['acao'])&&$_GET['acao']==='senha'){
		?>
				<form action="" method="post">
                    <label>Nova senha:*</label>
                    <input type="password" maxlength="16" name="<?php echo $campo='senha'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($categoria)?stripslashes($categoria[$campo]):""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    <small class="preenchimento_info">Informe a nova senha de acesso para alterar a senha atual.</small>
                    
                    <input type="hidden" name="action" value="alterar_senha" />
                    <input type="submit" value="Alterar" />
                    <input type="button" value="Cancelar" />
                </form>
		<?php
			}
			else{
		?>
				<form action="" method="post">
                    <label>Nome:*</label>
                    <input type="text" maxlength="80" name="<?php echo $campo='nome'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($categoria)?stripslashes($categoria[$campo]):""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>E-mail:*</label>
                    <input type="text" maxlength="150" name="<?php echo $campo='email'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($categoria)?stripslashes($categoria[$campo]):""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Telefone:</label>
                    <input type="text" maxlength="20" name="<?php echo $campo='telefone'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($categoria)?stripslashes($categoria[$campo]):""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Celular:</label>
                    <input type="text" maxlength="20" name="<?php echo $campo='celular'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($categoria)?stripslashes($categoria[$campo]):""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <input type="hidden" name="action" value="alterar_perfil" />
                    <input type="submit" value="Alterar" />
                    <input type="button" value="Cancelar" />
                </form>
		<?php
			}
        ?>
    </div>
<?php } ?>














<?php
/*
//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS//PRODUTOS
                	if($_GET['modulo']==="produtos"){
						if($_GET['acao']==='novo'||$_GET['acao']==='editar'){
				?>
                            <div class="tabela">
                                <h3>Produtos - <?php echo $_GET['acao']==='novo'?'Cadastrar':'Editar'; ?></h3>
                                <div class="tab_content">                        
                                    <?php
                                        $db=new db();
                                        if(isset($_GET['registro'])||isset($_SESSION['usuario'])){
                                            $produto=$db->fetch('SELECT * FROM produto WHERE id='.$_GET['registro']);
                                            $produto=$produto[0];
										}
                                    ?>
                                    <form action="" method="post">
                                    	<label>Familia do Produto:*</label> 
                                        <select name="<?php echo $campo='familia'; ?>">
											<?php
                                                $db=new db();
                                                $categorias=$db->fetch("select * from produto_familia order by nome");
                                                
                                                if(!empty($categorias)){
                                                    echo "<option value=''>Selecione...</option>";
                                                    foreach($categorias as $categoria){
                                                        $selected=$_POST[$campo]==$categoria['id']?"selected='selected'":is_array($produto)&&$produto[$campo]==$categoria['id']?"selected='selected'":"";
        
                                                        echo "<option value='".$categoria['id']."' ".$selected.">".$categoria['nome']."</option>";
                                                    }
                                                }
                                                else echo "<option value=''>Nenhuma familia de produto disponível...</option>";
                                            ?>
                                        </select>
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>                                                                            
                                    	<label>Título:*</label>                                            
                                        <input type="text" maxlength="150" name="<?php echo $campo='nome'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                    
                                    	<label>Vasilhame:*</label>
                                        <select name="<?php echo $campo='vasilhame'; ?>">
											<?php
                                                $db=new db();
                                                $categorias=$db->fetch("select * from produto_vasilhame order by nome");
                                                
                                                if(!empty($categorias)){
                                                    echo "<option value=''>Selecione...</option>";
                                                    foreach($categorias as $categoria){
                                                        $selected=$_POST[$campo]==$categoria['id']?"selected='selected'":is_array($produto)&&$produto[$campo]==$categoria['id']?"selected='selected'":"";
        
                                                        echo "<option value='".$categoria['id']."' ".$selected.">".$categoria['nome']."</option>";
                                                    }
                                                }
                                                else echo "<option value=''>Nenhum tipo de vasilhame disponível...</option>";
                                            ?>
                                        </select>
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Código Principal:*</label>                                            
                                        <input type="text" maxlength="60" name="<?php echo $campo='codigo'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Volume em litros principal:*</label>                                            
                                        <input type="text" maxlength="8" name="<?php echo $campo='volume_em_litros'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>R$ por vasilhame:*</label>                                            
                                        <input type="text" maxlength="14" name="<?php echo $campo='valor'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />                             
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        
                                        
                                        <label>Código Componente A:</label>                                            
                                        <input type="text" maxlength="60" name="<?php echo $campo='codigo_componente_a'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Volume em litros A:</label>                                            
                                        <input type="text" maxlength="8" name="<?php echo $campo='volume_em_litros_a'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto[$campo])?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>R$ componente A:</label>                                            
                                        <input type="text" maxlength="14" name="<?php echo $campo='valor_a'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto[$campo])&&$produto[$campo]>0?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />                             
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Código Componente B:</label>                                            
                                        <input type="text" maxlength="60" name="<?php echo $campo='codigo_componente_b'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Volume em litros B:</label>                                            
                                        <input type="text" maxlength="8" name="<?php echo $campo='volume_em_litros_b'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto[$campo])?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>R$ componente B:</label>                                            
                                        <input type="text" maxlength="14" name="<?php echo $campo='valor_b'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto[$campo])&&$produto[$campo]>0?stripslashes(number_format($produto[$campo],2,'.',',')):""); ?>" />                             
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Classificação de Estoque:*</label>
                                        <select name="<?php echo $campo='made_to'; ?>">
											<?php
                                                $db=new db();
                                                $categorias=$db->fetch("select * from produto_made_to order by nome");
                                                
                                                if(!empty($categorias)){
                                                    echo "<option value=''>Selecione...</option>";
                                                    foreach($categorias as $categoria){
                                                        $selected=$_POST[$campo]==$categoria['id']?"selected='selected'":is_array($produto)&&$produto['made_to']==$categoria['id']?"selected='selected'":"";
        
                                                        echo "<option value='".$categoria['id']."' ".$selected.">".$categoria['nome']."</option>";
                                                    }
                                                }
                                                else echo "<option value=''>Nenhuma origem disponível...</option>";
                                            ?>
                                        </select>
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                                                          
                                        <label>Categoria:*</label>
                                        <select name="<?php echo $campo='categoria'; ?>">
											<?php
                                                $db=new db();
                                                $categorias=$db->fetch("select * from produto_categoria order by nome");
                                                
                                                if(!empty($categorias)){
                                                    echo "<option value=''>Selecione...</option>";
                                                    foreach($categorias as $categoria){
                                                        $selected=$_POST[$campo]==$categoria['id']?"selected='selected'":is_array($produto)&&$produto[$campo]==$categoria['id']?"selected='selected'":"";
        
                                                        echo "<option value='".$categoria['id']."' ".$selected.">".$categoria['nome']."</option>";
                                                    }
                                                }
                                                else echo "<option value=''>Nenhuma categoria disponível...</option>";
                                            ?>
                                        </select>                                       
                                    	<?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Quantidade Mínima:*</label>                                            
                                        <input type="text" maxlength="6" name="<?php echo $campo='qtdade_minima'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />                             
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Origem:*</label>
                                        <select name="<?php echo $campo='origem'; ?>">
											<?php
                                                $db=new db();
                                                $categorias=$db->fetch("select * from produto_origem order by nome");
                                                
                                                if(!empty($categorias)){
                                                    echo "<option value=''>Selecione...</option>";
                                                    foreach($categorias as $categoria){
                                                        $selected=$_POST[$campo]==$categoria['id']?"selected='selected'":is_array($produto)&&$produto[$campo]==$categoria['id']?"selected='selected'":"";
        
                                                        echo "<option value='".$categoria['id']."' ".$selected.">".$categoria['nome']."</option>";
                                                    }
                                                }
                                                else echo "<option value=''>Nenhuma categoria disponível...</option>";
                                            ?>
                                        </select>                                       
                                    	<?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
                                        
                                        <label>Múltiplos de:*</label>                                            
                                        <input type="text" maxlength="4" name="<?php echo $campo='multiplo'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($produto)?stripslashes($produto[$campo]):""); ?>" />                             
                                        <small class="preenchimento_info">Para qualquer quantidade informe 1 (um)</small>
                                        <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>

                                        <input type="hidden" name="action" value="salvar_produto" />
                                        <input type="submit" value="Enviar" />
                                        <input type="button" value="Cancelar" />
                                    </form>
                                </div>
                            </div>
				<?php
						}
						else{
				?>
							<div class="tabela">
                                <h3>Produtos</h3>
                                <div class="tab_content">                        
                                    <?php
                                        $db=new db();
										$categorias=$db->fetch("SELECT * FROM produto_familia order by nome");
										if(!empty($categorias)){
											foreach($categorias as $categoria){
												$produtos=$db->fetch('SELECT * FROM produto WHERE status=1 AND familia='.$categoria['id'].' order by codigo');
												
												echo '<h4>'.$categoria['nome'].'</h4>';
												echo "<div class='bloco_registro'>";
													if(!empty($produtos)){
														foreach($produtos as $produto){
															echo "<div class='registro'>";
																echo "<div class='conteudo'>";
																	echo "<p><strong>".($produto['codigo'].' - '.$produto['nome']).'</strong></p>';
																	echo "<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."&acao=remover&registro=".$produto['id']."' class='icone deletar'><img src='imagens/icones/acoes/deletar.png' alt='Deletar Registro' /></a>";
																	echo "<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."&registro=".$produto['id']."' class='icone'><img src='imagens/icones/acoes/editar.png' alt='Editar Registro' /></a>";
																echo "</div>";
															echo "</div>";
														}
													}
													else echo "<span class='no_regs'>Nenhum produto cadastrado.</span>";
												echo "</div>";
											}
										}
                                    ?>
                                </div>
                            </div>
				<?php
						}
					}
*/?>