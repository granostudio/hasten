<?php if($_GET['modulo']==="artigo"){ ?>
    <h3>Notícias</h3>
    <div class="tab_content">                        
        <?php
			if(!empty($_GET['acao'])&&$_GET['acao']==='gerenciar'){
				$artigos=new artigo();
                $artigos=$artigos->getRegistros();
				
				if(!empty($artigos)){							
					foreach($artigos as $artigo){
						echo "<div class='registro'>";
							echo "<div class='conteudo'>";
								echo "<p><strong>".data::converterData($artigo['data'],true,'html',true)."</strong> - ".texto::chamada($artigo['titulo_pt'],80).'</p>';
								 
								echo "<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."&acao=remover&registro=".$artigo['id']."' class='icone deletar'><img src='imagens/icones/acoes/deletar.png' alt='Deletar Registro' /></a>";
								echo "<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."&registro=".$artigo['id']."' class='icone'><img src='imagens/icones/acoes/editar.png' alt='Editar Registro' /></a>";
							echo "</div>";
						echo "</div>";
					}
				}
				else echo "<span class='no_regs'>Nenhum artigo cadastrado, cadastre agora mesmo <a href='sistema.php?modulo=".$_GET['modulo']."'>clicando aqui</a>.</span>";
			}
			else if(!empty($_GET['acao'])&&$_GET['acao']==='comentarios'){
				$noticias=new noticia();
                $noticias=$noticias->getRegistros();
				
				if(!empty($noticias)){
					foreach($noticias as $categoria){						
						echo '<h4>'.$categoria['titulo'].'</h4>';
						echo "<div class='bloco_registro'>";
							if(!empty($categoria['noticias'])){
								$registros=0;
								foreach($categoria['noticias'] as $noticia){
									$atencao=0;
									$comentarios=NULL;
									if(!empty($noticia['comentarios'])){
										echo "<div class='registro'>";
											echo "<div class='conteudo'>";
												echo "<p><strong>".texto::chamada($artigo['resumo'],120).'</strong></p>';
												
													foreach($noticia['comentarios'] as $comentario){
														$registros++;
														if($comentario['status']==2) $atencao++;
														
														$comentarios.="<div class='registro'>";
															$comentarios.="<div class='conteudo'>";
																$comentarios.="<p><strong>".texto::chamada($comentario['comentario'],120).'</strong></p>';
																 
																$comentarios.="<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."_comentario&acao=remover&registro=".$comentario['id']."' class='icone deletar'><img src='imagens/icones/acoes/deletar.png' alt='Deletar Registro' /></a>";
																$comentarios.="<a href='".$_SERVER['PHP_SELF']."?modulo=".$_GET['modulo']."_comentario&registro=".$comentario['id']."' class='icone'><img src='imagens/icones/acoes/editar.png' alt='Editar Registro' /></a>";
																$comentarios.="<img src='imagens/icones/acoes/".($comentario['status']==2?'aguardando':($comentario['status']==3?'inativo':'ativo')).".png' alt='Status' class='icone' />";
															$comentarios.="</div>";
														$comentarios.="</div>";
													}
												
													if($atencao>0) echo "<small class='pendentes'>Com pendencias <img src='imagens/icones/acoes/atencao.png' alt='Atenção: existem comentários pendentes de aprovação' class='icone' /></small>";
													
											echo "<div class='comentarios'>";										
												echo $comentarios;
											echo "</div>";
										echo "</div>";
									echo "</div>";
								}
							}
						}
						if(empty($registros)) echo "<span class='no_regs'>Nenhum comentário cadastrado.</span>";
						
						echo "</div>";
					}
				}
				else echo "<div class='bloco_registro'><span class='no_regs'><strong>Atenção:</strong> No momento não há nenhuma categoria cadastrada, cadastre primeiro uma categoria, para então criar um artigo.</span></div>";
			}
			else{
		?>
                <form action="" method="post" enctype="multipart/form-data">         
                    <label>Título (português):*</label>
                    <input type="text" maxlength="150" name="<?php echo $campo='titulo_pt'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Título (inglês):*</label>
                    <input type="text" maxlength="150" name="<?php echo $campo='titulo_en'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?>" />
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                                                      
                    <label>Artigo (português):*</label>
                    <textarea name="<?php echo $campo='texto_pt'; ?>"><?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?></textarea>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Artigo (inglês):*</label>
                    <textarea name="<?php echo $campo='texto_en'; ?>"><?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?></textarea>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Imagem:</label>
                    <input type="file" name="<?php echo $campo='imagem'; ?>" />
                    <small class="preenchimento_info">
                        <?php
                            if(!empty($dataRegistro[$campo])&&file_exists("../imagens/artigo/".$dataRegistro[$campo]))
                                echo "<div class='atual'>
                                          <a href='../imagens/artigo/".$dataRegistro[$campo]."' class='cb'><img src='../imagens/artigo/".$dataRegistro[$campo]."' alt='Imagem' /></a>
                                      </div>";
                        ?>
                        
                        Respeite as medidas a seguir para não ocorrer distorções ou perca na qualidade da imagem.<br />
                        Formatos: JPG, GIF (opaco) ou PNG (opaco)<br />
                        Dimensões: 439px X 277px<br />
                        Tamanho máximo: 2Mb
                    </small>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                                    
                    <label>Data:</label>
                    <input type="text" maxlength="12" name="<?php echo $campo='data'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?data::converterData($dataRegistro[$campo],true):""); ?>" class="datahora" />
                    <small class="preenchimento_info">O artigo passará a ser exibido apenas após a data informada. Qualquer data anterior a data atual o torna ativo.</small>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Tags (português):*</label>
                    <input type="text" maxlength="300" name="<?php echo $campo='tags_pt'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?>" />
                    <small class="preenchimento_info">Separadas por vírgulas</small>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Tags (inglês):*</label>
                    <input type="text" maxlength="300" name="<?php echo $campo='tags_en'; ?>" value="<?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?$dataRegistro[$campo]:""); ?>" />
                    <small class="preenchimento_info">Separadas por vírgulas</small>
                    <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
                    
                    <label>Status:*</label>
                    <select name="<?php echo $campo='status'; ?>">
                    	<option value="">Selecione...</option>
                        <option value='1' <?php echo isset($_POST[$campo])?($_POST[$campo]==1?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==1?'selected="selected"':''):''); ?>>Exibido</option>
                        <option value='2' <?php echo isset($_POST[$campo])?($_POST[$campo]==2?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==2?'selected="selected"':''):''); ?>>Oculto</option>
                    </select>
                    <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
  
                    <input type="hidden" name="action" value="salvar_artigo" />
                    <input type="submit" value="<?php echo !empty($dataRegistro)?'Alterar':'Cadastrar'; ?>" />
                    <input type="button" value="Cancelar" />
                </form>
		<?php }	?>
	</div> 
<?php } ?>