<?php if($_GET['modulo']==="artigo_comentario"){ ?>
    <h3>Notícias - Comentários Pendentes</h3>
    <div class="tab_content">                        
        <form action="" method="post">
            <label>Comentário:*</label>
            <textarea name="<?php echo $campo='comentario'; ?>"><?php echo isset($_POST[$campo])?$_POST[$campo]:(!empty($dataRegistro)?stripslashes($dataRegistro[$campo]):""); ?></textarea>
            <?php if(!empty($retorno_message[$campo])) echo $retorno_message[$campo]; ?>
            
            <label>Status:*</label>
            <select name="<?php echo $campo='status'; ?>">
                <option value='1' <?php echo isset($_POST[$campo])?($_POST[$campo]===1?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==1?'selected="selected"':''):''); ?>>Aprovado</option>
                <option value='3' <?php echo isset($_POST[$campo])?($_POST[$campo]===3?'selected="selected"':''):(!empty($dataRegistro)?($dataRegistro[$campo]==3?'selected="selected"':''):''); ?>>Reprovado</option>
            </select>
            <?php echo !empty($retorno_message[$campo])?$retorno_message[$campo]:""; ?>
            
            <input type="hidden" name="action" value="noticia_comentario" />
            <input type="submit" value="<?php echo !empty($dataRegistro)?'Alterar':'Cadastrar'; ?>" />
            <input type="button" value="Cancelar" />
        </form>
	</div>
<?php } ?>