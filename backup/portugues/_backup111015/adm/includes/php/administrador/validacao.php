<?php
	//VALIDAÇÃO DE MÓDULO
	$modulos_aceitos=array('perfil','artigo','artigo_comentario');
	if(!empty($_GET['modulo'])&&!in_array($_GET['modulo'],$modulos_aceitos)) header('Location: sistema.php');
		
	$referrer=explode("?",basename($_SERVER['REQUEST_URI']));
	
	try{
		if($_SERVER['REQUEST_METHOD']=="POST"){
			if($_GET['modulo']==="artigo"||
			   $_GET['modulo']==="artigo_comentario"){
				if($_POST['action']=='salvar_'.$_GET['modulo']){
					$classe=new $_GET['modulo']();
					$classe->validarFormulario();
					$classe->salvarRegistro();
										
					header("Location: ".$_SERVER['HTTP_REFERER']);
					exit();
				}
			}
		}
		else if($_SERVER['REQUEST_METHOD']==="GET"){
			if(!empty($_GET['modulo'])){
				if(!empty($_GET['registro'])&&is_numeric($_GET['registro'])){
					$dataRegistro=new $_GET['modulo']();
					$dataRegistro=$dataRegistro->getRegistro();

					if(empty($dataRegistro)){
						header("Location: sistema.php?modulo=".$_GET['modulo']);
						exit();
					}
				}
				
				if(!empty($_GET['acao'])&&$_GET['acao']==='remover'){
					$modulo=new $_GET['modulo']();
					$modulo->removerRegistro();
					
					if($_GET['modulo']=='noticia_comentario') header("Location: sistema.php?modulo=noticia&acao=comentarios");
					else header("Location: sistema.php?modulo=".$_GET['modulo'].'&acao=gerenciar');
					exit();
				}
			}
		}
	}
	catch(validacaoException $e){
		if(!empty($_POST['method'])&&$_POST['method']=="ajax"){
			$retorno_message['status']=false;
			$retorno_message['campo']=$e->getCampo();
			$retorno_message['mensagem']=$e->getMensagem();
			
			echo json_encode($retorno);
			exit();
		}
		else{
			$retorno_message[$e->getCampo()]='<span class="retorno">'.$e->getMensagem().'</span>';
		}
	}
	catch(SistemaException $e){ exit($e->getMensagem()); }
?>