<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/quem.css" rel="stylesheet" type="text/css" />
    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="tudo">
<div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_quem.png" width="920" height="202" /></div>
</div>
        
        <div class="img_quem"><img src="imagens/eduardo.jpg" width="212" height="337" /></div>
        <div class="quem_txt1"><p>Se foi em 1981 ou 1982, já não me lembro ao certo, mas eu era apenas um garoto recém-saído do ensino médio quando conheci o Dr. Albert Sabin, reconhecido no mundo por desenvolver a vacina oral contra a poliomielite, a famosa "gotinha". 
Meu pai, diretor da Dow Chemical - Laboratórios Lepetit na época, havia me escalado para recepcionar e assessorar o ilustre visitante, que viria ao Brasil para palestrar no Congresso de Pediatria em Belo Horizonte, Minas Gerais, minha terra natal.</p>
<p>Não que eu tivesse, naqueles tempos, alguma pretensão de seguir carreira na Saúde - queria, na verdade, fazer antropologia e, mais do que isso, sair do Brasil. Recebi a missão porque havia estudado nos Estados Unidos e, por isso,falava inglês fluente, ainda sem sotaque.</p>

</div>

<div class="quem_txt2"><p>Empolgado com a tarefa, parti com o motorista da Dow Química, o João, para buscar o Dr. Sabin no aeroporto da Pampulha, num luxuoso Dodge Le Baron. E então o acompanhei por toda parte – hotel, congresso e passeios -, exceto pelos eventos sociais, à noite, nos quais eu não tinha idade para ir. Visitamos Ouro Preto e Congonhas, contei a ele as histórias locais e o apresentei à comida mineira, feita no fogão a lenha. Seu prato favorito: tutu com banana assada, torresmo e couve. Acho que me viu como um filho e me tratou como tal.</p>

<p>Aquele era só o começo de uma história de 30 anos na indústria de produtos médicos, como executivo global de negócios. E muito antes de descobrir que antropologia não me permitiria uma carreira internacional, eu já caminhava sobre o campo da Saúde, trabalhando com meu pai na distribuição de Citrovit nos laboratórios de Minas Gerais, dos 7 aos 14 anos. Vestia uma jaqueta e shorts azul, gravata borboleta, sapato e meia preta até os joelhos. Esse era meu uniforme. </p></div>

<div class="olho"><img src="imagens/olho.jpg" width="228" height="216" /></div>

        <div class="conteudo_quem">
        <p>Um dia, estava de saída do laboratório Hermes Pardini, hoje o maior da América Latina, todo cabisbaixo, depois de esperar duas horas e meia para ser atendido e saber que a empresa só abriria novos contratos no ano seguinte, quando a Dona Carmen Pardini me viu passar.</p>

<p><i>- "Você não é o filho do Dr. Hugo?", ela me perguntou?<br />
- "Sou sim, Dona Carmen, meu nome é Eduardo", respondi.<br />
- "Olha quem está aqui, Hermes, o filho do Hugo", disse Dona Carmen, chamando o fundador do laboratório e apertando minhas bochechas.<br />
- "Você não mudou nada, só o cabelo ficou mais escuro", ele brincou. "O que você está fazendo aqui?"<br />
- "Sou vendedor da OrganonTeknika", doutor Hermes. E então contei a eles o resultado da minha reunião.<br />
- "Ô, Carmen, a gente tem contrato de exclusividade assinado?", perguntou ele.<br />
- "Que eu saiba, não, Hermes", Dona. Carmen confirmou.</i></p>

<p>Ali conquistei o primeiro pedido dos laboratórios Hermes Pardini,e com ele fechei minha cota do ano. Seis meses depois, tinha cumprido a meta de vendas do Brasil e, em novembro, da América Latina. No final do meu primeiro ano, virei gerente regional de vendas e, logo depois, nacional. A convite da empresa,montei o que talvez tenha sido o primeiro telemarketing na Saúde, para comercializar produtos mais simples, por exemplo teste de gravidez,para pequenos laboratórios. O projeto foi um sucesso por lá. As meninas do call center chegavam a faturar  o que hoje é equivalente a R$ 1 milhão, só por telefone.</p>

<p><strong>Junto com nomes de prestígio do setor, ajudei a fundar a Câmera Brasileira de Diagnóstico Laboratorial (CBDL), com o objetivo de defender os direitos dos importadores, que na época respondiam por mais de 60% dos produtos fornecidos para laboratórios.</strong></p>

<p>De lá para cá, vivi uma sequência de encontros acertados. A começar pelo convite do diretor de América Latina da Sanofi Pasteur para que eu assumisse a gerência de desenvolvimento da região, a partir dos Estados Unidos. Depois, em 1998, fui chamado pela Kimberly-Clark para atuar a partir de Boca Raton, na Flórida, e depois na Holanda. Em 2001, a empresa decidiu cortar os investimentos nos mercados emergentes e a Mentor me encontrou. Mas minha trajetória na gigante norte-americana teria continuidade. Voltei para lá em 2005 e três anos depois, voltei mesmo foi para casa, em Belo Horizonte.</p>

<p><strong>A linha do tempo diz o restante: Stryker, UpToDate, Masimo e, agora, a Hasten, que já nasce com uma bagagem de 30 anos de vivência e conhecimento do qual me apropriei nesses anos todos.As outras histórias que não cabem em currículo e linha do tempo, eu conto num café.</strong></p>
    </div>
        
    <div class="destaque"><p>Eduardo de Melo é especialista no desenvolvimento de vendas e oportunidades de negócios para a indústria internacional de equipamentos e insumos médicos no Brasil e América Latina. Ajuda organizaçõesdo setor a alcançarem crescimento rápido e sustentável, a partir da elaboração e implementação de planos de negócios, configuração de uma rede de distribuição local, liderança de equipes de diferentes níveis e culturas, definição de marketing estratégico, domínio sobre processo regulatório na região, conhecimento do setor, capacitação e gestão de vendas complexas.</p></div>
  <img src="imagens/linha_tempo.jpg" width="933" height="255" /></div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
