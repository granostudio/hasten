<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/home.css" rel="stylesheet" type="text/css" />
    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
    <script src="includes/js/jquery.js" type="text/javascript"></script>
<script src="includes/js/efeitos.js" type="text/javascript"></script>

</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div id="banner">
            <img src="imagens/banner_1.jpg" id="1" class="hide"/>
        <img src="imagens/banner_2.jpg" id="2" class="hide" />
        <img src="imagens/banner_3.jpg" id="3" class="hide"/>
            </div>
        </div>
        
        <div class="conteudo">
        <h1>Benefícios da Hasten Corp</h1>
        <p> Somos uma consultoria especializada em saúde com um olhar atento para as oportunidades de negócios nos países emergentes.<br />
Um de nossos principais diferenciais é a capacidade de combinar o conhecimento da cultura e das demandas do investidor internacional ao jeito de fazer negócios no Brasil. <br />Desta forma, promovemos a aceleração das vendas, seja em importação ou exportação, o aumento de receita e redução de riscos, nos segmentos de saúde humana e animal.</p>
        </div>
        
        <div class="servicos"><h2>Serviços</h2>
        <p class="servicos1">Com sede nos Estados Unidos e escritórios no Brasil, Chile e México, a Hasten oferece serviços de consultoria que vão da 	estratégia à operação:</p>
        <div class="quadro1"><li>Representação de marcas</li>
		<li>Elaboração e execução de plano de <br />negócio</li>
		<li>Desenvolvimento de negócios internacionais</li>
		<li>Gestão de vendas complexas</li>
		</div>
        <div class="quadro2"><li>Consultoria</li>
		<li>Marketing estratégico</li>
		<li>Relações públicas</li>
		<li>Formação de redes de distribuição locais</li>
		</div>
        <div class="quadro3"><li>Logística</li>
		<li>Orientação sobre particularidades regulatórias e tributárias em cada país</li>
		<li>Operação</li>
		</div>         
        </div>
        
        
    </div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
