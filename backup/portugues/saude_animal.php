<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<title>Hasten</title>
	<link href="includes/css/geral.css" rel="stylesheet" type="text/css" />
    <link href="includes/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="tudo">
    <div class="centro">
<?php include_once('includes/php/topo_menu.php') ?>
            <div class="banner_interno"><img src="imagens/banner_animal.png" width="920" height="202" /></div>
        </div>
        
        <div class="conteudo_interno">
<p><strong>Em todo o mundo, saúde animal é um segmento que não para de crescer. O faturamento já está na casa dos US$ 20 bilhões e o crescimento nominal gira em torno de 8% ao ano, de acordo com a empresa de inteligência de mercado Animal Pharm.</strong></p>

<p>Nesse cenário, o Brasil desponta como umdos mercados mais promissores, tanto para multinacionais, que pretendem expandir suas operações, como indústrias brasileiras de todos os portes, especialmente as que querem oferecer produtos de nicho, adaptados à realidade do criador brasileiro.</p>

<p>Com R$ 4 bilhões de faturamento por ano, o País ocupa a terceira posição no ranking mundial, atrás apenas dos Estados Unidos e da China. Os produtos biológicos respondem por 27% desse montante, seguidos por antiparasitários (25%), antimicrobianos (16%), terapêuticos (9%), suplementos (5%) e outros (17%).</p>

<p>Os números, levantados pelo Sindicato Nacional da Indústria de Produtos de Saúde Animal (Sindan), indicam ainda que a maior parte da produção é destinada aos ruminantes, com 56,3%, seguidos por cães e gatos, 14,7%, aves, 14,4%, suínos 12,5% e equinos, 2%.</p>

<p>Não por acaso, esses dados seguem exatamente o perfil da produção animal do País,que possui a maior criaçãocomercial de bovinos do mundo;o segundo maior mercadode animais domésticos; e, também, o maior volume de exportações de carne de frango.São mais de 185 milhões de cabeças de gado bovino, 39 milhões de suínos, 17,3 milhões de ovinos, 9,3 milhões de caprinos e 5,5 milhões de equinos, além de um número crescente de bichos de estimação, entre eles, 37,1 milhões de cães e 21,3 milhões de gatos, segundo a Associação Brasileira da Indústria de Produtos para Animais de Estimação (Abinpet).</p>

<p>Em seu Panorama da Indústria Farmacêutica Veterinária, o Banco Nacional do Desenvolvimento Econômico e Social (BNDES) aponta três fatores para a expansão do setor: continuidade da ameaça de doençasanimais, aumento do interesse da população sobre a segurançaalimentar e consequente elevaçãodo rigor do arcabouço regulatório,e, por fim, crescimento da população de bichosde companhia.</p>

<p>Do ponto de vista da indústria, o segmento está em franca expansão por causa do aumento das exportaçõesde produtos veterinários, uma vez que o Brasil é um importante centro deprodução para as multinacionais; maior fiscalizaçãosanitária e critérios cada vez mais exigentes para a comercialização; e maior conscientização dos criadores sobre a importância de manter os rebanhos saudáveis, com programassanitários eficientes e regulares.</p>

<p><strong>Entre os principais clientes da indústria veterinária hoje estão os pecuaristas (4 milhões), propriedades rurais (4 milhões), médicos veterinários (30 mil), avicultores (14 mil), suinocultores (4 mil), e equinocultores (1 mil).</strong></p><br />
<img src="imagens/grafico_animal.jpg" width="920" height="348" /></div>
        
        
    </div>
  
   <?php include_once('includes/php/rodape.php') ?> 
    
</div>
</body>
</html>
