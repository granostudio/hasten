module.exports = function(grunt) {
  "use strict";
  var pkgJson = grunt.file.readJSON('wordpress/ambientconfig.json' );
  var pkgFTP = grunt.file.readJSON('.ftppass' );
  grunt.initConfig({
    //server local -------------------------------------------------------------
    php: {
        local: {
            options: {
                port: 9001,
                hostname: 'localhost',
                base: 'wordpress',
                keepalive: false,
                open: true,
                silent: true,
            },
        },
        watch: {
          options: {
            port: 9001,
            hostname: 'localhost',
            base: 'wordpress',
            open: true,
            options: {
                livereload: {
                  host: 'localhost',
                  port: 9001,
                }
              },
          }
        }
    },
    // DEPLOY ----------------------------------------------------------------
    ftpush: {
      dev: {
        auth: {
          host: pkgJson.ftphost,
          port: pkgJson.ftpport,
          authKey: 'ftpkey'
        },
        src: 'wordpress',
        dest: '/public_html',
        simple: true
        // exclusions: ['path/to/source/folder/**/.DS_Store', 'path/to/source/folder/**/Thumbs.db', 'dist/tmp'],
        // keep: ['/important/images/at/server/*.jpg']
      },
      live: {
        auth: {
          host: pkgJson.ftphost,
          port: pkgJson.ftpport,
          authKey: 'ftpkey'
        },
        src: 'wordpress',
        dest: '/public_html',
        simple: true
        // exclusions: ['path/to/source/folder/**/.DS_Store', 'path/to/source/folder/**/Thumbs.db', 'dist/tmp'],
        // keep: ['/important/images/at/server/*.jpg']
      }
    },
    // abrir pastas depois do deploywp -----------------------------------------
    open: {
      dev: {
        path: 'http://'+pkgJson.clienteurl+"/dev",
        app: 'Google Chrome'
      },
      live: {
        path: 'http://'+pkgJson.clienteurl,
        app: 'Google Chrome'
      }
    },
    //copy frontend para wp ----------------------------------------------------
    copy: {
      dev: {
        files: [
          {expand: true, cwd: 'frontend/', src: ['**'], dest: 'wordpress/wp-content/themes/granostudio-child/'}
        ],
      },
      update_copy: {
        files: [
          {expand: true, cwd: 'granoexpresso/root/wordpress', src: ['**'], dest: 'wordpress/'}
        ],
      }
    },
    // COMPASS ----------------------------------------------------------------
    compass: { // Task
      admin: { // tema mãe
        options: {  // Target options
          sassDir: 'wordpress/wp-content/themes/granostudio/sass',
          cssDir: 'wordpress/wp-content/themes/granostudio/css',
          environment: 'production',
          outputStyle: 'expanded',
          // require: 'bootstrap-sass'
        }
      },
      dev: { // tema mãe
        options: {  // Target options
          sassDir: 'frontend/sass',
          cssDir: 'frontend/css',
          environment: 'production',
          outputStyle: 'expanded',
          // require: 'bootstrap-sass'
        }
      },
    },
    // CSSMIN ----------------------------------------------------------------
    cssmin: {
        options: {
          shorthandCompacting: false,
          roundingPrecision: -1
        },
        admin: {
          files: {
            'wordpress/wp-content/themes/granostudio/css/main.min.css': ['wordpress/wp-content/themes/granostudio/css/plugins/*.css','wordpress/wp-content/themes/granostudio/css/bootstrap/bootstrap.css', 'wordpress/wp-content/themes/granostudio/css/main.css']
          }
        },
        dev: {
          files: {
            'frontend/css/main.min.css': ['frontend/css/plugins/*.css', 'frontend/css/main.css']
          }
        }
    },
    // WATCH ----------------------------------------------------------------
    watch: {
      admin: {
        files: ['wordpress/wp-content/themes/granostudio/sass/*.scss','wordpress/wp-content/themes/granostudio/sass/modules/*.scss', 'wordpress/wp-content/themes/granostudio/js/src/*.js', 'wordpress/wp-content/themes/granostudio/js/src/plugins/*.js'],
        tasks: [
          'compass:admin',
          'cssmin:admin',
          'uglify:admin'],
        options: {
          livereload: true,
        },
      },
      local: {
        files: ['frontend/sass/*.scss','frontend/sass/modules/*.scss', 'frontend/js/src/**.js', 'frontend/js/src/plugins/**.js','frontend/modules/**.php'],
        tasks: [
          'compass:dev',
          'cssmin:dev',
          'uglify:dev',
          'copy:dev'],
        options: {
          livereload: true,
        },
      },
      dev: {
        files: ['frontend/sass/*.scss','frontend/sass/modules/*.scss', 'frontend/js/src/**.js', 'frontend/js/src/plugins/**.js','frontend/modules/**.php'],
        tasks: [
          'compass:dev',
          'cssmin:dev',
          'uglify:dev',
          'copy:dev',
          'ftpush:dev'],
        options: {
          livereload: true,
        },
      }
    },
    // UGLIFY ----------------------------------------------------------------
    uglify: {
      admin: {
        files: {
          'wordpress/wp-content/themes/granostudio/js/dist/scripts.min.js': ['wordpress/wp-content/themes/granostudio/js/src/plugins/*.js', 'wordpress/wp-content/themes/granostudio/js/src/scripts.js'],
          'wordpress/wp-content/themes/granostudio/js/dist/grano-pagetemplate.min.js': 'wordpress/wp-content/themes/granostudio/js/src/grano-pagetemplate.js',
          'wordpress/wp-content/themes/granostudio/js/dist/customize-preview.min.js': 'wordpress/wp-content/themes/granostudio/js/src/customize-preview.js'
        }
      },
      dev: {
        files: {
          'frontend/js/dist/scripts.min.js': [ 'frontend/js/src/plugins/*.js', 'frontend/js/src/scripts.js'],
          'frontend/js/dist/grano-pagetemplate.min.js': 'frontend/js/src/grano-pagetemplate.js',
          'frontend/js/dist/customize-preview.min.js': 'frontend/js/src/customize-preview.js'
        }
      }
    },
    // EXEC -------------------------------------------------------------------
    exec: {
      update_rm: {
        cmd: 'git rm -f granoexpresso',
      },
      update_submodule: {
        cmd: 'git submodule add --force https://bitbucket.org/granostudio/granoexpresso.git; git submodule foreach git pull origin master',
      }
    }
  });

  grunt.loadNpmTasks('grunt-php');
  grunt.loadNpmTasks('grunt-open');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-ftpush');




  // --------------------------------------------------------------------------------------
  // TASKs
  // --------------------------------------------------------------------------------------
  // instalação inicial do wp
  grunt.registerTask('grano-wp', ['ftpush:live','ftpush:dev','open:dev','open:live']);
  // desenvolvimento dev
  grunt.registerTask('grano-dev', ['watch:dev']);
  // desenvolvimento local
  grunt.registerTask('grano-local', ['php:local','watch:local']);

  // colocar o site no ar
  grunt.registerTask('grano-live', ['copy:dev','ftpush:live','open:live']);

  // des. local do tema original
  grunt.registerTask('admin-local', ['php:local','watch:admin']);

  // update grano expresso
  grunt.registerTask('grano-update', ['exec:update_submodule','copy:update_copy', 'exec:update_rm', 'copy:dev']);

  // --------------------------------------------------------------------------------------

};
